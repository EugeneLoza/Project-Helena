{Copyright (C) 2013-2016 Yevhen Loza

This program is Free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a Copy of the GNU General Public License
along with This program. If not, see <http://www.gnu.org/licenses/>.}

unit Unit1;

{$Mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, Types,
  CastleControls, CastleVectors, CastleGLUtils, CastleRectangles, CastleUIControls,
  CastleGLImages,
  CastleImages, CastleFonts, CastleUnicode, CastleStringUtils,
  CastleSoundEngine, CastleTimeUtils, CastleKeysMouse, CastleControl;

const
  DebugMode = false;

const
  BotTypes = 4; {0..BotTypes}
  MaxCracks = 11;
  MaxPass = 22;
  MaxWall = 25;
  MaxMusic = 14;

const
  MaxMaxX ={113}226;  //max 'reasonable' 50x50
  MinMaxX = 35;
  MaxMaxY = MaxMaxX;
  MinMaxY = MinMaxX;
  MaxBots = 500;

  Zoom = 35;
  ViewSizeX = Zoom;
  ViewSizeY = Zoom;
  ZoomScale = 700 div 35;

  MaxBuildings = 1000;
  MaxBuildingX = 20;
  MaxBuildingY = 20;
  RandomAccuracy = 100;

  MaxPlayers = 16;          // max 16 fit in bunker

  BackpackSize = 12;
  ChangeWeaponTime = 100;
  DropItemTime = 20;
  PickUpItemTime = 40;

  BlastPush = 10;
  WallHardness = 1;

  MaxItems = MaxBots * BackpackSize + 100;
  MaxOnFloor = 50; // max displayed OnFloor Items
  MaxUnitsList = MaxBots;

  MinimumTUUsable = 29;

  DistanceAccuracy = 5; //5: max 51 horizontal steps (6 turns)
  DistanceStep = 7;
  //accuracy 7/5 provides 1% error (-3TU/turn), 3/2 provides 6% error (+15Tu/turn)
  DistanceError = 1.4142135 - DistanceStep / DistanceAccuracy;
  // 1% error = 3TU per turn   { = DistanceAccuracy * Sqrt(2)}

  PlayerBotMoveDelay = 100;     {ms}
  EnemyBotMoveDelay = 300;      {ms}
  ShotDelay = 200;              {ms}
  HitDelay = 200;
  BlastDelay = 500;             {ms}
  InfoDelay = 100;

  DefenseDifficulty = 5;
  StandardDamage = 10;
  AverageLOSValue = 42.92;   ////////////////////////////// corrected LOS

const
  ItemDamageRate = 0.4;           //40% Damage taken by armed Weapon

const
  MaxBotHPConst = 3000;
  MaxPlayerHPConst = 3000;

const
  ActionAttack = 1;
  ActionRandom = 2;
  ActionGuard = 3;

  CallReinforcementsRange = 25; //sqr

const
  VisibleRange = 32;
  VisibleAccuracy = 10;
  AccuracyAccuracy = 10;
  OldVisible = 1;
  MaxVisible = 100;

  MaxAngle = 8;

const
  PlayerMovementDelay = 0;

const
  Player = 1;
  Computer = 2;

const
  MapFree = 0;
  MapSmoke = 15;
  MaxStatus = 31;
  MapWall = 128;
  MapWallSmoke = MapSmoke div 2;
  DieSmoke = MapSmoke div 3;

  MapGeneration_Wall = 247;//255-4-4;
  MapGeneration_Free = 251;//255-4;
  MapGeneration1 = 246;
  MapGeneration2 = 245;
  MapGeneration3 = 244;
  MapGeneration4 = 243;

const
  GameMode_Game = 255;
  GameMode_Victory = 254;
  GameMode_Defeat = 253;

  GameModeMap = 127; //separator between Mode "click to continue" and "Move"

  GameMode_None = 0;
  GameMode_ItemInfo = 1;

const
  BorderSize = 7;
  TextWidth = 45;

const
  DataFolder = 'DAT' + PathDelim;
  ScriptFolder = DataFolder + 'default' + PathDelim;
  INIFileName = 'projecthelena.ini';

const
  Strategy_Hide = true;

  Strategy_TypeCheater = true;
  Strategy_TypeDamnCheater = false;

  Strategy_RealLOS = 255;
  Strategy_PossibleLoc = 50;
  Strategy_PossibleLOS = 100;
  Strategy_Visible = 1;

type
  ItemType = record
    W_ID: Byte;                 //for Weapons, 0 if pure Ammo
    State, MaxState: Byte;
    RechargeState: word;

    Ammo_ID: Byte;
    n: Byte;
  end;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button6: TButton;
    CastleControl1: TCastleControl;
    CastleControl2: TCastleControl;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    ProgressBar1: TProgressBar;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    ScrollBar1: TScrollBar;
    ScrollBar2: TScrollBar;
    ScrollBar3: TScrollBar;
    ScrollBar4: TScrollBar;
    Timer1: TTimer;
    ToggleBox1: TToggleBox;
    TrackBar2: TTrackBar;
    TrackBar3: TTrackBar;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure CastleControl1Press(Sender: TObject; const Event: TInputPressRelease);
    procedure CastleControl1Render(Sender: TObject);
    procedure CastleControl2Render(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure CheckBox5Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CastleControl1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CastleControl1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure Image2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image3MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure Image4MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image4MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure Image5MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image5MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure Image6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image6MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure Image7MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure ToggleBox1Change(Sender: TObject);
    procedure ScrollBar1Change(Sender: TObject);
    procedure ScrollBar2Change(Sender: TObject);
    procedure ScrollBar3Change(Sender: TObject);
    procedure ScrollBar4Change(Sender: TObject);

  private
    { private declarations }
  public
    procedure Create_language_interface;
    procedure ReadINIFile;
    procedure WriteINIFile;
    procedure GenerateMap;
    procedure Generate_Items_types;
    procedure DrawMap;
    procedure Draw_Stats;
    procedure look_aRound(Thisbot: Integer);
    procedure Botlook_aRound(Thisbot: Integer);
    procedure ClearVisible;
    procedure BotClearVisible;
    procedure BotrecalcVisible;
    procedure Botcalculate_PossibleLoc(Thisbot: Integer);
    procedure Botcalculate_PossibleLOS;
    procedure Read_Buildings;
    function Generate_Enemy_List(messageflag: Boolean): Boolean;
    procedure GenerateMovement_generic(fromX, fromY, toX, toY, maxdistance: Byte;
      Clearit, Playergo{,stealth}: Boolean);
    procedure GenerateMovement(Thisbot, targetX, targetY: Integer);
    procedure Move_bot(Thisbot, toX, toY, use_waypoints: Integer);
    procedure end_turn;
    procedure BotAction(Thisbot: Integer);
    procedure start_turn;
    procedure set_ProgressBar(Flg: Boolean);
    procedure find_OnFloor(x, y: Integer);
    procedure growSmoke;
    procedure alert_other_Bots(target, who: Integer);
    procedure BotShots(attacker, defender: Integer);
    function SpendTU(Thisbot: Integer; tus: Integer): Boolean;
    procedure calculate_area(ax, ay, a, aSmoke, aBlast: Integer);
    function LoadWeapon(Thisbot, ThisItem: Integer): Boolean;
    procedure pick_up(Thisbot, ThisItem: Integer);
    procedure Drop_Item(Thisbot, ThisItem: Integer);
    procedure SetControlsMenu(Flg: Boolean);
    procedure SetControlsGame(Flg: Boolean);
    procedure CenterMap(ToX, ToY: Integer);

    procedure DisplayItemInfo(ThisItem: ItemType);

    //    procedure display_message(mess:string);
  end;

const
  maxUsableAmmo = 9;

type
  WeaponType = record
    Name: string[30];
    ACC, DAM: Byte;
    Recharge, Aim, Reload: Byte;
    MaxState: Byte;
    Ammo: array[1..MaxUsableAmmo] of Byte;
    Kind, rnd: Integer;
    Description: string;
  end;

type
  AmmoType = record
    Name: string[30];
    Quantity: Byte;
    ACC, DAM: Integer;
    Explosion, AREA, Smoke: Integer;
    Kind, rnd: Integer;
    Description: string;
  end;

type
  laying_ItemType = record
    Item: ItemType;
    x, y: Integer;
  end;

type
  BotType = record
    Name: string[30];
    Action: Byte;
    BType: Byte;
    caution: Byte;
    target: Integer;
    HP, MaxHP: word;
    TU: Byte;
    Speed: Byte;
    Angle: Byte;
    X, Y: Integer;
    Owner: Integer;
    Items: array[1..BackpackSize] of ItemType;

    LastSeenX, LastSeenY, lastseen_tu: Byte;
  end;

type
  Maparray = array[1..MaxMaxX, 1..MaxMaxY] of Byte;

var
  Form1: TForm1;

  map, MapStatus, vis, Movement, distance, MapChanged: ^Maparray;
  MapG: ^Maparray;
  botvis: ^Maparray;
  MapParameter: array[0..8] of Single;
  mapBuildings: array[1..MaxBuildings] of string;
  nBuildings: Integer;
  LOS_base: ^Maparray;
  mapFreespace: Integer;
  MapInhomogeneity: Single;
  estimated_Botstogether: Single;

  averageLOS: Single;
  MaxX, MaxY: Integer;
  max_maxValue, MinMaxValue, average_maxValue: Integer;
  Playersn: Integer;
  ViewX, ViewY{,ViewSizeX,ViewSizeY}: Integer;
  DrawMapall, ShowLOS: Boolean;
  MapGenerated: Boolean;
  MapGenerationText, mapFreeText: string;
  Random_tiles: Boolean;
  reGenerateLOS: Boolean;
  GameMode, PreviousGameMode: Byte;

  bot: array[1..MaxBots] of BotType;
  PlayerUnits, EnemyUnits: array[1..MaxUnitsList] of Integer;
  PlayerUnitsN, EnemyUnitsn: Integer;

  Item: array[1..MaxItems] of laying_ItemType;
  Itemsn: Integer;
  OnFloor: array[1..MaxOnFloor] of Integer;
  OnFloorn: Byte;
  WeaponSpecifications: array[1..255] of WeaponType;
  AmmoSpecifications: array[1..255] of AmmoType;
  //other Items 200...255;

  NBot: Integer;
  Selected, SelectedEnemy, Selectedx, Selectedy, SelectedItem, SelectedOnFloor: Integer;
  current_turn, This_turn: Integer;

  MovementMapfor: Integer;
  nx1, nx2, ny1, ny2: Integer;

  recalculateLOS_Strategy, useLOS_Strategy, cheater_type: Boolean;
  Strategy_caution: Single;
  Strategy_cheater: Byte;
  Strategy_finishhim: Boolean;

  w_types, a_types: Integer;

  Txt: array[1..100] of string;

  FirstRun: Boolean;
  Infohash: Integer;
  PlayerID: Integer;

  MapWall_img, Mappass_img: Byte;
  Mapshade: array[1..2, 1..3] of single;

  DoExplosion: Boolean;
  Explosion_area: ^Maparray;
  DoHighlight: Boolean;
  HighlightX, HighlightY: Byte;
  AnimationTimer: TDateTime;

  txt_out: TStringList;
  txtX, txtY: Integer;
  Dotxt: Boolean;

  Info_Text: string;
  InfoAnimation: TDateTime;
  DoAnimate_Info: Boolean;

  Shot_sound, falcon_sound, unLoad_sound, Reload_sound, clip_sound,
  unclip_sound, Item_sound, Drop_sound: TSoundBuffer;

  Music: TSoundBuffer;
  MusicDuration: Double;
  oldMusic: Integer;


//  sintable,costable: array[0..MaxAngle] of Single;

implementation

{$R+}{$Q+}

{$R *.lfm}

{ TForm1 }

{-----------------------------}

function sgn(Value: Integer): shortint;
begin
  if Value > 0 then
    sgn := 1
  else
  if Value < 0 then
    sgn := -1
  else
    sgn := 0;
end;

{-----------------------------}

function RGB(intensity, r, g, b: Byte): Integer;
begin
  RGB := Round(r * intensity / 255) + 256 * Round(g * intensity / 255) +
    65536 * Round(b * intensity / 255);
end;

{================================================================================}
{================================================================================}
{================================================================================}

procedure TForm1.GenerateMovement_generic(fromX, fromY, toX, toY, maxdistance: Byte;
  Clearit, Playergo{,stealth}: Boolean);
var
  ix, iy: Integer;
  x1, x2, y1, y2: Integer;
  noMoves, distanceexceeded: Boolean;
  Movement_new: ^Maparray;
  distance_new: Integer;
begin
  New(Movement_new);
  if Clearit then
  begin
    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
      begin
        if Map^[ix, iy] < MapWall then
          Movement^[ix, iy] := 100
        else
          Movement^[ix, iy] := 255;
        distance^[ix, iy] := 255;
      end;

{    if stealth then
     for ix:=1 to MaxX do
        for iy:=1 to MaxY do if botVis^[ix,iy]=StrategyVisible then Movement^[ix,iy]:=253;
}
    if Playergo then
    begin
      for ix := 1 to MaxX do
        for iy := 1 to MaxY do
          if Vis^[ix, iy] = 0 then
            Movement^[ix, iy] := 253;

      for ix := 1 to NBot do
        if Bot[ix].HP > 0 then
        begin
          if Bot[ix].Owner = Player then
            Movement^[Bot[ix].X, Bot[ix].Y] := 254;
          if (Bot[ix].Owner = Computer) and (Vis^[Bot[ix].X, Bot[ix].Y] >
            OldVisible) then
            Movement^[Bot[ix].X, Bot[ix].Y] := 254;
        end;
    end
    else
      for ix := 1 to NBot do
        if Bot[ix].HP > 0 then
          Movement^[Bot[ix].X, Bot[ix].Y] := 254;

    Movement^[fromX, fromY] := 9;
    distance^[fromX, fromY] := 0;
    nx1 := fromX - 1;
    ny1 := fromY - 1;
    nx2 := fromX + 1;
    ny2 := fromY + 1;
    if nx1 <= 0 then
      nx1 := 1;
    if ny1 <= 0 then
      ny1 := 1;
    if nx2 > MaxX then
      nx2 := MaxX;
    if ny2 > MaxY then
      ny2 := MaxY;
  end;

  Movement_new^ := Movement^;

  distanceexceeded := false;
  repeat
    if maxdistance < 255 then
      distanceexceeded := true;
    noMoves := true;
    x1 := nx1;
    x2 := nx2;
    y1 := ny1;
    y2 := ny2;
    for ix := x1 to x2 do
      for iy := y1 to y2 do
        if Movement^[ix, iy] = 100 then
        begin
          if ix - 1 > 0 then
          begin
            if iy - 1 > 0 then
              if Movement^[ix - 1, iy - 1] < 10 then
              begin
                Movement_new^[ix, iy] := 7;
                distance_new := distance^[ix - 1, iy - 1] + DistanceStep;
              end;
            if iy + 1 <= MaxY then
              if Movement^[ix - 1, iy + 1] < 10 then
              begin
                Movement_new^[ix, iy] := 1;
                distance_new := distance^[ix - 1, iy + 1] + DistanceStep;
              end;
            if Movement^[ix - 1, iy] < 10 then
            begin
              Movement_new^[ix, iy] := 8;
              distance_new := distance^[ix - 1, iy] + DistanceAccuracy;
            end;
          end;
          if ix + 1 <= MaxX then
          begin
            if iy - 1 > 0 then
              if Movement^[ix + 1, iy - 1] < 10 then
              begin
                Movement_new^[ix, iy] := 5;
                distance_new := distance^[ix + 1, iy - 1] + DistanceStep;
              end;
            if iy + 1 <= MaxY then
              if Movement^[ix + 1, iy + 1] < 10 then
              begin
                Movement_new^[ix, iy] := 3;
                distance_new := distance^[ix + 1, iy + 1] + DistanceStep;
              end;
            if Movement^[ix + 1, iy] < 10 then
            begin
              Movement_new^[ix, iy] := 4;
              distance_new := distance^[ix + 1, iy] + DistanceAccuracy;
            end;
          end;
          if iy - 1 > 0 then
            if Movement^[ix, iy - 1] < 10 then
            begin
              Movement_new^[ix, iy] := 6;
              distance_new := distance^[ix, iy - 1] + DistanceAccuracy;
            end;
          if iy + 1 <= MaxY then
            if Movement^[ix, iy + 1] < 10 then
            begin
              Movement_new^[ix, iy] := 2;
              distance_new := distance^[ix, iy + 1] + DistanceAccuracy;
            end;


          if Movement_new^[ix, iy] <> 100 then
          begin
            noMoves := false;
            if distance_new > 255 then
              distance_new := 255;
            distance^[ix, iy] := distance_new;
            if (ix = nx1) and ((nx1 - 1) > 0) then
              nx1 := nx1 - 1;
            if (iy = ny1) and ((ny1 - 1) > 0) then
              ny1 := ny1 - 1;
            if (ix = nx2) and ((nx2 + 1) <= MaxX) then
              nx2 := nx2 + 1;
            if (iy = ny2) and ((ny2 + 1) <= MaxY) then
              ny2 := ny2 + 1;
            if (distance_new <= maxdistance) then
              distanceexceeded := false;
          end
          else
            distance^[ix, iy] := 255;
        end;
    Movement^ := Movement_new^;
  until (noMoves) or (distanceexceeded) or (Movement^[toX, toY] < 10);
  Dispose(Movement_new);
end;

procedure TForm1.GenerateMovement(Thisbot, targetX, targetY: Integer);
// Generatenewmap,target
begin
  if (targetX > 0) and (targetX <= MaxX) and (targetY > 0) and (targetY <= MaxY) then
  begin
    GenerateMovement_generic(Bot[Thisbot].X, Bot[Thisbot].Y, targetX,
      targetY, 255, MovementMapfor <> Thisbot, Bot[Thisbot].Owner = Player{,false});
    MovementMapfor := Thisbot;
  end;
end;

{================================================================================}
{================================================================================}
{================================================================================}

function checkLOS(x1, y1, x2, y2: Integer; Smoker: Boolean): Integer;
var
  dx, dy: Integer;
  xx1, yy1: Integer;
  Range, maxRange: Integer;
  L, L1: Integer;
begin
  L := -1;
  // L1:=-1;
  if (x1 > 0) and (y1 > 0) and (x2 > 0) and (y2 > 0) and (x1 <= MaxX) and
    (y1 <= MaxY) and (x2 <= MaxX) and (y2 <= MaxY) then
  begin
    dx := x2 - x1;
    dy := y2 - y1;
    maxRange := Round(VisibleAccuracy * Sqrt(Sqr(dx) + Sqr(dy))) + 1;
    L := VisibleRange * VisibleAccuracy;
    Range := 0;
    repeat
      Inc(Range);
      xx1 := x1 + Round(dx * Range / maxRange);
      yy1 := y1 + Round(dy * Range / maxRange);
      if Smoker = true then
      begin
        if Map^[xx1, yy1] < MapWall then
          Dec(L, MapStatus^[xx1, yy1] + 1);
      end
      else
      if Map^[xx1, yy1] < MapWall then
        Dec(L, 2);
      if (Map^[xx1, yy1] >= MapWall) and ((xx1 <> x2) or (yy1 <> y2)) then
        L := -1;
    until (Range >= maxRange) or (L < 1);

    //stupid bugfix for los(x1,x2)<>los(x2,x1)
    if L < 1 then
    begin
      dx := x1 - x2;
      dy := y1 - y2;
      maxRange := Round(VisibleAccuracy * Sqrt(Sqr(dx) + Sqr(dy))) + 1;
      L1 := VisibleRange * VisibleAccuracy;
      Range := 0;
      repeat
        Inc(Range);
        xx1 := x2 + Round(dx * Range / maxRange);
        yy1 := y2 + Round(dy * Range / maxRange);
        if Smoker = true then
        begin
          if Map^[xx1, yy1] < MapWall then
            Dec(L1, MapStatus^[xx1, yy1] + 1);
        end
        else
        if Map^[xx1, yy1] < MapWall then
          Dec(L1, 2);
        if (Map^[xx1, yy1] >= MapWall) and ((xx1 <> x1) or (yy1 <> y1)) then
          L1 := -1;
      until (Range >= maxRange) or (L1 < 1);
      L := L1;
    end;
  end;
  checkLOS := L;
end;

{================================================================================}
{================================================================================}
{================================================================================}

function TForm1.Generate_Enemy_List(messageflag: Boolean): Boolean;
var
  i, j: Integer;
  EnemyUnits_new: Integer;
begin
  EnemyUnits_new := 0;
  for i := 1 to NBot do
    if (Bot[i].Owner = Computer) and (Bot[i].HP > 0) and
      (EnemyUnits_new < MaxUnitsList) then
      if Vis^[Bot[i].X, Bot[i].Y] > OldVisible then
      begin
        Inc(EnemyUnits_new);
        EnemyUnits[EnemyUnits_new] := i;
        if Bot[i].Action = ActionRandom then
        begin
          Bot[i].Action := ActionAttack;
          for j := 1 to NBot do
            if (Bot[j].Owner = Player) and (Bot[j].HP > 0) then
              Bot[i].target := j;
          alert_other_Bots(Bot[i].target, i);
        end;
      end;
  if EnemyUnits_new <> EnemyUnitsn then
  begin
    Generate_Enemy_List := true;
    if (EnemyUnits_new > EnemyUnitsn) and (messageflag) and (This_turn = Player) then
    begin
      DrawMap;
      ShowMessage(Txt[1]);
    end;
  end
  else
    Generate_Enemy_List := false;
  EnemyUnitsn := EnemyUnits_new;
end;

{================================================================================}
{================================================================================}
{================================================================================}


procedure TForm1.look_aRound(Thisbot: Integer);
var //visRange:Integer;
  ix, iy: Integer;
  LOS: Integer;
begin
  if (Bot[Thisbot].Owner = Player) and (Bot[Thisbot].HP > 0) then
  begin
    for ix := Bot[Thisbot].X - VisibleRange to Bot[Thisbot].X + VisibleRange do
      if (ix > 0) and (ix <= MaxX) then
        for iy := Bot[Thisbot].Y - VisibleRange to Bot[Thisbot].Y + VisibleRange do
          if (iy > 0) and (iy <= MaxY) then
          begin
            LOS := checkLOS(Bot[Thisbot].X, Bot[Thisbot].Y, ix, iy, true);
            if LOS > 0 then
            begin
              LOS := Round((MaxVisible - OldVisible) * LOS /
                (VisibleRange * VisibleAccuracy)) + OldVisible;
              if Vis^[ix, iy] < LOS then
              begin
                Vis^[ix, iy] := LOS;
                MapChanged^[ix, iy] := 255;
                if (ix = 2) then
                begin
                  if (Vis^[1, iy] = 0) then
                  begin
                    Vis^[1, iy] := OldVisible;
                    MapChanged^[1, iy] := 255;
                  end;
                end
                else if (ix = MaxX - 1) then
                begin
                  if (Vis^[MaxX, iy] = 0) then
                  begin
                    Vis^[MaxX, iy] := OldVisible;
                    MapChanged^[MaxX, iy] := 255;
                  end;
                end;
                if (iy = 2) then
                begin
                  if (Vis^[ix, 1] = 0) then
                  begin
                    Vis^[ix, 1] := OldVisible;
                    MapChanged^[ix, 1] := 255;
                  end
                  else;
                end
                else if (iy = MaxY - 1) then
                begin
                  if (Vis^[ix, MaxY] = 0) then
                  begin
                    Vis^[ix, MaxY] := OldVisible;
                    MapChanged^[ix, MaxY] := 255;
                  end;
                end;
              end;

            end;
          end;
  end;
end;

procedure TForm1.Botlook_aRound(Thisbot: Integer);
var
  ix, iy: Integer;
  LOS: Integer;
  PlayerBot: Integer;
begin
  if (Bot[Thisbot].HP > 0) then
  begin
    for ix := Bot[Thisbot].X - VisibleRange to Bot[Thisbot].X + VisibleRange do
      if (ix > 0) and (ix <= MaxX) then
        for iy := Bot[Thisbot].Y - VisibleRange to Bot[Thisbot].Y + VisibleRange do
          if (iy > 0) and (iy <= MaxY) then
          begin
            LOS := checkLOS(Bot[Thisbot].X, Bot[Thisbot].Y, ix, iy, true);
            if (Bot[Thisbot].Owner = Computer) then
            begin
              if LOS > 0 then
              begin
                if (botVis^[ix, iy] = Strategy_PossibleLoc) then
                  recalculateLOS_Strategy := true;
                if cheater_type = Strategy_TypeCheater then
                  for PlayerBot := 1 to NBot do
                    if Bot[PlayerBot].Owner = Player then
                      if (Bot[PlayerBot].X = ix) and (Bot[PlayerBot].Y = iy) then
                        Botlook_aRound(PlayerBot);
                if botVis^[ix, iy] < Strategy_RealLOS then
                  botVis^[ix, iy] := Strategy_Visible;
              end;
            end
            else
            if LOS > 0 then
              botVis^[ix, iy] := Strategy_RealLOS;
          end;
  end;
end;

{================================================================================}
{================================================================================}
{================================================================================}

procedure TForm1.find_OnFloor(x, y: Integer);
var
  i: Integer;
begin
  OnFloorn := 0;
  if Itemsn > 0 then
    for i := 1 to Itemsn do
      if (x = Item[i].X) and (y = Item[i].Y) then
        if OnFloorn < MaxOnFloor then
        begin
          Inc(OnFloorn);
          OnFloor[OnFloorn] := i;
        end;
end;

{================================================================================}
{================================================================================}
{================================================================================}

function TForm1.SpendTU(Thisbot: Integer; tus: Integer): Boolean;
var
  oldtu: Byte;
begin
  if (Bot[Thisbot].TU >= tus) or (GameMode = GameMode_Victory) then
  begin
    oldtu := Bot[Thisbot].TU;
    if GameMode <> GameMode_Victory then
      Dec(Bot[Thisbot].TU, tus);
    if Bot[Thisbot].Items[1].RechargeState > tus then
      Dec(Bot[Thisbot].Items[1].RechargeState, tus)
    else
      Bot[Thisbot].Items[1].RechargeState := 0;

    if (Bot[Thisbot].Owner = Player) then
      if ((oldtu >= Strategy_cheater) and (Bot[Thisbot].TU <= Strategy_Cheater)) or
        (botVis^[Bot[Thisbot].X, Bot[Thisbot].Y] = Strategy_Visible) or
        (botVis^[Bot[Thisbot].X, Bot[Thisbot].Y] = Strategy_RealLOS) then
      begin
        Bot[Thisbot].LastSeenX := Bot[Thisbot].X;
        Bot[Thisbot].LastSeenY := Bot[Thisbot].Y;
        Bot[Thisbot].lastseen_tu := oldtu;
        if DebugMode then
          Memo1.Lines.Add('[dbg/spend] ' + Bot[Thisbot].Name +
            ' lastseen: ' + IntToStr(Bot[Thisbot].LastSeenX) + '.' +
            IntToStr(Bot[Thisbot].LastSeenY) + ' / ' + IntToStr(
            Bot[Thisbot].lastseen_tu) + ' tu');
      end;

    SpendTU := true;
  end
  else
  begin
    SpendTU := false;
    if Bot[Thisbot].Owner = Player then
    begin
      if tus <= 255 then
        ShowMessage(Txt[2])
      else
        ShowMessage(Txt[3]);
    end;
  end;
end;

{=================================================================}
{=================================================================}
{=================================================================}

const
  maxwaypoints = 1000;

var
  waypointx, waypointy: array[1..Maxwaypoints] of Integer;
  waypointn: Integer;
  waypoint_Count: Integer;
//    waypoint_distance:Integer;
procedure Generatewaypoints(Thisbot, toX, toY: Integer);
var
  xx1, yy1: Integer;
begin
  waypointn := 0;
  xx1 := toX;
  yy1 := toY;
  //  waypoint_distance:=0;
  repeat
    Inc(waypointn);
    waypointx[waypointn] := xx1;
    waypointy[waypointn] := yy1;

    case Movement^[xx1, yy1] of
      5:
      begin
        Inc(xx1);
        Dec(yy1);
      end;
      6:
      begin
        Dec(yy1);
      end;
      7:
      begin
        Dec(xx1);
        Dec(yy1);
      end;
      8:
      begin
        Dec(xx1);
      end;
      1:
      begin
        Dec(xx1);
        Inc(yy1);
      end;
      2:
      begin
        Inc(yy1);
      end;
      3:
      begin
        Inc(xx1);
        Inc(yy1);
      end;
      4:
      begin
        Inc(xx1);
      end;
    end;

    //    inc(waypoint_distance,Round(Bot[Thisbot].Speed*Sqrt(Sqr(xx1-waypointx[waypointn])+Sqr(yy1-waypointy[waypointn]))));
  until ((xx1 = Bot[Thisbot].X) and (yy1 = Bot[Thisbot].Y)) or
    (waypointn >= maxwaypoints);
end;

procedure TForm1.Move_bot(Thisbot, toX, toY, use_waypoints: Integer);
var
  i: Integer;
  oldx, oldy: Integer;
  step_tu: Byte;
  dx: Integer;
  Flg: Boolean;

  myTimer: TDate;
begin
  GenerateMovement(Thisbot, toX, toY);
  oldx := Bot[Thisbot].X;
  oldy := Bot[Thisbot].Y;
  if (toX > 0) and (toY > 0) and (toX <= MaxX) and (toY <= MaxY) then
    if Movement^[toX, toY] < 10 then
    begin
      Generatewaypoints(Thisbot, toX, toY);

      waypoint_Count := 0;
      repeat
        step_tu := Round(Bot[Thisbot].Speed * Sqrt(
          Sqr(Bot[Thisbot].X - waypointx[waypointn]) +
          Sqr(Bot[Thisbot].Y - waypointy[waypointn])));
        Flg := true;
        for i := 1 to NBot do
          if (Bot[i].HP > 0) and (i <> Thisbot) then
            if (Bot[i].X = waypointx[waypointn]) and (Bot[i].Y = waypointy[waypointn])
            then //begin
              Flg := false;
        //        waypointn:=0;
        //      end;
        if Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] > OldVisible then
          MapChanged^[Bot[Thisbot].X, Bot[Thisbot].Y] := 255;
        if (Bot[Thisbot].TU >= step_tu) and (Flg) then
        begin
          SpendTU(Thisbot, step_tu);

          if Bot[Thisbot].Y - waypointy[waypointn] <> 0 then
          begin
            dx := Round(MaxAngle * arctan(
              (Bot[Thisbot].X - waypointx[waypointn]) / (Bot[Thisbot].Y -
              waypointy[waypointn])) / 2 / Pi);
            if Bot[Thisbot].Y - waypointy[waypointn] > 0 then
              dx := MaxAngle * 3 div 4 - dx
            else
              dx := MaxAngle div 4 - dx;
            Bot[Thisbot].Angle := dx;
          end
          else
          begin
            if Bot[Thisbot].X - waypointx[waypointn] > 0 then
              Bot[Thisbot].Angle := MaxAngle div 2
            else
              Bot[Thisbot].Angle := 0;
          end;

          Bot[Thisbot].X := waypointx[waypointn];
          Bot[Thisbot].Y := waypointy[waypointn];
          if Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] > OldVisible then
            MapChanged^[Bot[Thisbot].X, Bot[Thisbot].Y] := 255;
          i := 3;   //buggy
          if (ViewX <= 1) or (ViewY <= 1) or (ViewX + ViewSizeX >= MaxX - 2) or
            (ViewY + ViewSizeY >= MaxY - 2) then
            i := 1;
          if (Bot[Thisbot].X <= ViewX + i) or (Bot[Thisbot].Y <= ViewY + i) or
            (Bot[Thisbot].X >= ViewX + ViewSizeX + 1 - i) or
            (Bot[Thisbot].Y >= ViewY + ViewSizeY + 1 - i) then
            if Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] > OldVisible then
              CenterMap(Bot[Thisbot].X, Bot[Thisbot].Y);

          if Bot[Thisbot].Owner = Player then
          begin
            look_aRound(Thisbot);
            if botVis^[oldx, oldy] > 0 then
            begin
              Bot[Thisbot].LastSeenX := Bot[Thisbot].X;
              Bot[Thisbot].LastSeenY := Bot[Thisbot].Y;
              Bot[Thisbot].lastseen_tu := Bot[Thisbot].TU;
              if DebugMode then
                Memo1.Lines.Add('[dbg/Move] ' + Bot[Thisbot].Name +
                  ' lastseen: ' + IntToStr(Bot[Thisbot].LastSeenX) +
                  '.' + IntToStr(Bot[Thisbot].LastSeenY) + ' / ' +
                  IntToStr(Bot[Thisbot].lastseen_tu) + ' tu');
            end;
            if Generate_Enemy_List(true) then
              waypointn := 0;
          end
          else
          begin
            if (cheater_type = Strategy_TypeCheater) then
              Botlook_aRound(Thisbot);
          end;
          if (Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] > OldVisible) or
            (Bot[Thisbot].Owner = Player) then
          begin
            myTimer := Now;
            DrawMap;
            dx := Round((Now - myTimer) * 24 * 60 * 60 * 1000);
            if Bot[Thisbot].Owner = Player then
            begin
              if PlayerBotMoveDelay > dx then
                sleep(PlayerBotMoveDelay - dx);
            end
            else
            begin
              if EnemyBotMoveDelay > dx then
                sleep(EnemyBotMoveDelay - dx);
            end;
          end;
          Inc(waypoint_Count);
          Dec(waypointn);
        end
        else
          waypointn := 0;
      until (waypointn <= 0) or (waypoint_Count >= use_waypoints);
      /// or interrupted by Time lack or Enemy
      if (Bot[Thisbot].X <> oldx) or (Bot[Thisbot].Y <> oldy) then
        MovementMapfor := -1;
      if (Thisbot = Selected) and (CheckBox2.Checked) and
        ((Bot[Thisbot].X <> toX) or (Bot[Thisbot].Y <> toY)) then
        GenerateMovement(Thisbot, toX, toY);
    end;
{ if Vis^[Bot[Thisbot].X,Bot[Thisbot].Y]>OldVisible then begin
   DrawMap;
 end;   }
end;

{================================================================================}
{================================================================================}
{================================================================================}

{$INCLUDE MapGenerators.inc}

procedure Create_bunker(bx, by, bSizex, bSizey: Integer);
var
  ix, iy: Integer;
begin
  for ix := bx - 3 to bx + bSizex + 2 do
    if (ix > 1) and (ix < MaxX) then
      for iy := by - 3 to by + bSizey + 2 do
        if (iy > 1) and (iy < MaxY) then
        begin
          if (((ix = bx - 1) or (ix = bx + bSizex)) and (iy >= by - 1) and
            (iy <= by + bSizey)) or (((iy = by - 1) or (iy = by + bSizey)) and
            (ix >= bx - 1) and (ix <= bx + bSizex)) then
            Map^[ix, iy] := MapGeneration_Wall
          else
            Map^[ix, iy] := MapGeneration_Free;
        end;

  SafeMapWritefinal(bx + bSizex div 2 - 1, by - 1, MapGeneration_Free);
  SafeMapWritefinal(bx + bSizex div 2, by + bSizey, MapGeneration_Free);
  SafeMapWritefinal(bx - 1, by + bSizey div 2 - 1, MapGeneration_Free);
  SafeMapWritefinal(bx + bSizex, by + bSizey div 2, MapGeneration_Free);

  SafeMapWritefinal(bx + bSizex div 2 - 1, by - 3, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex div 2 - 2, by - 3, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex div 2, by - 3, MapGeneration_Wall);

  SafeMapWritefinal(bx + bSizex div 2 + 1, by + bSizey + 2, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex div 2 - 1, by + bSizey + 2, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex div 2, by + bSizey + 2, MapGeneration_Wall);

  SafeMapWritefinal(bx - 3, by + bSizey div 2 - 1, MapGeneration_Wall);
  SafeMapWritefinal(bx - 3, by + bSizey div 2 - 2, MapGeneration_Wall);
  SafeMapWritefinal(bx - 3, by + bSizey div 2, MapGeneration_Wall);

  SafeMapWritefinal(bx + bSizex + 2, by + bSizey div 2 + 1, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex + 2, by + bSizey div 2 - 1, MapGeneration_Wall);
  SafeMapWritefinal(bx + bSizex + 2, by + bSizey div 2, MapGeneration_Wall);

  if DebugMode then
    Form1.Memo1.Lines.Add('Bunker Building Created.');
end;

{-----------------------------------------------------}

var
  bldg_area: Integer;

procedure Create_box(limitSize: Integer);
var
  x1, y1, x2, y2: Integer;
  ix, iy: Integer;
begin
  x1 := Round(Random * (MaxX - 8)) + 1;
  x2 := x1 + Round(Sqr(Sqr(Random)) * ((MaxX - x1))) + 4;
  if x2 >= MaxX then
    x2 := MaxX - 1;
  if x2 - x1 > limitSize then
    x2 := x1 + limitSize;
  y1 := Round(Random * (MaxY - 8)) + 1;
  y2 := y1 + Round(Sqr(Sqr(Random)) * ((MaxY - y1))) + 4;
  if y2 >= MaxY then
    y2 := MaxY - 1;
  if y2 - y1 > limitSize then
    y2 := y1 + limitSize;
  for ix := x1 to x2 do
    for iy := y1 to y2 do
      SafeMapWritefinal(ix, iy, MapGeneration_Free);
  for ix := x1 + 1 to x2 - 1 do
    for iy := y1 + 1 to y2 - 1 do
      SafeMapWritefinal(ix, iy, MapGeneration_Wall);
  for ix := x1 + 2 to x2 - 2 do
    for iy := y1 + 2 to y2 - 2 do
      SafeMapWritefinal(ix, iy, MapGeneration_Free);
  if Random > 0.5 then
  begin
    if Random > 0.5 then
      x1 := x1 + 1
    else
      x1 := x2 - 1;
    SafeMapWritefinal(x1, y1 + Round(Random * (y2 - y1 - 4)) + 2, MapGeneration_Free);
  end
  else
  begin
    if Random > 0.5 then
      y1 := y1 + 1
    else
      y1 := y2 - 1;
    SafeMapWritefinal(x1 + Round(Random * (x2 - x1 - 4)) + 2, y1, MapGeneration_Free);
  end;
  Inc(bldg_area, (x2 - x1 + 1) * (y2 - y1 + 1));

  if DebugMode then
    Form1.Memo1.Lines.Add('Box Building Created.');
end;

{----------------------------------------------------------------}

const
  box_probability = 0.3;
//      bunker_probabioity=0.4;
//      bldg_probability=0.5;
procedure GenerateMapBuildings;
var
  f1: File of Byte;
  ix, iy, jx, jy, tmp: Integer;
  x1, y1: Integer;
  bldgx, bldgy: Byte;
  tmpmap, tmprnd: array[1..MaxBuildingX, 1..MaxBuildingY] of Byte;
  s1: string;
  Value: Byte;
  Flg: Boolean;
  Seed1, Seed2, Seed3: Single;

  MapSeed: Single;
begin
  if (Form1.CheckBox6.Checked) then
  begin
    if MapParameter[0] = -1 then
      MapSeed := Random * 0.3
    else
      MapSeed := MapParameter[0];
    bldg_area := 0;

    if (Random < box_probability) then
      repeat
        Create_box(MinMaxValue div 3)
      until Random > box_probability;
{   if (Random<bunker_probability) then
     repeat Create_bunker(Round(Random*(MaxX-8)),Round(Random*(MaxY-8)),Round(Random*4)+1,Round(Random*4)+1) until Random>bunker_probability;}

    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
        Vis^[ix, iy] := 0;

    if {(Random<bldg_probability) and} (nBuildings > 0) then
      repeat
        begin
          // Memo1.Lines.Add(FileName);
          s1 := mapBuildings[1 + Round(Random * (nBuildings - 1))];
          AssignFile(f1, ExtractFilePath(application.ExeName) +
            DataFolder + 'BLDG' + PathDelim + s1);
          Reset(f1);

          Read(f1, bldgx);
          Read(f1, bldgy);
          //       Form1.Memo1.Lines.Add(IntToStr(bldgx)+' x '+IntToStr(bldgy) + ' / ' + IntToStr(MinMaxValue));
          if (MinMaxValue > bldgx + 2) and (MinMaxValue > bldgy + 2) then
          begin
            Seed1 := Random;
            Seed2 := Random;
            Seed3 := Random;

            for ix := 1 to bldgx do
              for iy := 1 to bldgy do
                Read(f1, tmpmap[ix, iy]);
            for ix := 1 to bldgx do
              for iy := 1 to bldgy do
                Read(f1, tmprnd[ix, iy]);

            Value := 0;
            repeat
              Inc(Value);
              x1 := Round(Random * (MaxX - bldgx - 4) + 2);
              y1 := Round(Random * (MaxY - bldgy - 4) + 2);
              Flg := true;
              for ix := x1 to x1 + bldgx do
                for iy := y1 to y1 + bldgy do
                  if Vis^[ix, iy] > 0 then
                    Flg := false;
            until (Flg) or (Value > 50);

            if Flg then
            begin
              if DebugMode then
                Form1.Memo1.Lines.Add('Adding Building ' + s1);
              for ix := 1 to bldgx do
                for iy := 1 to bldgy do
                  if tmpmap[ix, iy] > 0 then
                  begin
                    if Seed1 < 0.5 then
                      jx := ix
                    else
                      jx := bldgx - ix + 1;
                    if Seed2 < 0.5 then
                      jy := iy
                    else
                      jy := bldgy - iy + 1;
                    if Seed3 < 0.5 then
                    begin
                      tmp := jy;
                      jy := jx;
                      jx := tmp;
                    end;
                    jx := jx + x1 - 1;
                    jy := jy + y1 - 1;
                    if Random < tmprnd[ix, iy] / RandomAccuracy then
                      Map^[jx, jy] := MapGeneration_Wall
                    else
                      Map^[jx, jy] := MapGeneration_Free;
                    Vis^[jx, jy] := 1;
                  end;
            end;
            // Read(f1,0);  //EntranceX
            // Read(f1,0);  //EntranceY
          end;
          CloseFile(f1);
        end;
      until (bldg_area < (MaxX - 2) * (MaxY - 2) * MapSeed) or (Random < 0.01);
  end;
end;

{----------------------------------------------------------------}

const
  enterX = 2;
  enterY = 2;

const
  homogenityX = 3;
  homogenityY = 3;

function TestMap(Free_from, Free_to: Byte): Boolean;
var
  ix, iy, dx, dy: Integer;
  nerrors, nFree: Integer;
  all_Count, Free_Count: Integer;
  deviation: Single;
  ThismapFloor, ThismapWall: Byte;
begin
  Map^ := MapG^;

  GenerateMapBuildings;
  Create_bunker(enterX, enterY, 4, 4);

  //Entrance
  for ix := enterX to enterX + 3 do
    for iy := enterY to enterY + 3 do
      Map^[ix, iy] := MapFree;

  if Random_tiles then
    repeat
      case Trunc(Random * 4) of
        0: ThismapFloor := 0;
        1: ThismapFloor := 1;
        2: ThismapFloor := 2;
        3: ThismapFloor := 3;
      end;
      case Trunc(Random * 4) of
        0: ThismapWall := 0;
        1: ThismapWall := 1;
        2: ThismapWall := 2;
        3: ThismapWall := 3;
      end;
    until ThismapFloor <> ThismapWall;

  repeat
    nFree := 0;
    for ix := 2 to MaxX - 1 do
      for iy := 2 to MaxY - 1 do
      begin
        MapStatus^[ix, iy] := 0;
        if Map^[ix, iy] >= MapGeneration_Free then
          for dx := -1 to 1 do
            for dy := -1 to 1 do
              if Map^[ix + dx, iy + dy] < MapWall then
              begin
                if Random_tiles then
                  Map^[ix, iy] := MapFree + ThismapFloor
                else
                begin
                  if Map^[ix, iy] >= MapGeneration_Free then
                    Map^[ix, iy] := MapFree + (Map^[ix, iy] - MapGeneration_Free);
                end;
                nFree := 1;
              end;

      end;
  until nFree = 0;

  nerrors := 0;
  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
      if Map^[ix, iy] >= MapGeneration_Free then
      begin
        Map^[ix, iy] := MapGeneration_Wall;
        Inc(nerrors);
      end;

  nFree := 0;
  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
    begin
      if Map^[ix, iy] < MapWall then
        Inc(nFree);
      if Map^[ix, iy] >= MapGeneration_Wall then
      begin
        if Random_tiles then
          Map^[ix, iy] := MapWall + ThismapWall
        else
          Map^[ix, iy] := MapWall + (Map^[ix, iy] - MapGeneration_Wall);
        MapStatus^[ix, iy] := MaxStatus - Round(Sqr(Random) * MaxStatus / 1.5);
      end;
    end;



  if DebugMode then
    Form1.Memo1.Lines.Add('Map error rate: ' +
      IntToStr(Round(nerrors / MaxX / MaxY * 100)) + '%');
  mapFreeText := Txt[29] + ': ' + IntToStr(Round(nFree / MaxX / MaxY * 100)) + '%';

  //homogenity check
  deviation := 0;
  for ix := 1 to homogenityX do
    for iy := 1 to homogenityY do
    begin
      Free_Count := 0;
      all_Count := 0;
      for dx := Round((ix - 1) * (MaxX / homogenityX)) +
        1 to Round(ix * (MaxX / homogenityX)) do
        for dy := Round((iy - 1) * (MaxY / homogenityY)) +
          1 to Round(iy * (MaxY / homogenityY)) do
        begin
          if Map^[dx, dy] < MapWall then
            Inc(Free_Count);
          Inc(all_Count);
        end;
      deviation := deviation + Sqr((Free_Count / all_Count) / (nFree / MaxX / MaxY) - 1);
    end;
  MapInhomogeneity := Sqrt(deviation) / (homogenityX * homogenityY);

  if DebugMode then
  begin
    Form1.Memo1.Lines.Add('Map inhomogenity: ' +
      IntToStr(Round(MapInhomogeneity * 100)) + '%');
    Form1.Memo1.Lines.Add(MapGenerationText);
    Form1.Memo1.Lines.Add(mapFreeText);
    Form1.Memo1.Lines.Add('...');
  end;

  if (nFree / MaxX / MaxY > Free_from / 100) and (nFree / MaxX /
    MaxY < Free_to / 100) and (MapInhomogeneity < 0.15) then
    TestMap := true
  else
    TestMap := false; {(nerrors/MaxX/MaxY<0.2) and (nerrors<nFree/3) and}
end;

{--------------------------------------------------------------------------------------}

function Createbot(Owner: Integer; Name: string; MaxHP, x, y: Integer): Boolean;
var
  i, j, Weaponhp: Integer;
  Flg: Boolean;
  Weapon_Kind, GeneratedWeaponType{,Ammo_Kind}, AmmoType: Integer;
  Ammo_Usable: Boolean;
begin
  Inc(NBot);
  Bot[NBot].Name := Name;
  Bot[NBot].MaxHP := MaxHP;
  Bot[NBot].HP := Bot[NBot].MaxHP;
  Bot[NBot].X := x;
  Bot[NBot].Y := y;

  Bot[NBot].LastSeenX := x;
  Bot[NBot].LastSeenY := y;
  Bot[NBot].lastseen_tu := 255;

  Bot[NBot].Owner := Owner;
  Bot[NBot].Speed := 30;
  Bot[NBot].Angle := Round(Random * MaxAngle);

  Bot[NBot].BType := 0;
  if Bot[NBot].Owner <> Player then
  begin
    Bot[NBot].caution := Round((Random) * 2 * Bot[NBot].Speed * Strategy_caution);
    Bot[NBot].BType := 2;
    if Random < 0.2 then
    begin
      Bot[NBot].MaxHP := 2 * Bot[NBot].MaxHP div 3;
      Bot[NBot].HP := Bot[NBot].MaxHP;
      Bot[NBot].Speed := 25;
      Bot[NBot].caution := Round((Random) * 3 * Bot[NBot].Speed * Strategy_caution);
      Bot[NBot].BType := 4;
    end
    else
    if Random < 0.15 then
    begin
      Bot[NBot].MaxHP := Bot[NBot].MaxHP * 3;
      Bot[NBot].HP := Bot[NBot].MaxHP;
      Bot[NBot].Speed := 40;
      Bot[NBot].caution := Round(Sqr(Random) * Bot[NBot].Speed * Strategy_caution);
      Bot[NBot].Name := Name + '(H)';
      Bot[NBot].BType := 3;
    end
    else
    if Random < 0.25 then
    begin
      Bot[NBot].MaxHP := Bot[NBot].MaxHP div 2;
      Bot[NBot].HP := Bot[NBot].MaxHP;
      Bot[NBot].Speed := 20;
      Bot[NBot].caution := Round(
        (3 * Bot[NBot].Speed + (Sqrt(Random) * 3 * Bot[NBot].Speed)) * Strategy_caution);
      Bot[NBot].Name := Name + '(Q)';
      Bot[NBot].BType := 1;
    end;

  end;

  for i := 2 to BackpackSize do
  begin
    Bot[NBot].Items[i].W_ID := 0;
    Bot[NBot].Items[i].Ammo_ID := 0;
  end;
  i := 0;
  Weaponhp := 0;
  repeat
    Inc(i);
    Weapon_Kind := 1;
    if Bot[NBot].Owner <> Player then
    begin
      if Random < 0.3 then
        Weapon_Kind := 2;
      if Random < 0.05 then
        Weapon_Kind := 3;
    end;
    if Form1.CheckBox3.Checked then
      Weapon_Kind := 3;
    repeat
      GeneratedWeaponType := Trunc(w_types * Random) + 1;
    until (Random * 100 < WeaponSpecifications[GeneratedWeaponType].rnd) and
      (WeaponSpecifications[GeneratedWeaponType].Kind = Weapon_Kind);
    Bot[NBot].Items[i].W_ID := GeneratedWeaponType;

    Bot[NBot].Items[i].MaxState :=
      WeaponSpecifications[Bot[NBot].Items[i].W_ID].MaxState - Round(
      0.1 * WeaponSpecifications[Bot[NBot].Items[i].W_ID].MaxState * Random);
    Bot[NBot].Items[i].State :=
      Round((Bot[NBot].Items[i].MaxState - 0.2 * Bot[NBot].Items[i].MaxState * Random) *
      (11 / (i + 10)));
    Bot[NBot].Items[i].RechargeState := 0;
    Inc(Weaponhp, Bot[NBot].Items[i].State);

    repeat
      AmmoType := Trunc(a_types * Random) + 1;
      Ammo_Usable := false;
      for j := 1 to maxUsableAmmo do
        if WeaponSpecifications[GeneratedWeaponType].Ammo[j] = AmmoType then
          Ammo_Usable := true;
      if (AmmoSpecifications[AmmoType].Kind <> 1) and
        (Bot[NBot].Owner = Player) and (not Form1.CheckBox3.Checked) then
        Ammo_Usable := false;
    until (Random * 100 < AmmoSpecifications[AmmoType].rnd) and (Ammo_Usable)
    {and (WeaponSpecifications[WeaponType].Kind=Weapon_Kind)};
    Bot[NBot].Items[i].Ammo_ID := AmmoType;
    Bot[NBot].Items[i].N := AmmoSpecifications[Bot[NBot].Items[i].Ammo_ID].Quantity;
  until (Bot[NBot].HP * ItemDamageRate < Weaponhp) or (i = BackpackSize) or
    (Bot[NBot].Owner = Player);

  if Map^[Bot[NBot].X, Bot[NBot].Y] < MapWall then
    Flg := true
  else
    Flg := false;
  if (Flg) and (NBot > 1) then
    for i := 1 to NBot - 1 do
      if (Bot[i].HP > 0) and (Bot[NBot].X = Bot[i].X) and (Bot[NBot].Y = Bot[i].Y) then
        Flg := false;
  if not Flg then
    Dec(NBot);
  Createbot := Flg;
end;

{-----------------------------------------------------------------------------------------------}

procedure GenerateLOS_baseMap{(all:Boolean;losX,losY:Integer)};
var
  ix, iy, dx, dy, Count: Integer;
  tmp_bar: Integer;
begin
  reGenerateLOS := false;
  if DebugMode then
    Form1.Memo1.Lines.Add('Generating LOS base map...');
  Form1.set_ProgressBar(true);
  tmp_bar := Form1.ProgressBar1.Max;
  Form1.ProgressBar1.Max := MaxX;


  for ix := 1 to MaxX do
  begin
    for iy := 1 to MaxY do
      if (LOS_base^[ix, iy] = 255) and (Map^[ix, iy] < MapWall) then
      begin
        Count := 0;
        for dx := ix - VisibleRange to ix + VisibleRange do
          for dy := iy - VisibleRange to iy + VisibleRange do
            if (dx > 0) and (dy > 0) and (dx <= MaxX) and (dy <= MaxY) then
              if Map^[dx, dy] < MapWall then
                if (checkLOS(ix, iy, dx, dy, false) > 0) then
                  Inc(Count);
        if Count > 1 then
          Dec(Count);
        if Count < 254 then
          LOS_base^[ix, iy] := Count
        else
          LOS_base^[ix, iy] := 254;

      end
      else
        LOS_base^[ix, iy] := 254;
    Form1.ProgressBar1.Position := ix;
    Form1.ProgressBar1.update;
  end;

  dx := 0;
  mapFreespace := 0;
  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
      if Map^[ix, iy] < MapWall then
      begin
        Inc(dx, LOS_base^[ix, iy]);
        Inc(mapFreespace);
      end;
  averageLOS := dx / mapFreespace;

  Form1.ProgressBar1.Max := tmp_bar;
  if This_turn = Player then
    Form1.set_ProgressBar(false);
  //Dispose(tmpMap);
end;

{--------------------------------------------------------------------------------------}

function calculate_Difficulty(Players, Playershp, enemies, enemieshp,
  maaxx, maaxy, mapFreex: Integer): Integer;
var
  estimated_firepower: Single;
  Difficultybonus: Integer;
  LOS_adjusted: Single;
begin
  LOS_adjusted := CallReInforcementsRange * Pi * mapFreex / ((maaxx - 2) * (maaxy - 2));
  if averageLOS > LOS_adjusted then
    LOS_adjusted := averageLOS;

  if Form1.CheckBox1.Checked then
    Difficultybonus := DefenseDifficulty
  else
    Difficultybonus := 1;
  estimated_Botstogether := enemies * LOS_adjusted / mapFreex;
  if estimated_Botstogether < 1 then
    estimated_Botstogether := 1;
  estimated_firepower := estimated_Botstogether / Players;
  estimated_firepower := estimated_firepower * (1 + enemies * 0.25 *
    5 * StandardDamage / Playershp);
  {Random Damage}
  estimated_firepower := estimated_firepower *
    (1 + 5 * StandardDamage * (estimated_Botstogether * enemieshp /
    enemies / (Players * StandardDamage * 5)) / Playershp);
  {persistent Damage}
  calculate_Difficulty := Round(100 * enemieshp / Playershp *
    Difficultybonus * estimated_firepower);
end;

function sayDifficulty(Difficulty: Integer): string;
  //var winchance:Integer;
begin
  case Difficulty of
    0.. 49: sayDifficulty := Txt[4] + ' (' + IntToStr(Difficulty) + '%)';
    50.. 99: sayDifficulty := Txt[5] + ' (' + IntToStr(Difficulty) + '%)';
    100..149: sayDifficulty := Txt[6] + ' (' + IntToStr(Difficulty) + '%)';
    150..199: sayDifficulty := Txt[7] + ' (' + IntToStr(Difficulty) + '%)';
    else
      sayDifficulty := Txt[8] + ' (' + IntToStr(Difficulty) + '%)'
{      0.. 75:sayDifficulty:=Txt[4]+' ('+IntToStr(Difficulty)+'%)';
     76..150:sayDifficulty:=Txt[5]+' ('+IntToStr(Difficulty)+'%)';
    151..225:sayDifficulty:=Txt[6]+' ('+IntToStr(Difficulty)+'%)';
    226..320:sayDifficulty:=Txt[7]+' ('+IntToStr(Difficulty)+'%)';
    ELSE   sayDifficulty:=Txt[8]+' ('+IntToStr(Difficulty)+'%)'}
  end;
{  winchance:=Round(100*exp(-0.0135446*(Difficulty-100)));
  if winchance>100 then winchance:=100;}
  sayDifficulty := sayDifficulty + ' '{+Txt[9]+' '+IntToStr(winchance)+'%'};
end;

{-------------------------------------------------------------------}

procedure TForm1.Read_Buildings;
var
  Path: string;
  Rec: TSearchRec;
begin
  nBuildings := 0;
  Path := ExtractFilePath(application.ExeName) + DataFolder + 'BLDG' + PathDelim;
  if FindFirst(Path + '*.HMP', faAnyFile - faDirectory, Rec) = 0 then
  begin
    try
      repeat
        Inc(nBuildings);
        mapBuildings[nBuildings] := Rec.Name;
        //        Memo2.Lines.Add(mapBuildings[nBuildings]);
      until FindNext(Rec) <> 0;
    finally
      FindClose(Rec);
    end;
  end;
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.GenerateMap;
var
  ix, iy, i_bot, j: Integer;
  x1, y1, Count: Integer;
  Maptype: Byte;
  GeneratedBots: Integer;
  Bothp_const, Player_hp_const: Integer;
  total_Bothp, total_Player_hp{,total_Botfirepower,total_Player_firepower}: Integer;
  //    estimated_firepower:Single;
  iFlag, Flg: Boolean;
  EstimatedDifficulty, NeededDifficulty: Integer;
  Weapon_Kind, AmmoType, WeaponType: Integer;
  Ammo_Usable: Boolean;
begin
  Infohash := 0;
  if FirstRun then
  begin
    if MessageDlg(Txt[80], mtCustom, [mbYes, mbNo], 0) = mrYes then
      CheckBox8.Checked := true
    else
      CheckBox8.Checked := false;
  end;

  Memo1.Lines.Add('[dbg] ver: ' + Copy(ExtractFileDir(application.ExeName),
    Length(ExtractFileDir(application.ExeName)) - 7, 8));

  MapParameter[0] := -1;
  MapParameter[1] := -1;
  MapParameter[2] := -1;
  MapParameter[3] := -1;
  MapParameter[4] := -1;
  MapParameter[5] := -1;
  MapParameter[6] := -1;
  MapParameter[7] := -1;
  MapParameter[8] := -1;
  New(MapG);

  Randomize;

  val(Edit2.Text, MaxX, ix);
  if MaxX < MinMaxX then
    MaxX := MinMaxX;
  if MaxX > MaxMaxX then
    MaxX := MaxMaxX;
  if (MaxX > MaxMaxX div 2) and (MaxX < MaxMaxX) then
  begin
    if MaxX < 3 * MaxMaxX div 4 then
      MaxX := MaxMaxX div 2
    else
      MaxX := MaxMaxX;
  end;
  Edit2.Text := IntToStr(MaxX);
  val(Edit7.Text, MaxY, ix);
  if MaxY < MinMaxY then
    MaxY := MinMaxY;
  if MaxY > MaxMaxY then
    MaxY := MaxMaxY;
  if (MaxY > MaxMaxY div 2) and (MaxY < MaxMaxY) then
  begin
    if MaxY < 3 * MaxMaxY div 4 then
      MaxY := MaxMaxY div 2
    else
      MaxY := MaxMaxY;
  end;
  Edit7.Text := IntToStr(MaxY);

  if MaxX > MaxY then
  begin
    max_maxValue := MaxX;
    MinMaxValue := MaxY;
  end
  else
  begin
    max_maxValue := MaxY;
    MinMaxValue := MaxX;
  end;
  average_maxValue := (MaxX + MaxY) div 2;

  if DebugMode then
    Memo1.Lines.Add(Txt[10]);
  Memo1.Lines.Add(Txt[11] + ': ' + IntToStr(MaxX) + 'x' + IntToStr(MaxY));

  Random_tiles := true;
  if ComboBox1.ItemIndex < 1 then
    Maptype := Trunc(Random * 20) + 1
  else
    Maptype := ComboBox1.ItemIndex;
  // repeat
  case Maptype of
    1: if Random > 0.6 then
        repeat
          GenerateMapRandom
        until TestMap(20, 70)
      else if Random > 0.6 then
        repeat
          GenerateMapblock
        until TestMap(20, 70)
      else if Random > 0.6 then
        repeat
          GenerateMapRandom_circles
        until TestMap(20, 70)
      else
        repeat
          GenerateMapcocon
        until TestMap(20, 80);
    2: repeat
        GenerateMapcircles
      until TestMap(20, 70);       //problems at 140x140
    3: repeat
        GenerateMapanticircles
      until TestMap(20, 70);
    4: if Random > 0.6 then
        repeat
          GenerateMapdiamonds
        until TestMap(20, 70)
      else
        repeat
          GenerateMapCheckers
        until TestMap(20, 70);
    5: repeat
        GenerateMapTmap
      until TestMap(20, 70);
    6: repeat
        GenerateMapLinearsinus
      until TestMap(20, 70);       //slow Generation
    7: if Random > 0.6 then
        repeat
          GenerateMaprectagonal
        until TestMap(20, 70)
      else
        repeat
          GenerateMapRooms
        until TestMap(20, 70);       //problems at 700x700
    8: repeat
        GenerateMapconcentric
      until TestMap(20, 70);
    9: repeat
        GenerateMapslant
      until TestMap(20, 70);
    10: repeat
        GenerateMapboxes
      until TestMap(20, 70);       //problems at 700x700
    11: repeat
        GenerateMapconcentricfull
      until TestMap(20, 70);       //problems at 700x700
    12: repeat
        GenerateMapegg
      until TestMap(20, 70);
    13: repeat
        GenerateMapnet
      until TestMap(20, 70);       //problems at 700x700
    14: repeat
        GenerateMapplus
      until TestMap(20, 70);
    15: if Random < 1 / 5 then
        repeat
          GenerateMapsmallRooms
        until TestMap(20, 70)
      else if Random < 1 / 4 then
        repeat
          GenerateMapImap
        until TestMap(20, 70)
      else if Random < 1 / 3 then
        repeat
          GenerateMapfour
        until TestMap(20, 70)
      else if Random < 1 / 2 then
        repeat
          GenerateMapfive
        until TestMap(20, 70)
      else
        repeat
          GenerateMapdash
        until TestMap(20, 70);
    16: if Random > 0.5 then
        repeat
          GenerateMapuntAngle
        until TestMap(30, 60)
      else
        repeat
          GenerateMaprotor
        until TestMap(20, 70);
    17: repeat
        GenerateMapeggre
      until TestMap(20, 70);
    18: repeat
        GenerateMapsnowflake
      until TestMap(20, 70);
    19: repeat
        GenerateMapareas
      until TestMap(20, 90);
    20: repeat
        GenerateMapWormhole
      until TestMap(20, 90);
  end;
  // until MapInhomogeneity<0.15;

  Dispose(MapG);
  if not DebugMode then
  begin
    Memo1.Lines.Add(MapGenerationText);
    Memo1.Lines.Add(mapFreeText);
    //Memo1.Lines.Add('...');
  end;

  Mappass_img := Round(Random * (MaxPass - 1)) + 1;
  MapWall_img := Round(Random * (MaxWall - 1)) + 1;
  Mapshade[1, 1] := 1;
  Mapshade[1, 2] := 0.8 + 0.2 * (Random);
  Mapshade[1, 3] := 0.8 + 0.2 * (Random);
  Mapshade[2, 1] := 0.8 + 0.2 * (Random);
  Mapshade[2, 2] := 1;
  Mapshade[2, 3] := 0.8 + 0.2 * Random;

  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
    begin
      Vis^[ix, iy] := 0;
      MapChanged^[ix, iy] := 255;
    end;

  if (Form1.RadioButton3.Checked) or ((Form1.RadioButton2.Checked) and
    (Random > 0.8)) then
  begin
    Memo1.Lines.Add(Txt[12]);
    for ix := MaxX div 5 + 2 to MaxX do
      for iy := MaxY div 5 + 2 to MaxY do
        if Map^[ix, iy] < MapWall then
          MapStatus^[ix, iy] := Round(MapSmoke * Sqrt(Random)); //tile
    for ix := 1 to 10 do
      growSmoke;
  end;


  if DebugMode then
    Memo1.Lines.Add(Txt[13]);

  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
      LOS_base^[ix, iy] := 255;
  GenerateLOS_baseMap{(true,1,1)};
  Memo1.Lines.Add(Txt[14] + ' = ' + IntToStr(Round(averageLOS)));
  if averageLOS < CallReInforcementsRange * Pi * mapFreespace / (MaxX * MaxY) then
    Form1.Memo1.Lines.Add(Txt[15] + ' = ' + IntToStr(
      Round(CallReInforcementsRange * Pi * mapFreespace / (MaxX * MaxY))));

  if DebugMode then
    Memo1.Lines.Add(Txt[16]);

  Itemsn := 0;
  NBot := 0;

  val(Edit5.Text, Playersn, ix);
  if Playersn < 1 then
    Playersn := 1;
  if Playersn > MaxPlayers then
    Playersn := MaxPlayers;
  val(Edit4.Text, Player_hp_const, ix);
  if Player_hp_const < 1 then
    Player_hp_const := 1;
  if Player_hp_const > MaxPlayerHPConst then
    Player_hp_const := MaxPlayerHPConst;
  Edit4.Text := IntToStr(Player_hp_const);
  val(Edit3.Text, Bothp_const, ix);
  if Bothp_const < 1 then
    Bothp_const := 1;
  if Bothp_const > MaxBotHPConst then
    Bothp_const := MaxBotHPConst;
  Edit3.Text := IntToStr(Bothp_const);
  val(Edit6.Text, NeededDifficulty, ix);

  total_Bothp := 0;
  total_Player_hp := 0;
  //  total_Botfirepower:=0;
  //  total_Player_firepower:=0;

  x1 := 5;           //**bunker Size!!!
  y1 := 5;
  iy := Round(Sqrt(Playersn)) - 1;
  for ix := 1 to Playersn do
  begin
    Createbot(Player, 'Player' + IntToStr(ix), Player_hp_const, x1 +
      enterX - 2, y1 + enterY - 2);
    Dec(x1);
    if x1 < 5 - iy then
    begin
      Dec(y1);
      x1 := 5;
    end;
    if Bot[NBot].HP > 15 then
      Inc(total_Player_hp, Bot[NBot].HP)
    else
      Inc(total_Player_hp, 15);
    //      inc(total_Player_firepower,StandardDamage);
  end;
  //  Bot[1].Items[1].W_ID:=4;
  //  Bot[1].Items[1].Ammo_ID:=6;

  val(Edit1.Text, GeneratedBots, ix);
  Inc(GeneratedBots, Playersn);
  if GeneratedBots < Playersn + 1 then
    GeneratedBots := Playersn + 1;
  if GeneratedBots > MaxBots then
    GeneratedBots := MaxBots;
  Edit1.Text := IntToStr(GeneratedBots - Playersn);

  Memo1.Lines.Add('...');

  Strategy_caution := TrackBar3.Position / 100;
  Strategy_finishhim := CheckBox7.Checked;
  Strategy_cheater := TrackBar2.Position;

  Memo1.Lines.Add(Txt[17]);
  Memo1.Lines.Add(Txt[18] + ': ' + IntToStr(Strategy_cheater) + ' tu');
  Memo1.Lines.Add(Txt[19] + ': ' + IntToStr(Round(100 * Strategy_caution)) + ' %');
  if Strategy_finishhim then
    Memo1.Lines.Add(Txt[20])
  else
    Memo1.Lines.Add(Txt[21]);
  Memo1.Lines.Add('...');

  i_bot := Playersn;
  repeat
    Inc(i_bot);
    Count := 0;
    repeat
      Inc(Count);
      x1 := Round(Random * (MaxX - 3) + 2);
      y1 := Round(Random * (MaxY - 3) + 2);
      Flg := true;
      for ix := 1 to Playersn do
      begin
        if (checkLOS(x1, y1, Bot[ix].X, Bot[ix].Y, true) > 0) and (Random < 0.99) then
          Flg := false;
        if (Sqr(x1 - Bot[ix].X) + Sqr(y1 - Bot[ix].Y) < Sqr(VisibleRange / 3)) and
          (Sqr(x1 - Bot[ix].X) + Sqr(y1 - Bot[ix].Y) < Sqr(MaxX / 4)) and
          (Random < 0.95) then
          Flg := false;
        if (Random > Sqrt((Sqr(x1 - Bot[ix].X) + Sqr(y1 - Bot[ix].Y)) /
          Sqr(3 / 2 * VisibleRange))) and (Random < 0.5) then
          Flg := false;
      end;
    until (((Flg) or (Random > 0.999)) and Createbot(Computer,
        'd' + IntToStr(Round(Random * 999)),{Round(Random*30)+}Bothp_const, x1, y1)) or
      (Count > 10000);
    if CheckBox1.Checked then
      Bot[NBot].Action := ActionAttack
    else
      Bot[NBot].Action := ActionRandom;
    Bot[NBot].target := Round(Random * (Playersn - 1)) + 1;

    if Bot[NBot].HP > 15 then
      Inc(total_Bothp, Bot[NBot].HP)
    else
      Inc(total_Bothp, 15);
    //inc(total_Botfirepower,StandardDamage);

    EstimatedDifficulty := calculate_Difficulty(
      Playersn, total_Player_hp, NBot - Playersn, total_Bothp, MaxX,
      MaxY, mapFreespace);
    if CheckBox5.Checked then
      iFlag := EstimatedDifficulty >= NeededDifficulty
    else
      iFlag := NBot >= GeneratedBots;

  until (iFlag) or (NBot > MaxBots) or (NBot >= mapFreespace);

  Memo1.Lines.Add(Txt[22] + ' = ' + IntToStr(Playersn));
  Memo1.Lines.Add(Txt[23] + ' = ' + IntToStr(total_Player_hp));
  Memo1.Lines.Add(Txt[24] + ' = ' + IntToStr(NBot - Playersn));
  Memo1.Lines.Add(Txt[25] + ' = ' + IntToStr(total_Bothp{ div (NBot-Playersn)}));
  if DebugMode then
    Memo1.Lines.Add('Bots together = ' +
      FloatToStr(Round(estimated_Botstogether * 10) / 10));
  Infohash := Infohash + total_Player_hp + total_Bothp;

  Memo1.Lines.Add(Txt[26] + ' = ' + sayDifficulty(EstimatedDifficulty));

  //Generate gRound Items
  if DebugMode then
    Memo1.Lines.Add(Txt[27]);
  iy := 0;
  for ix := 1 to NBot do {if Bot[ix].Owner=Computer then}
  begin
    Inc(iy, Bot[ix].HP);
  end;
  Count := 0;
  while ((iy > (Itemsn * 10 + NBot * 20) * 10) or (Random < 0.9) or
      (Count < Sqrt(MaxX * MaxY) / 4)) and (Itemsn < MaxItems) do
  begin
    Inc(Count);
    Inc(Itemsn);
    repeat
      x1 := Round(Random * (MaxX - 2) + 1);
      y1 := Round(Random * (MaxY - 2) + 1);
    until (Map^[x1, y1] < MapWall);
    Item[Itemsn].X := x1;
    Item[Itemsn].Y := y1;
    Item[Itemsn].Item.W_ID := 0;
    WeaponType := 0;
    if (Random < iy * ItemDamageRate * 2 / ((Itemsn * 10 + NBot * 20) * 10)) or
      (Random < 0.1) then
    begin
      Weapon_Kind := 1;
      if Random < 0.10 then
        Weapon_Kind := 2;
      if Random < 0.08 then
        Weapon_Kind := 3;
      if CheckBox3.Checked then
        Weapon_Kind := 3;
      repeat
        WeaponType := Trunc(w_types * Random) + 1;
      until (Random * 100 < WeaponSpecifications[WeaponType].rnd) and
        (WeaponSpecifications[WeaponType].Kind = Weapon_Kind);
      Item[Itemsn].Item.W_ID := WeaponType;

      Item[Itemsn].Item.MaxState :=
        Round((WeaponSpecifications[Item[Itemsn].Item.W_ID].MaxState * 3 / 4) *
        Random + WeaponSpecifications[Item[Itemsn].Item.W_ID].MaxState / 4);
      Item[Itemsn].Item.State := Round(Item[Itemsn].Item.MaxState * Random);
      Item[Itemsn].Item.RechargeState := 0;
    end;

    repeat
      AmmoType := Trunc(a_types * Random) + 1;
      if WeaponType = 0 then
        Ammo_Usable := true
      else
      begin
        Ammo_Usable := false;
        for j := 1 to maxUsableAmmo do
          if WeaponSpecifications[WeaponType].Ammo[j] = AmmoType then
            Ammo_Usable := true;
      end;
    until (Random * 100 < AmmoSpecifications[AmmoType].rnd) and
      (Ammo_Usable) {and (WeaponSpecifications[WeaponType].Kind=Weapon_Kind)};
    Item[Itemsn].Item.Ammo_ID := AmmoType;
    Item[Itemsn].Item.N := Round(
      (AmmoSpecifications[Item[Itemsn].Item.Ammo_ID].Quantity - 1) * Random) + 1;
  end;

  //  Memo1.Lines.Add('...');
  Memo2.Lines := Memo1.Lines;

  Memo1.Clear;

  MapGenerated := true;

  Memo1.Lines.Add(Txt[28]);
  ScrollBar1.Position := 0;
  ScrollBar2.Position := 0;
  Selected := -1;
  SelectedEnemy := -1;
  SelectedOnFloor := -1;
  SelectedItem := -1;
  Selectedx := -1;
  Selectedy := -1;
  MovementMapfor := -1;
  ViewX := 0;
  ViewY := 0;
  Memo1.Lines.Add('=========================');

  WriteINIFile;

  if CheckBox4.Checked then
  begin
    Memo1.Lines.Add('Tutorial Mode on');

  end;


  current_turn := 0;
  start_turn;

  //  MapChanged:=true;
end;

{--------------------------------------------------------------------------------------}

function Valueof(s: string): Integer;
var
  v1, v2: Integer;
begin
  val(s, v1, v2);
  if v2 <> 0 then
    ShowMessage('Script error: unable to process number.');
  Valueof := v1;
end;

procedure TForm1.Generate_Items_types;
var
  i: Integer;
  s: string;
  f1: TextFile;
  languageFolder: string;
begin
  case ComboBox2.ItemIndex of
    0: languageFolder := 'ENG' + PathDelim;
    1: languageFolder := 'RUS' + PathDelim;
  end;
  AssignFile(f1, ExtractFilePath(application.ExeName) + ScriptFolder +
    languageFolder + 'weapon.inf');
  Reset(f1);
  w_types := 0;
  repeat
    ReadLn(f1, s);
    if Trim(s) = '<WEAPON>' then
    begin
      Inc(w_types);
      if w_types > 255 then
      begin
        w_types := 255;
        ShowMessage('Weapon.ini error: No more than 255 Weapon/Item types allowed!');
      end;
      with WeaponSpecifications[w_types] do
      begin
        for i := 1 to maxUsableAmmo do
          Ammo[i] := 0;
        Name := 'no name';
        ACC := 0;
        DAM := 0;
        Recharge := 0;
        AIM := 0;
        Reload := 0;
        MaxState := 0;
        Description := '';
        Kind := 0;
        RND := 0;

        i := 0;
        repeat
          ReadLn(f1, s);
          s := Trim(s);
          if Copy(s, 1, 5) = 'NAME=' then
            Name := Trim(Copy(s, 6, 99))
          else
          if Copy(s, 1, 4) = 'ACC=' then
            ACC := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 4) = 'DAM=' then
            DAM := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 9) = 'RECHARGE=' then
            Recharge := Valueof(Copy(s, 10, 99))
          else
          if Copy(s, 1, 4) = 'AIM=' then
            AIM := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 7) = 'RELOAD=' then
            Reload := Valueof(Copy(s, 8, 99))
          else
          if Copy(s, 1, 9) = 'MAXSTATE=' then
            MaxState := Valueof(Copy(s, 10, 99))
          else
          if Copy(s, 1, 5) = 'KIND=' then
            Kind := Valueof(Copy(s, 6, 99))
          else
          if Copy(s, 1, 4) = 'RND=' then
            RND := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 12) = 'DESCRIPTION=' then
            Description := Trim(Copy(s, 13, 999))
          else
          if Copy(s, 1, 5) = 'AMMO=' then
          begin
            Inc(i);
            if i > maxUsableAmmo then
            begin
              ShowMessage('Weapon.ini error: max Usable Ammo types = ' +
                IntToStr(maxUsableAmmo));
              i := maxUsableAmmo;
            end;
            Ammo[i] := Valueof(Copy(s, 6, 99));
          end;
        until (s = '</WEAPON>') or (EOF(f1));
      end;
    end;
  until EOF(f1);
  CloseFile(f1);

  AssignFile(f1, ExtractFilePath(application.ExeName) + ScriptFolder +
    languageFolder + 'ammo.inf');
  Reset(f1);
  a_types := 0;
  repeat
    ReadLn(f1, s);
    if Trim(s) = '<AMMO>' then
    begin
      Inc(a_types);
      if a_types > 255 then
      begin
        a_types := 255;
        ShowMessage('Ammo.ini error: No more than 255 Ammo types allowed!');
      end;
      with AmmoSpecifications[a_types] do
      begin
        Name := 'no name';
        ACC := 0;
        DAM := 0;
        Quantity := 0;
        Explosion := 0;
        AREA := 0;
        Smoke := 0;
        Description := '';
        Kind := 0;
        RND := 0;

        repeat
          ReadLn(f1, s);
          s := Trim(s);
          if Copy(s, 1, 5) = 'NAME=' then
            Name := Trim(Copy(s, 6, 99))
          else
          if Copy(s, 1, 4) = 'ACC=' then
            ACC := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 4) = 'DAM=' then
            DAM := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 9) = 'QUANTITY=' then
            Quantity := Valueof(Copy(s, 10, 99))
          else
          if Copy(s, 1, 10) = 'EXPLOSION=' then
            Explosion := Valueof(Copy(s, 11, 99))
          else
          if Copy(s, 1, 5) = 'AREA=' then
            AREA := Valueof(Copy(s, 6, 99))
          else
          if Copy(s, 1, 5) = 'AREA=' then
            AREA := Valueof(Copy(s, 6, 99))
          else
          if Copy(s, 1, 5) = 'KIND=' then
            Kind := Valueof(Copy(s, 6, 99))
          else
          if Copy(s, 1, 4) = 'RND=' then
            RND := Valueof(Copy(s, 5, 99))
          else
          if Copy(s, 1, 12) = 'DESCRIPTION=' then
            Description := Trim(Copy(s, 13, 999))
          else
          if Copy(s, 1, 6) = 'SMOKE=' then
            Smoke := Valueof(Copy(s, 7, 99));
        until (s = '</AMMO>') or (EOF(f1));
      end;
    end;
  until EOF(f1);
  CloseFile(f1);
end;

{--------------------------------------------------------------------------------------}

procedure dosaveInfo;
var
  f1: TextFile;
begin
  AssignFile(f1, 'battle' + IntToStr(Round(Now * 24 * 60 * 60)) + '.txt');
  Rewrite(f1);
  WriteLn(f1, 'Player ID = ' + IntToStr(PlayerID));
  WriteLn(f1, '......');
  Write(f1, Form1.Memo2.Lines.Text);
  WriteLn(f1, '......');
  Write(f1, Form1.Memo1.Lines.Text);
  WriteLn(f1, '......');
  WriteLn(f1, 'Hash = ' + IntToStr(Infohash));
  CloseFile(f1);
end;

{--------------------------------------------------------------------------------------}

const
  standardcropsymbol = 80;

procedure TForm1.Create_language_interface;
var
  s, CaptionText, HintText, HintText0: string;
  doShowHint: Boolean;
  f1: TextFile;
  cropsymbol: Integer;
  languageFolder: string;
begin
  case ComboBox2.ItemIndex of
    0: languageFolder := 'ENG' + PathDelim;
    1: languageFolder := 'RUS' + PathDelim;
  end;

  AssignFile(f1, ExtractFilePath(application.ExeName) + ScriptFolder +
    languageFolder + 'interface.inf');
  Reset(f1);
  repeat
    ReadLn(f1, s);
    s := Trim(s);
    if Copy(s, 1, 1) = '-' then
    begin
      ReadLn(f1, CaptionText);
      CaptionText := Trim(CaptionText);
      ReadLn(f1, HintText0);
      HintText0 := Trim(HintText0);
      if HintText0 = '*' then
        doShowHint := false
      else
        doShowHint := true;
      HintText := '';
      repeat
        if Length(HintText0) <= standardcropsymbol then
          cropsymbol := Length(HintText0)
        else
        begin
          cropsymbol := standardcropsymbol;
          repeat
            Inc(cropsymbol);
          until (Copy(HintText0, cropsymbol, 1) = ' ') or
            (Length(HintText0) <= cropsymbol);

        end;
        HintText := HintText + Copy(HintText0, 1, cropsymbol);
        HintText0 := Copy(HintText0, cropsymbol + 1, Length(HintText0));
        if Length(HintText0) > 0 then
          HintText := HintText + sLineBreak;
      until Length(HintText0) = 0;
      case s of
        '-Button01':
        begin
          Button1.Caption := CaptionText;
          Button1.ShowHint := doShowHint;
          Button1.Hint := HintText;
        end;
        '-Button02':
        begin
          Button2.Caption := CaptionText;
          Button2.ShowHint := doShowHint;
          Button2.Hint := HintText;
        end;
        '-Button03':
        begin
          Button3.Caption := CaptionText;
          Button3.ShowHint := doShowHint;
          Button3.Hint := HintText;
        end;
        '-Button04':
        begin
          Button4.Caption := CaptionText;
          Button4.ShowHint := doShowHint;
          Button4.Hint := HintText;
        end;
{      '-Button05': begin
                     Button5.caption:=CaptionText;
                     Button5.ShowHint:=doShowHint;
                     Button5.Hint:=HintText;
                   end;}
        '-Button06':
        begin
          Button6.Caption := CaptionText;
          Button6.ShowHint := doShowHint;
          Button6.Hint := HintText;
        end;
{      '-Button07': begin
                     Button7.caption:=CaptionText;
                     Button7.ShowHint:=doShowHint;
                     Button7.Hint:=HintText;
                   end;}
        '-ToggleBox01':
        begin
          ToggleBox1.Caption := CaptionText;
          ToggleBox1.ShowHint := doShowHint;
          ToggleBox1.Hint := HintText;
        end;
        '-CheckBox01':
        begin
          CheckBox1.Caption := CaptionText;
          CheckBox1.ShowHint := doShowHint;
          CheckBox1.Hint := HintText;
        end;
        '-CheckBox02':
        begin
          CheckBox2.Caption := CaptionText;
          CheckBox2.ShowHint := doShowHint;
          CheckBox2.Hint := HintText;
        end;
        '-CheckBox03':
        begin
          CheckBox3.Caption := CaptionText;
          CheckBox3.ShowHint := doShowHint;
          CheckBox3.Hint := HintText;
        end;
        '-CheckBox04':
        begin
          CheckBox4.Caption := CaptionText;
          CheckBox4.ShowHint := doShowHint;
          CheckBox4.Hint := HintText;
        end;
        '-CheckBox05':
        begin
          CheckBox5.Caption := CaptionText;
          CheckBox5.ShowHint := doShowHint;
          CheckBox5.Hint := HintText;
        end;
        '-CheckBox06':
        begin
          CheckBox6.Caption := CaptionText;
          CheckBox6.ShowHint := doShowHint;
          CheckBox6.Hint := HintText;
        end;
        '-CheckBox07':
        begin
          CheckBox7.Caption := CaptionText;
          CheckBox7.ShowHint := doShowHint;
          CheckBox7.Hint := HintText;
        end;
        '-CheckBox08':
        begin
          CheckBox8.Caption := CaptionText;
          CheckBox8.ShowHint := doShowHint;
          CheckBox8.Hint := HintText;
        end;
        '-RadioButton01':
        begin
          RadioButton1.Caption := CaptionText;
          RadioButton1.ShowHint := doShowHint;
          RadioButton1.Hint := HintText;
        end;
        '-RadioButton02':
        begin
          RadioButton2.Caption := CaptionText;
          RadioButton2.ShowHint := doShowHint;
          RadioButton2.Hint := HintText;
        end;
        '-RadioButton03':
        begin
          RadioButton3.Caption := CaptionText;
          RadioButton3.ShowHint := doShowHint;
          RadioButton3.Hint := HintText;
        end;
        '-Label01':
        begin
          Label1.Caption := CaptionText;
          Label1.ShowHint := doShowHint;
          Label1.Hint := HintText;
        end;
        '-Label02':
        begin
          Label2.Caption := CaptionText;
          Label2.ShowHint := doShowHint;
          Label2.Hint := HintText;
        end;
        '-Label03':
        begin
          Label3.Caption := CaptionText;
          Label3.ShowHint := doShowHint;
          Label3.Hint := HintText;
        end;
        '-Label04':
        begin
          Label4.Caption := CaptionText;
          Label4.ShowHint := doShowHint;
          Label4.Hint := HintText;
        end;
        '-Label05':
        begin
          Label5.Caption := CaptionText;
          Label5.ShowHint := doShowHint;
          Label5.Hint := HintText;
        end;
        '-Label06':
        begin
          Label6.Caption := CaptionText;
          Label6.ShowHint := doShowHint;
          Label6.Hint := HintText;
        end;
        '-Label07':
        begin
          Label7.Caption := CaptionText;
          Label7.ShowHint := doShowHint;
          Label7.Hint := HintText;
        end;
        '-Label08':
        begin
          //Label8.caption:=CaptionText;
          Label8.ShowHint := doShowHint;
          Label8.Hint := HintText;
          Label8.Visible := false;
        end;
{      '-Label09': begin
                     Label9.caption:=CaptionText;
                     Label9.ShowHint:=doShowHint;
                     Label9.Hint:=HintText;
                   end;}
        '-Label10':
        begin
          //Label10.caption:=CaptionText;
          Label10.ShowHint := doShowHint;
          Label10.Hint := HintText;
          Label10.Visible := false;
        end;
        '-Label11':
        begin
          Label11.Caption := CaptionText;
          Label11.ShowHint := doShowHint;
          Label11.Hint := HintText;
        end;
        '-Label12':
        begin
          Label12.Caption := CaptionText;
          Label12.ShowHint := doShowHint;
          Label12.Hint := HintText;
        end;
{      '-Label13': begin
                     Label13.caption:=CaptionText;
                     Label13.ShowHint:=doShowHint;
                     Label13.Hint:=HintText;
                   end;}
        '-Label14':
        begin
          Label14.Caption := CaptionText;
          Label14.ShowHint := doShowHint;
          Label14.Hint := HintText;
        end;
        '-Label15':
        begin
          Label15.Caption := CaptionText;
          Label15.ShowHint := doShowHint;
          Label15.Hint := HintText;
        end;
        '-Label16':
        begin
          Label16.Caption := CaptionText;
          Label16.ShowHint := doShowHint;
          Label16.Hint := HintText;
        end;
        '-Label17':
        begin
          Label17.Caption := CaptionText;
          Label17.ShowHint := doShowHint;
          Label17.Hint := HintText;
        end;
        '-Label18':
        begin
          Label18.Caption := CaptionText;
          Label18.ShowHint := doShowHint;
          Label18.Hint := HintText;
        end;
        '-Label19':
        begin
          Label19.Caption := CaptionText;
          Label19.ShowHint := doShowHint;
          Label19.Hint := HintText;
        end;
        '-Label20':
        begin
          Label20.Caption := CaptionText;
          Label20.ShowHint := doShowHint;
          Label20.Hint := HintText;
        end;
        '-Label21':
        begin
          Label21.Caption := CaptionText;
          Label21.ShowHint := doShowHint;
          Label21.Hint := HintText;
        end;
{      '-TrackBar01': begin
                     //TrackBar1.caption:=CaptionText;
                     TrackBar1.ShowHint:=doShowHint;
                     TrackBar1.Hint:=HintText;
                   end;}
        '-TrackBar02':
        begin
          //TrackBar2.caption:=CaptionText;
          TrackBar2.ShowHint := doShowHint;
          TrackBar2.Hint := HintText;
        end;
        '-TrackBar03':
        begin
          //TrackBar3.caption:=CaptionText;
          TrackBar3.ShowHint := doShowHint;
          TrackBar3.Hint := HintText;
        end;
      end;
    end;

  until EOF(f1);
  CloseFile(f1);

  AssignFile(f1, ExtractFilePath(application.ExeName) + ScriptFolder +
    LanguageFolder + 'text.inf');
  Reset(f1);
  repeat
    ReadLn(f1, s);
    s := Trim(s);
    if Copy(s, 1, 1) = '^' then
    begin
      Txt[Valueof(Copy(s, 2, 3))] := Trim(Copy(s, 6, Length(s)));
    end;
  until EOF(f1);
  CloseFile(f1);

  Generate_Items_types;
  Edit4Change(nil);

end;

{--------------------------------------------------------------------------------------}


procedure tForm1.ReadINIFile;
var
  f1: TextFile;
  ininame: string;
  s, s1: string;
  i, Value: Integer;
begin
  FirstRun := false;
  ininame := ExtractFilePath(application.ExeName) + PathDelim + INIFileName;
  if Fileexists(ininame) then
  begin
    AssignFile(f1, ininame);
    Reset(f1);
    repeat
      ReadLn(f1, s);
      s := Trim(s);
      if Copy(s, 1, 1) = '^' then
      begin
        s1 := '';
        i := 0;
        repeat
          Inc(i);
        until Copy(s, i, 1) = '=';
        s1 := Copy(s, 2, i - 2);
        Value := Valueof(Trim(Copy(s, i + 1, Length(s))));
        case s1 of
          'LANGUAGE': ComboBox2.ItemIndex := Value;
          'PlayerID': PlayerID := Value;
          'MaxX': Edit2.Text := IntToStr(Value);
          'MaxY': Edit7.Text := IntToStr(Value);
          'Smoke':
          begin
            if Value = 0 then
              RadioButton1.Checked := true
            else
            if Value = 1 then
              RadioButton2.Checked := true
            else
            if Value = 2 then
              RadioButton3.Checked := true;
          end;
          'BuildingS': if Value = 1 then
              CheckBox6.Checked := true
            else
              CheckBox6.Checked := false;
          'E-Bots': Edit1.Text := IntToStr(Value);
          'P-Bots': Edit5.Text := IntToStr(Value);
          'E-HP': Edit3.Text := IntToStr(Value);
          'P-HP': Edit4.Text := IntToStr(Value);
          'DEFENCE': if Value = 1 then
              CheckBox1.Checked := true
            else
              CheckBox1.Checked := false;
          'BAZOOKA': if Value = 1 then
              CheckBox3.Checked := true
            else
              CheckBox3.Checked := false;
          'DEMAND': if Value = 1 then
              CheckBox5.Checked := true
            else
              CheckBox5.Checked := false;
          'Difficulty': Edit6.Text := IntToStr(Value);
          'CHEATER': TrackBar2.Position := Value;
          'CAUTION': TrackBar3.Position := Value;
          'FINISHHIM': if Value = 1 then
              CheckBox7.Checked := true
            else
              CheckBox7.Checked := false;
          'GenerateLOG': if Value = 1 then
              CheckBox8.Checked := true
            else
              CheckBox8.Checked := false;
          'CONFIRM': if Value = 1 then
              CheckBox2.Checked := true
            else
              CheckBox2.Checked := false;
        end;
      end;
    until EOF(f1);
    CloseFile(f1);
  end
  else
  begin
    PlayerID := Round(Random * 999999);
    FirstRun := true;
  end;
end;

procedure tForm1.WriteINIFile;
var
  f1: TextFile;
begin
  AssignFile(f1, ExtractFilePath(application.ExeName) + PathDelim + INIFileName);
  Rewrite(f1);
  WriteLn(f1, '^PlayerID=' + IntToStr(PlayerID));
  WriteLn(f1, '^LANGUAGE=' + IntToStr(ComboBox2.ItemIndex));
  WriteLn(f1, '^MaxX=' + Edit2.Text);
  WriteLn(f1, '^MaxY=' + Edit7.Text);
  if RadioButton1.Checked then
    WriteLn(f1, '^Smoke=0');
  if RadioButton2.Checked then
    WriteLn(f1, '^Smoke=1');
  if RadioButton3.Checked then
    WriteLn(f1, '^Smoke=2');
  if CheckBox6.Checked then
    WriteLn(f1, '^BuildingS=1')
  else
    WriteLn(f1, '^BuildingS=0');
  WriteLn(f1, '^E-Bots=' + Edit1.Text);
  WriteLn(f1, '^P-Bots=' + Edit5.Text);
  WriteLn(f1, '^E-HP=' + Edit3.Text);
  WriteLn(f1, '^P-HP=' + Edit4.Text);
  if CheckBox1.Checked then
    WriteLn(f1, '^DEFENCE=1')
  else
    WriteLn(f1, '^DEFENCE=0');
  if CheckBox3.Checked then
    WriteLn(f1, '^BAZOOKA=1')
  else
    WriteLn(f1, '^BAZOOKA=0');
  if CheckBox5.Checked then
    WriteLn(f1, '^DEMAND=1')
  else
    WriteLn(f1, '^DEMAND=0');
  WriteLn(f1, '^Difficulty=' + Edit6.Text);
  WriteLn(f1, '^CHEATER=' + IntToStr(TrackBar2.Position));
  WriteLn(f1, '^CAUTION=' + IntToStr(TrackBar3.Position));
  if CheckBox7.Checked then
    WriteLn(f1, '^FINISHHIM=1')
  else
    WriteLn(f1, '^FINISHHIM=0');
  if CheckBox8.Checked then
    WriteLn(f1, '^GenerateLOG=1')
  else
    WriteLn(f1, '^GenerateLOG=0');
  if CheckBox2.Checked then
    WriteLn(f1, '^CONFIRM=1')
  else
    WriteLn(f1, '^CONFIRM=0');

  CloseFile(f1);
end;

{--------------------------------------------------------------------------------------}

var
  GLI1: array[1..MaxWall] of TGLImage;
  GLI2: array[1..MaxPass] of TGLImage;
  BotImage: array [0..BotTypes, 0..MaxAngle] of TGLImage;
  CracksImage: array[1..MaxCracks] of TGLImage;
  ItemImage, SelectedImage, img_EnemySelected: TGLImage;
  ExplosionImage: array[1..10] of TGLImage;
  TextFrame: TGLImage;

  DamageFont, InfoFont, MoveFont: TTextureFont;

function getx(mapx: Byte): Integer;
begin
  getx := (mapx - 1 - ViewX) * ZoomScale;
end;

function gety(mapy: Byte): Integer;
begin
  gety := (Zoom - mapy + ViewY) * ZoomScale;
end;

const
  maxsteps = MaxMaxX;

procedure TForm1.CastleControl1Render(Sender: TObject);
var
  ix, iy: Integer;
  ShadeColor: TVector4;
  Shadebright: single;
  i: Integer;
  t: TDAteTime;
  //    srect:TRectangle;
  sx, sy: Integer;
  Highlight: single;
  xx1, yy1: Integer;

  ax, ay, av: array[1..Maxsteps] of Integer;
  tmpv, nv: Integer;
  sv: string;
begin
  //initialize routines buggy-wooggy
  if SelectedImage = nil then
    SelectedImage := TGLImage.Create(DataFolder + 'png' + PathDelim + 'selected.png');
  if img_EnemySelected = nil then
    img_EnemySelected := TGLImage.Create(DataFolder + 'png' + PathDelim +
      'enemyselected.png');
  for i := 1 to 10 do
    if ExplosionImage[i] = nil then
    begin
      ExplosionImage[i] := TGLImage.Create(DataFolder + 'png' + PathDelim +
        'expl' + IntToStr(i) + '.png');
      ExplosionImage[i].Alpha := acBlending;
    end;
  for i := 1 to MaxWall do
    if GLI1[i] = nil then
      GLI1[i] := TGLImage.Create(DataFolder + 'png' + PathDelim +
        'wall' + IntToStr(i) + '.png');
  for i := 1 to MaxPass do
    if GLI2[i] = nil then
      GLI2[i] := TGLImage.Create(DataFolder + 'png' + PathDelim +
        'pass' + IntToStr(i) + '.png');
  if ItemImage = nil then
    ItemImage := TGLImage.Create(DataFolder + 'png' + PathDelim + 'Item.png');
  for ix := 0 to BotTypes do
    for iy := 0 to MaxAngle do
      if BotImage[ix, iy] = nil then
      begin
        if iy < MaxAngle then
        begin
          case ix of
            0: BotImage[ix, iy] :=
                TGLImage.Create(DataFolder + 'png' + PathDelim + 'T' +
                IntToStr(iy) + '.png');
            1: BotImage[ix, iy] :=
                TGLImage.Create(DataFolder + 'png' + PathDelim + 'Q' +
                IntToStr(iy) + '.png');
            2: BotImage[ix, iy] :=
                TGLImage.Create(DataFolder + 'png' + PathDelim + 'N' +
                IntToStr(iy) + '.png');
            3: BotImage[ix, iy] :=
                TGLImage.Create(DataFolder + 'png' + PathDelim + 'H' +
                IntToStr(iy) + '.png');
            4: BotImage[ix, iy] :=
                TGLImage.Create(DataFolder + 'png' + PathDelim + 'M' +
                IntToStr(iy) + '.png');
          end;
          BotImage[ix, iy].Alpha := acBlending;
        end
        else
          BotImage[ix, MaxAngle] := BotImage[ix, 0];
      end;
  for ix := 1 to maxCracks do
    if CracksImage[ix] = nil then
    begin
      CracksImage[ix] := TGLImage.Create(DataFolder + 'png' + PathDelim +
        'cracks' + IntToStr(ix) + '.png');
      CracksImage[ix].Alpha := acBlending;
    end;
  if TextFrame = nil then
  begin
    TextFrame := TGLImage.Create(DataFolder + 'png' + PathDelim + 'TextFrame.png');
    TextFrame.Alpha := acBlending;
  end;

  t := Now;
  //Drawing the map
  for ix := 1 to MaxX do
    if (ix > ViewX) and (ix <= ViewX + ViewSizeX) then
      for iy := 1 to MaxY do
        if (iy > ViewY) and (iy <= ViewY + ViewSizeY) then
          if ((MapChanged^[ix, iy] > 0) or (DrawMapall)) and (Vis^[ix, iy] > 0) then
          begin
            //MapChanged^[ix,iy]:=0;
            if Vis^[ix, iy] > OldVisible then
            begin
              ShadeBright := ((Vis^[ix, iy] - OldVisible) / (MaxVisible - OldVisible));
              ShadeColor := Vector4(shadebright, shadebright, shadebright, 1);
            end
            else if Vis^[ix, iy] = OldVisible then
            begin
              ShadeBright := 0.3;
              ShadeColor := Vector4(shadebright / 2, shadebright / 2,
                shadebright, 1);
            end;
            if Map^[ix, iy] >= MapWall then
            begin
              shadeColor[0] := shadeColor[0] * Mapshade[1, 1];
              shadeColor[1] := shadeColor[1] * Mapshade[1, 2];
              shadeColor[2] := shadeColor[2] * Mapshade[1, 3];
              GLI1[MapWall_img].Color := ShadeColor;
              GLI1[MapWall_img].Draw(getx(ix), gety(iy));
              Highlight := Sqr(MapStatus^[ix, iy] / MaxStatus);
              CracksImage[(ix + 3 * iy + Sqr(ix) * 5 + Sqr(iy) * 7) mod
                maxCracks + 1].Color :=
                Vector4(shadebright, shadebright, shadebright,
                0.4 + 0.6 * (1 - Highlight));
              CracksImage[(ix + 3 * iy + Sqr(ix) * 5 + Sqr(iy) * 7) mod
                maxCracks + 1].Draw(getx(ix), gety(iy));
            end
            else
            begin
              Highlight := 0;
              if DoHighlight then
              begin
                if checkLOS(ix, iy, HighlightX, HighlightY, true) > 0 then
                  Highlight := 0.5;
              end;
              shadeColor[0] := shadeColor[0] * Mapshade[2, 1] + Highlight;
              shadeColor[1] := shadeColor[1] * Mapshade[2, 2] - Highlight / 2;
              shadeColor[2] := shadeColor[2] * Mapshade[2, 3] - Highlight / 2;
              GLI2[Mappass_img].Color := ShadeColor;
              GLI2[Mappass_img].Draw(getx(ix), gety(iy));
              if (Vis^[ix, iy] > OldVisible) and (MapStatus^[ix, iy] > 0) then
              begin
                for i := 1 to 1 +
                  Round(2 * (0.01 + 0.3 * (MapStatus^[ix, iy] - 1) /
                    (MapSmoke - 1)) * Sqr(ZoomScale)) do
                begin
                  ShadeBright := Random;
                  DrawRectangle(Rectangle(Round(ZoomScale * Random) +
                    getx(ix), Round(ZoomScale * Random) + gety(iy), 1, 1),
                    Vector4(shadebright, shadebright, shadebright, 0.7));
                end;
              end;
            end;
          end;
  for i := 1 to Itemsn do
    if (Vis^[Item[i].X, Item[i].Y] >= OldVisible) then
      if (Item[i].X > ViewX) and (Item[i].X <= ViewX + ViewSizeX) and
        (Item[i].Y > ViewY) and (Item[i].Y <= ViewY + ViewSizeY) then
      begin
        ShadeBright := 0.3 + 0.7 * ((Vis^[Item[i].X, Item[i].Y] - OldVisible) /
          (MaxVisible - OldVisible));
        ItemImage.Color := Vector4(shadebright, shadebright, shadebright, 1);
        ItemImage.Draw(getx(Item[i].X), gety(Item[i].Y));
      end;

  for i := 1 to NBot do
    if (Bot[i].HP > 0) and (Vis^[Bot[i].X, Bot[i].Y] > OldVisible) then
      if (Bot[i].X > ViewX) and (Bot[i].X <= ViewX + ViewSizeX) and
        (Bot[i].Y > ViewY) and (Bot[i].Y <= ViewY + ViewSizeY) then
      begin
        ShadeBright := 0.3 + 0.7 * ((Vis^[Bot[i].X, Bot[i].Y] - OldVisible) /
          (MaxVisible - OldVisible));
        BotImage[Bot[i].BType, Bot[i].Angle].Color :=
          Vector4(shadebright, shadebright, shadebright, 1);
        BotImage[Bot[i].BType, Bot[i].Angle].Draw(getx(Bot[i].X), gety(Bot[i].Y));
        if i = Selected then
          SelectedImage.Draw(getx(Bot[i].X), gety(Bot[i].Y))
        else
        if i = SelectedEnemy then
          img_EnemySelected.Draw(getx(Bot[i].X), gety(Bot[i].Y))
        else;
      end;

  {--------}

  {Draw Movement path}
  if (Selectedx > 0) and (Selected > 0) and (Form1.CheckBox2.Checked) then
  begin
    if (Movement^[Selectedx, Selectedy] < 9) and (Selected = MovementMapfor) and
      ((Selectedx <> Bot[Selected].X) or (Selectedy <> Bot[Selected].Y)) then
    begin
      xx1 := Selectedx;
      yy1 := Selectedy;
      nv := 0;
      tmpv := 0;
      repeat
        if nv < MaxMaxX then
          Inc(nv);
        ax[nv] := xx1;
        ay[nv] := yy1;
        av[nv] := tmpv;
        case Movement^[xx1, yy1] of
          5:
          begin
            Inc(xx1);
            Dec(yy1);
            tmpv += Round(Bot[Selected].Speed * Sqrt(2));
          end;
          6:
          begin
            Dec(yy1);
            tmpv += Bot[Selected].Speed;
          end;
          7:
          begin
            Dec(xx1);
            Dec(yy1);
            tmpv += Round(Bot[Selected].Speed * Sqrt(2));
          end;
          8:
          begin
            Dec(xx1);
            tmpv += Bot[Selected].Speed;
          end;
          1:
          begin
            Dec(xx1);
            Inc(yy1);
            tmpv += Round(Bot[Selected].Speed * Sqrt(2));
          end;
          2:
          begin
            Inc(yy1);
            tmpv += Bot[Selected].Speed;
          end;
          3:
          begin
            Inc(xx1);
            Inc(yy1);
            tmpv += Round(Bot[Selected].Speed * Sqrt(2));
          end;
          4:
          begin
            Inc(xx1);
            tmpv += Bot[Selected].Speed;
          end;
        end;
      until (xx1 = Bot[Selected].X) and (yy1 = Bot[Selected].Y);

      i := nv;
      repeat
        if nv < MaxMaxX then
        begin
          sv := IntToStr(Bot[Selected].TU - tmpv + av[i]);
          if Abs(Bot[Selected].TU - tmpv + av[i]) >= 1000 then
            sv := 'X';
          if Bot[Selected].TU - tmpv + av[i] >= 0 then
          begin
            if tmpv - av[i] <= Bot[Selected].Items[1].RechargeState then
              ShadeColor := Vector4(1, 1, 0.2, 1)
            else
              ShadeColor := Vector4(0.2, 1, 0.2, 1);
          end
          else
            ShadeColor := Vector4(1, 0.1, 0.1, 1);
        end
        else
        begin
          sv := 'X';
          ShadeColor := Vector4(1, 1, 0.2, 1);
        end;
        try
          MoveFont.Print(getx(ax[i]) + 2 - 1, gety(ay[i]) + ZoomScale div 3 + 1,
            Vector4(0, 0, 0, 1), sv);
          MoveFont.Print(getx(ax[i]) + 2 + 1, gety(ay[i]) + ZoomScale div 3 - 1,
            Vector4(0, 0, 0, 1), sv);
          MoveFont.Print(getx(ax[i]) + 2, gety(ay[i]) + ZoomScale div 3, ShadeColor, sv);
        finally
        end;
        Dec(i);
      until i = 0;
    end;
  end;

  {--------}

  if DoExplosion then
  begin
    i := Trunc(9 * (Now - AnimationTimer) * 24 * 60 * 60 * 1000 / BlastDelay) + 1;
    if i < 1 then
      i := 1;
    if i > 10 then
      i := 10;
    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
        if Explosion_area^[ix, iy] > 0 then
        begin
          //       ExplosionImage[i].Draw(getx(ix),gety(iy));
          sx := getx(ix) + ZoomScale div 2 - Explosion_area^[ix, iy] div 2;
          sy := gety(iy) + ZoomScale div 2 - Explosion_area^[ix, iy] div 2;
          ExplosionImage[i].Draw(sx, sy, Explosion_area^[ix, iy], Explosion_area^[ix, iy],
            0, 0, ZoomScale - 1, ZoomScale - 1);
        end;
    //AnimationTimer-=Form1.CastleControl1.fps.UpdateSecondsPassed;
  end;

  if Dotxt then
    try
      DamageFont.PrintStrings(txtX + 1, txtY - 1, Vector4(0, 0, 0, 1),
        txt_out, false, 0);
      DamageFont.PrintStrings(txtX - 1, txtY + 1, Vector4(0, 0, 0, 1),
        txt_out, false, 0);
      DamageFont.PrintStrings(txtX, txtY, Vector4(0.7 + 0.3 *
        (Now - AnimationTimer) * 24 * 60 * 60 * 1000 / HitDelay, 0.7 +
        0.3 * (Now - AnimationTimer) * 24 * 60 * 60 * 1000 / HitDelay,
        0.7 + 0.3 * (Now - AnimationTimer) * 24 * 60 * 60 * 1000 / HitDelay, 1),
        txt_out, false, 0);
    finally
    end;

  if GameMode = GameMode_ItemInfo then
  begin
    if DoAnimate_Info then
    begin
      i := 20 + Round(580 * (Now - InfoAnimation) * 24 * 60 * 60 * 1000 / InfoDelay);
      if i > 600 then
        i := 600;
      TextFrame.Color := Vector4(1, 1, 1, 0.8 * i / 600);
      TextFrame.Draw3x3(50, 50, 600, i, 7, 7, 7, 7);
    end
    else
    begin
      TextFrame.Color := Vector4(1, 1, 1, 0.8);
      TextFrame.Draw3x3(50, 50, 600, 600, 7, 7, 7, 7);
      try
        InfoFont.PrintBrokenString(60, 650 - 30, Vector4(1, 1, 0.5, 1),
          Info_Text, 580, true, 1);
      finally
      end;
    end;
  end;

  Form1.Label1.Caption := IntToStr(Round((Now - t) * 24 * 60 * 60 * 1000));
end;

var
  MinimapRectangle: TGLImage;

procedure TForm1.CastleControl2Render(Sender: TObject);
var
  ix, iy: Integer;
  ShadeColor: TVector4;
  shadebright: single;
  i: Integer;
  mScale: single;
  sx, sy: Integer;
  xShift, yShift: single;
begin
  if MinimapRectangle = nil then
    MinimapRectangle := TGLImage.Create(DataFolder + 'png' + PathDelim +
      'MinimapRectangle.png');

  if MaxX >= MaxY then
    mScale := 1 / MaxX * Form1.CastleControl2.Width
  else
    mScale := 1 / MaxY * Form1.CastleControl2.Height;
  if MaxX > MaxY then
    yShift := (MaxX - MaxY) / 2
  else
    yShift := 0;
  if MaxX < MaxY then
    xShift := (MaxY - MaxX) / 2
  else
    xShift := 0;


  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
      if Vis^[ix, iy] > 0 then
      begin
        ShadeBright := 0.5 + 0.3 * ((Vis^[ix, iy] - OldVisible) /
          (MaxVisible - OldVisible));
        if Map^[ix, iy] >= MapWall then
          shadeColor := Vector4(ShadeBright * 1, ShadeBright *
            0.7, ShadeBright * 0.7, 1)
        else
          shadeColor := Vector4(ShadeBright * 0.7, ShadeBright, ShadeBright, 1);
        sx := Round((ix - 1 + xShift) * mScale);
        sy := Round((MaxY - iy + yShift) * mScale);
        DrawRectangle(Rectangle(sx, sy, Round((ix + xShift) * mScale) - sx,
          Round((MaxY - iy + 1 + yShift) * mScale) - sy), shadeColor);
      end;
  shadeColor := Vector4(0.1, 0.7, 1, 1);
  for i := 1 to Itemsn do
    if Vis^[Item[i].X, Item[i].Y] > 0 then
    begin
      sx := Round((Item[i].X - 1 + xShift) * mScale);
      sy := Round((MaxY - Item[i].Y + yShift) * mScale);
      DrawRectangle(Rectangle(sx, sy, Round((Item[i].X + xShift) * mScale) -
        sx, Round((MaxY - Item[i].Y + 1 + yShift) * mScale) - sy), shadeColor);
    end;

  if (Zoom < MaxX) or (Zoom < MaxY) then
    MinimapRectangle.Draw3x3(Round((ViewX + xShift) * mScale), Round(
      (MaxY - ViewY - Zoom + yShift) * mScale), Round(Zoom * mScale),
      Round(Zoom * mScale), 1, 1, 1, 1);

  for i := 1 to NBot do
    if (Bot[i].HP > 0) and (Vis^[Bot[i].X, Bot[i].Y] > OldVisible) then
    begin
      if Bot[i].Owner = Player then
        shadeColor := Vector4(0, 1, 0, 1)
      else
        shadeColor := Vector4(1, 0, 0, 1);
      sx := Round((Bot[i].X - 1 + xShift) * mScale);
      sy := Round((MaxY - Bot[i].Y + yShift) * mScale);
      DrawRectangle(Rectangle(sx, sy, Round((Bot[i].X + xShift) * mScale) -
        sx, Round((MaxY - Bot[i].Y + 1 + yShift) * mScale) - sy), shadeColor);
    end;
end;

{==============================}

procedure TForm1.FormCreate(Sender: TObject);
var
  Duration: Double;
  CharSet: TUnicodeCharList;
begin
  txt_out := TStringList.Create;
  AnimationTimer := 0;
  Shot_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Shotgun.wav', Duration);
  falcon_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Shotgun_heavy.wav', Duration);
  Reload_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Reload.wav', Duration);
  unLoad_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Reload_reverse.wav', Duration);
  clip_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Clip.wav', Duration);
  unclip_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Clip_reverse.wav', Duration);
  Item_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Item.wav', Duration);
  Drop_sound := SoundEngine.LoadBuffer(DataFolder + 'wav' + PathDelim +
    'Item_reverse.wav', Duration);

  Timer1.Enabled := true;
  Timer1.Interval := 1000;
  oldMusic := -1;

  SoundEngine.ParseParameters;
  SoundEngine.MinAllocatedSources := 1;

  CharSet := TUnicodeCharList.Create;
  CharSet.Add(SimpleAsciiCharacters);
  CharSet.Add(
    'ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮёйцукенгшщзхъфывапролджэячсмитьбю');
  DamageFont := TTextureFont.Create(DataFolder + 'DejaVuSans', 20, true);
  InfoFont := TTextureFont.Create(DataFolder + 'DejaVuSans', 18, true, CharSet);
  MoveFont := TTextureFont.Create(DataFolder + 'DejaVuSans', 9, true);
  FreeAndNil(CharSet);

  //   CastleControl1.DoubleBuffered:=true;
{$IFDEF WINDOWS}
  Form1.Color := cldefault;
  Label1.Font.Color := cldefault;
  Label2.Font.Color := cldefault;
  Label3.Font.Color := cldefault;
  Label4.Font.Color := cldefault;
  Label5.Font.Color := cldefault;
  Label6.Font.Color := cldefault;
  Label7.Font.Color := cldefault;
  Label8.Font.Color := cldefault;
  Label10.Font.Color := cldefault;
  Label11.Font.Color := cldefault;
  Label12.Font.Color := cldefault;
  Label14.Font.Color := cldefault;
  Label15.Font.Color := cldefault;
  Label16.Font.Color := cldefault;
  Label17.Font.Color := cldefault;
  Label18.Font.Color := cldefault;
  Label19.Font.Color := cldefault;
  Label20.Font.Color := cldefault;
  Label21.Font.Color := cldefault;
  CheckBox1.Font.Color := cldefault;
  CheckBox2.Font.Color := cldefault;
  CheckBox3.Font.Color := cldefault;
  CheckBox4.Font.Color := cldefault;
  CheckBox5.Font.Color := cldefault;
  CheckBox6.Font.Color := cldefault;
  CheckBox7.Font.Color := cldefault;
  CheckBox8.Font.Color := cldefault;
  RadioButton1.Font.Color := cldefault;
  RadioButton2.Font.Color := cldefault;
  RadioButton3.Font.Color := cldefault;
  Edit1.Color := cldefault;
  Edit2.Color := cldefault;
  Edit3.Color := cldefault;
  Edit4.Color := cldefault;
  Edit5.Color := cldefault;
  Edit6.Color := cldefault;
  Edit7.Color := cldefault;
  Edit1.Font.Color := cldefault;
  Edit2.Font.Color := cldefault;
  Edit3.Font.Color := cldefault;
  Edit4.Font.Color := cldefault;
  Edit5.Font.Color := cldefault;
  Edit6.Font.Color := cldefault;
  Edit7.Font.Color := cldefault;
{$ENDIF}

  MaxX := MaxMaxX;
  MaxY := MaxMaxY;

  Edit1.Text := IntToStr(9);
  Edit2.Text := IntToStr(30);
  Edit5.Text := IntToStr(4);
  ReadINIFile;

  //  ViewSizeX:=30;
  //  ViewSizeY:=30;

  Create_language_interface;

  Label5.Caption := Txt[11] + ' (min ' + IntToStr(MinMaxX) + ' ... max ' +
    IntToStr(MaxMaxX) + '):';
  Label11.Caption := '(1..' + IntToStr(MaxBots - MaxPlayers) + ')';
  Label12.Caption := '(1..' + IntToStr(MaxPlayers) + ')';

  New(map);
  New(MapStatus);
  New(vis);
  New(MapChanged);
  New(Movement);
  New(distance);
  New(LOS_base);
  New(botvis);
  DoExplosion := false;
  New(Explosion_area);
  DoHighlight := false;

  GameMode := GameMode_None;
  Form1.DoubleBuffered := true;

  Read_Buildings;

  //GenerateMap;
  MapGenerated := false;
  ToggleBox1.Checked := true;
  ToggleBox1.State := cbChecked;
  set_ProgressBar(false);

end;

{================================================================================}
{================================================================================}
{================================================================================}

procedure hit_bot(Thisbot, dam: Integer);
var
  j, Ammo_d: Integer;
  smk: Integer;
begin
  if Bot[Thisbot].Items[1].State > Round(dam * ItemDamageRate) then
    Dec(Bot[Thisbot].Items[1].State, Round(dam * ItemDamageRate))
  else
    Bot[Thisbot].Items[1].State := 0;
  Bot[Thisbot].Action := ActionAttack;

  Dotxt := true;
  txt_out.Clear;
  txt_out.append(IntToStr(dam));
  txtX := getx(Bot[Thisbot].X){-ZoomScale div 2};
  txtY := gety(Bot[Thisbot].Y){+ZoomScale div 2};
  AnimationTimer := Now;
  repeat
    Form1.CastleControl1.Paint;
  until (Now - AnimationTimer) * 24 * 60 * 60 * 1000 > HitDelay;
  Dotxt := false;
  Form1.DrawMap;

  if Bot[Thisbot].HP > dam then
    Dec(Bot[Thisbot].HP, dam)
  else
  begin
    Bot[Thisbot].HP := 0;

    smk := MapStatus^[Bot[Thisbot].X, Bot[Thisbot].Y] + DieSmoke;
    if smk > MapSmoke then
      smk := MapSmoke;
    MapStatus^[Bot[Thisbot].X, Bot[Thisbot].Y] := smk;

    Form1.Memo1.Lines.Add(Bot[Thisbot].Name + ' ' + Txt[30]);
    if Thisbot = Selected then
    begin
      Form1.ScrollBar1.Position := 0;
      Form1.ScrollBar2.Position := 0;
      Selected := -1;
    end;
    if Thisbot = SelectedEnemy then
    begin
      SelectedEnemy := -1;
      Form1.Generate_Enemy_List(true);
    end;
    MovementMapfor := -1;
    if (Bot[Thisbot].Owner = Player) and (This_turn <> Player) then
      Form1.ClearVisible;       //and Enemy turn!!!!
    //Drop Items//Drop corpse
    for j := 1 to BackpackSize do
      if ((Bot[Thisbot].Items[j].W_ID > 0) or (Bot[Thisbot].Items[j].Ammo_ID > 0)) then
      begin
        if Itemsn = MaxItems then
          ShowMessage('TOO MANY ItemS!!!')
        else
          Inc(Itemsn);
        Item[Itemsn].X := Bot[Thisbot].X;
        Item[Itemsn].Y := Bot[Thisbot].Y;
        Item[Itemsn].Item := Bot[Thisbot].Items[j];
        if j = 1 then
        begin
          Item[Itemsn].Item.State :=
            Round(0.9 * Item[Itemsn].Item.State - 0.2 *
            Item[Itemsn].Item.State * Random);
          Ammo_d := Item[Itemsn].Item.N - Item[Itemsn].Item.N div 3 - Round(Random);
          if Ammo_d > 0 then
            Item[Itemsn].Item.N := Ammo_d
          else
            Item[Itemsn].Item.Ammo_ID := 0;
        end;
      end;
  end;
end;

const
  maxBlast = 50;
  maxGeneration = 50;           //no greater than 60!!!

type
  Blastarea = array[-maxBlast..MaxBlast, -maxBlast..MaxBlast] of shortint;

procedure TForm1.calculate_area(ax, ay, a, aSmoke, aBlast: Integer);
var
  ix, iy, dx, dy, ddx, ddy, Count, Generation, i, j: Integer;
  Flg, FlgX: Boolean;
  directionX, directionY: ^Blastarea;
var
  area: ^Blastarea;
  Generationsum: Single;
  //    reGeneratelos:Boolean;
  smk: Integer;
  //tmp_area:^Blastarea;
begin
  New(area);
  New(directionX);
  New(directionY);

  for dx := -maxBlast to maxBlast do
    for dy := -maxBlast to maxBlast do
    begin
      if (ax + dx > 1) and (ax + dx < MaxX) and (ay + dy > 1) and (ay + dy < MaxY) then
      begin
        if Map^[ax + dx, ay + dy] < MapWall then
          area^[dx, dy] := 127
        else
          area^[dx, dy] := 126;
      end
      else
        area^[dx, dy] := 125;
      directionX^[dx, dy] := 0;
      directionY^[dx, dy] := 0;
    end;

  area^[0, 0] := 1;

  Generation := 1;
  Count := 1;
  //New(tmp_area);
  repeat
    Inc(Generation);
    //tmp_area^:=area^;
    for ix := -Generation to Generation do
      if Abs(ix) < maxBlast then
        for iy := -Generation to Generation do
          if Abs(iy) < maxBlast then
          begin
            if area^[ix, iy] = 127 then
            begin
              for dx := -1 to 1 do
                for dy := -1 to 1 do
                  if {tmp_}area^[ix + dx, iy + dy] < Generation then
                  begin
                    area^[ix, iy] := Generation;
                    Inc(directionX^[ix, iy], dx);
                    Inc(directionY^[ix, iy], dy);
                  end;
              if area^[ix, iy] = Generation then
                Inc(Count);
            end
            else if area^[ix, iy] = 126 then
            begin
              for dx := -1 to 1 do
                for dy := -1 to 1 do
                  if area^[ix + dx, iy + dy] < Generation then
                    area^[ix, iy] := Generation + 60;
            end;
          end;
    //Walls

  until (Count >= a) or (Generation >= maxGeneration);
  //Dispose(tmp_area);

  Generationsum := 0;
  if Generation > maxBlast then
    Generation := maxBlast;

  for ix := -Generation to Generation do
    for iy := -Generation to Generation do
      if area^[ix, iy] <= maxGeneration then
        Generationsum := Generationsum + 1 / Sqrt(area^[ix, iy]);

  //Draw Explosion
  //prepare Drawing matrix
  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
    begin
      Explosion_area^[ix, iy] := 0;
      if Vis^[ix, iy] >= OldVisible then
      begin
        dx := ix - ax;
        dy := iy - ay;
        if (Abs(dx) < Generation) and (Abs(dy) < Generation) then
          if (area^[dx, dy] <= maxGeneration) then
          begin
            i := Round(1.5 * (Sqr((aBlast / 10 / Generationsum) / Sqrt(area^[dx, dy])) +
              ZoomScale / 3));
            if i > ZoomScale * 2 then
              i := ZoomScale * 2;
            Explosion_area^[ix, iy] := i;
          end;
      end;
    end;

  DoExplosion := true;
  AnimationTimer := Now;
  repeat
    CastleControl1.Paint;
  until (Now - AnimationTimer) * 24 * 60 * 60 * 1000 > BlastDelay;
  DoExplosion := false;
  DrawMap;

  //Damage tagets //make Smoke // destroy Walls
  //   reGenerateLOS:=false;
  for dx := -Generation to Generation do
    for dy := -Generation to Generation do
      if (ax + dx > 1) and (ax + dx < MaxX) and (ay + dy > 1) and
        (ay + dy < MaxY) and (area^[dx, dy] < 125) then
      begin
        if Map^[ax + dx, ay + dy] < MapWall then
        begin
          ix := Round((aSmoke / Generationsum) / Sqrt(area^[dx, dy]));
          if ix < 1 then
            ix := 1;

          smk := MapStatus^[ax + dx, ay + dy] + ix;
          if smk > MapSmoke then
            smk := MapSmoke;
          MapStatus^[ax + dx, ay + dy] := smk;

          MapChanged^[ax + dx, ay + dy] := 255;

          for i := 1 to NBot do
            if (Bot[i].X = ax + dx) and (Bot[i].Y = ay + dy) and (Bot[i].HP > 0) then
            begin
              ix := Round((aBlast / Generationsum) / Sqrt(area^[dx, dy]));
              if ix < 1 then
                ix := 1;
              Memo1.Lines.Add(Bot[i].Name + ' ' + Txt[31] + ' ' +
                IntToStr(ix) + ' ' + Txt[32]);
              hit_bot(i, ix);
            end;
        end
        else
        begin
          ix := MapStatus^[ax + dx, ay + dy] - Round(
            (aBlast / Generationsum) / Sqrt(area^[dx, dy] - 60) / WallHardness);
          MapChanged^[ax + dx, ay + dy] := 255;
          if (ix < 0) then
          begin
            Map^[ax + dx, ay + dy] := Map^[ax + dx, ay + dy] - MapWall;
            MapStatus^[ax + dx, ay + dy] := MapWallSmoke; //tile

            for ddx := -VisibleRange to VisibleRange do
              if (ax + dx + ddx > 0) and (ax + dx + ddx <= MaxX) then
                for ddy := -VisibleRange to VisibleRange do
                  if (ay + dy + ddy > 0) and (ay + dy + ddy <= MaxY) then
                    if checkLOS(ax + dx, ay + dy, ax + dx + ddx, ay +
                      dy + ddy, false) > 0 then
                      LOS_base^[ax + dx + ddx, ay + dy + ddy] := 255;
            reGenerateLOS := true;

          end
          else
          begin
            MapStatus^[ax + dx, ay + dy] := ix;
          end;
        end;
      end;
  if (reGenerateLOS) and (This_turn <> Player) then
    GenerateLOS_baseMap{(false,ax+dx,ay+dy)};

  //Push surviving tagets
  for i := 1 to NBot do
    if (Bot[i].HP > 0) then
    begin
      FlgX := true;
      for dx := -Generation to Generation do
        if FlgX then
          for dy := -Generation to Generation do
            if FlgX then
              if (Bot[i].X = ax + dx) and (Bot[i].Y = ay + dy) and
                (area^[dx, dy] <= maxGeneration) then
              begin
                FlgX := false;
                ix := Round((aBlast / Generationsum) / Sqrt(area^[dx, dy]));
                if ix > BlastPush then
                begin
                  MovementMapfor := -1;
                  MapChanged^[Bot[i].X, Bot[i].Y] := 255;
                  if Map^[Bot[i].X - sgn(directionX^[dx, dy]),
                    Bot[i].Y - sgn(directionY^[dx, dy])] < MapWall then
                  begin
                    Flg := true;
                    for j := 1 to NBot do
                      if (Bot[j].X = Bot[i].X - sgn(directionX^[dx, dy])) and
                        (Bot[j].Y = Bot[i].Y - sgn(directionY^[dx, dy])) and
                        (Bot[j].HP > 0) then
                        Flg := false;
                    if Flg then
                    begin
                      //               if DebugMode then Memo1.Lines.Add('[dbg] Push '+Bot[i].Name);
                      Dec(Bot[i].X, sgn(directionX^[dx, dy]));
                      Dec(Bot[i].Y, sgn(directionY^[dx, dy]));
                      MapChanged^[Bot[i].X, Bot[i].Y] := 255;
                    end;
                  end
                  else
                  if Map^[Bot[i].X, Bot[i].Y - sgn(directionY^[dx, dy])] < MapWall then
                  begin
                    Flg := true;
                    for j := 1 to NBot do
                      if (Bot[j].X = Bot[i].X) and
                        (Bot[j].Y = Bot[i].Y - sgn(directionY^[dx, dy])) and
                        (Bot[j].HP > 0) then
                        Flg := false;
                    if Flg then
                    begin
                      //               if DebugMode then Memo1.Lines.Add('[dbg] Push '+Bot[i].Name);
                      //dec(Bot[i].X,sgn(directionX^[dx,dy]));
                      Dec(Bot[i].Y, sgn(directionY^[dx, dy]));
                      MapChanged^[Bot[i].X, Bot[i].Y] := 255;
                    end;
                  end
                  else
                  if Map^[Bot[i].X - sgn(directionX^[dx, dy]), Bot[i].Y] < MapWall then
                  begin
                    Flg := true;
                    for j := 1 to NBot do
                      if (Bot[j].X = Bot[i].X - sgn(directionX^[dx, dy])) and
                        (Bot[j].Y = Bot[i].Y) and (Bot[j].HP > 0) then
                        Flg := false;
                    if Flg then
                    begin
                      //               if DebugMode then Memo1.Lines.Add('[dbg] Push '+Bot[i].Name);
                      Dec(Bot[i].X, sgn(directionX^[dx, dy]));
                      //dec(Bot[i].Y,sgn(directionY^[dx,dy]));
                      MapChanged^[Bot[i].X, Bot[i].Y] := 255;
                    end;
                  end;
                end;
              end;
    end;


  if This_turn = Computer then
    ClearVisible
  else
  begin
    for i := 1 to NBot do
      if Bot[i].Owner = Player then
      begin
        look_aRound(i);
        Generate_Enemy_List(true);
      end;
    DrawMap;
  end;

  Dispose(area);
  Dispose(directionX);
  Dispose(directionY);
end;


{-------------------------------------------------------------------}

procedure tForm1.alert_other_Bots(target, who: Integer);
var
  j: Integer;
begin
  for j := 1 to NBot do
    if (Bot[j].Owner = Computer) and (Bot[j].HP > 0) then
      if Sqr(Bot[j].X - Bot[who].X) + Sqr(Bot[j].Y - Bot[who].Y) <
        CallReInforcementsRange then
      begin
        Bot[j].target := target;
        Bot[j].Action := ActionAttack;
      end;

end;

{--------------------------------------------------------------------------------}

procedure tForm1.BotShots(attacker, defender: Integer);
var
  dx: Integer;
  Damage: Integer;
  LOS: Integer;
  Flg: Boolean;

  Timetoattack: word;
  Range, Statedamper: Single;
  ACC, DAM: Single;
begin
  Flg := true;
  if (Bot[attacker].Items[1].W_ID > 0) and (Bot[attacker].Items[1].Ammo_ID > 0) then
  begin
    Timetoattack := WeaponSpecifications[Bot[attacker].Items[1].W_ID].aim +
      Bot[attacker].Items[1].RechargeState;
    if Bot[attacker].Items[1].N = 0 then
    begin
      Flg := false;
    end;
    if Bot[attacker].Items[1].State = 0 then
    begin
      if Bot[attacker].Owner = Player then
        ShowMessage(Txt[33]);
      Flg := false;
    end;
  end
  else
  begin
    Flg := false;
    if Bot[attacker].Owner = Player then
      ShowMessage(Txt[34]);
  end;

  if (Flg) and (Bot[attacker].TU >= Timetoattack) and (Bot[defender].HP > 0) then
  begin
    LOS := checkLOS(Bot[attacker].X, Bot[attacker].Y, Bot[defender].X,
      Bot[defender].Y, true);
    if LOS > 0 then
    begin
      ACC := WeaponSpecifications[Bot[attacker].Items[1].W_ID].Acc +
        AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].Acc + 4;
      DAM := WeaponSpecifications[Bot[attacker].Items[1].W_ID].Dam +
        AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].Dam;
      //add modifiers
      Range := (AccuracyAccuracy * Sqrt(Sqr(Bot[defender].X - Bot[attacker].X) +
        Sqr(Bot[defender].Y - Bot[attacker].Y)) - ACC) / AccuracyAccuracy;
      if Range < 1 then
        Range := 1;
      if Bot[attacker].Items[1].State < Bot[attacker].Items[1].MaxState div 3 then
        Statedamper := Bot[attacker].Items[1].State /
          (Bot[attacker].Items[1].MaxState / 3)
      else
        Statedamper := 1;
      Damage := Round(Statedamper * DAM / Sqrt(Sqrt(Range)));
      if Damage < 1 then
        Damage := 1;
      {if Damage>0 then} begin
        Memo1.Lines.Add(Bot[attacker].Name + ' ' + Txt[35] + ' ' +
          Bot[defender].Name + ' ' + Txt[36] + ' ' + IntToStr(Damage));


        if Bot[defender].Owner = Computer then
        begin
          alert_other_Bots(attacker, defender);
        end;

        hit_bot(defender, Damage);

        SpendTU(attacker, Timetoattack);

        //modifiers;

        if Bot[attacker].Y - Bot[defender].Y <> 0 then
        begin
          dx := Round(MaxAngle * arctan((Bot[attacker].X - Bot[defender].X) /
            (Bot[attacker].Y - Bot[defender].Y)) / 2 / Pi);
          if Bot[attacker].Y - Bot[defender].Y > 0 then
            dx := MaxAngle * 3 div 4 - dx
          else
            dx := MaxAngle div 4 - dx;
          Bot[attacker].Angle := dx;
        end
        else
        begin
          if Bot[attacker].X - Bot[defender].X > 0 then
            Bot[attacker].Angle := MaxAngle div 2
          else
            Bot[attacker].Angle := 0;
        end;

        MapChanged^[Bot[attacker].X, Bot[attacker].Y] := 255;
        if (Bot[attacker].X <= ViewX - 1 + 2) or (Bot[attacker].Y <= ViewY - 1 + 2) or
          (Bot[attacker].X >= ViewX + ViewSizeX - 2) or
          (Bot[attacker].Y >= ViewY + ViewSizeY - 2) or
          (Bot[defender].X <= ViewX - 1 + 2) or (Bot[defender].Y <= ViewY - 1 + 2) or
          (Bot[defender].X >= ViewX + ViewSizeX - 2) or
          (Bot[defender].Y >= ViewY + ViewSizeY - 2) then
          CenterMap((Bot[attacker].X + Bot[defender].X) div 2,
            (Bot[attacker].Y + Bot[defender].Y) div 2);

        if WeaponSpecifications[Bot[attacker].Items[1].W_ID].Kind < 3 then
          SoundEngine.PlaySound(Shot_sound, false, false, 0, 1, 0, 1, TVector3.Zero)
        else
          SoundEngine.PlaySound(falcon_sound, false, false, 0, 1,
            0, 1, TVector3.Zero);

        //todo: Draw Shot here

        if AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].area > 0 then
        begin
          calculate_area(Bot[defender].X, Bot[defender].Y,
            AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].AREA,
            AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].Smoke,
            AmmoSpecifications[Bot[attacker].Items[1].Ammo_ID].Explosion);
          DrawMap;
        end;
        if Bot[attacker].HP > 0 then
        begin
          Dec(Bot[attacker].Items[1].N);
          if Bot[attacker].Items[1].State > 0 then
            Dec(Bot[attacker].Items[1].State);
          if Bot[attacker].Items[1].N > 0 then
            Bot[attacker].Items[1].RechargeState :=
              WeaponSpecifications[Bot[attacker].Items[1].W_ID].Recharge
          else
            Bot[attacker].Items[1].Ammo_ID := 0;
        end;

      end;
    end;
  end;
end;

{================================================================================}
{================================================================================}
{================================================================================}

procedure TForm1.growSmoke;
var
  ix, iy: Integer;
  i, j: Integer;
  dx, dy: shortint;
  Smoke_new: ^Maparray;
begin
  New(Smoke_new);

  for j := 1 to 3 do
  begin
    Smoke_new^ := MapStatus^;

    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
        if (Smoke_new^[ix, iy] = 1) and (Random > 0.7) then
          Smoke_new^[ix, iy] := 0;

    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
        if (MapStatus^[ix, iy] > 1) and (Map^[ix, iy] < MapWall) then
        begin
          i := 0;
          repeat
            Inc(i);
            dx := Round(Random * 2) - 1;
            dy := Round(Random * 2) - 1;
            if (ix + dx > 0) and (ix + dx <= MaxX) and (iy + dy > 0) and
              (iy + dy <= MaxY) and ((dx <> 0) or (dy <> 0)) then
            begin
              if (Smoke_new^[ix + dx, iy + dy] < MapStatus^[ix, iy]) and
                (Smoke_new^[ix + dx, iy + dy] < MapSmoke) then
              begin
                Dec(MapStatus^[ix, iy]); //tile
                Dec(Smoke_new^[ix, iy]); //tile
                {if Random>0.5 then }Inc(Smoke_new^[ix + dx, iy + dy]); //tile
              end;
            end;
          until (i >= 30) or (MapStatus^[ix, iy] = 1);
        end;
    MapStatus^ := Smoke_new^;
  end;
  Dispose(Smoke_new);
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.ClearVisible;
var
  x1, y1, i: Integer;
begin
  for x1 := 1 to MaxX do
    for y1 := 1 to MaxY do
      if Vis^[x1, y1] > 0 then
      begin
        Vis^[x1, y1] := OldVisible;
        MapChanged^[x1, y1] := 255;
      end;

  for i := 1 to NBot do
    if Bot[i].Owner = Player then
      look_aRound(i);
end;

procedure TForm1.BotClearVisible;
var
  x1, y1, i: Integer;
begin
  cheater_type := Strategy_TypeDamnCheater;
  if Strategy_cheater > 0 then
    for i := 1 to NBot do
      if (Bot[i].Owner = Computer) and (Bot[i].Action = ActionAttack) and
        (Bot[i].HP > 0) then
        cheater_type := Strategy_TypeCheater;

  //  if cheater_type=Strategy_TypeCheater then
  begin
    for x1 := 1 to MaxX do
      for y1 := 1 to MaxY do
        botVis^[x1, y1] := 0;
    for i := 1 to NBot do
      if Bot[i].Owner = Computer then
        Botlook_aRound(i);
  end;{ else begin
    for x1:=1 to MaxX do
      for y1:=1 to MaxY do if Vis^[x1,y1]>OldVisible then botVis^[x1,y1]:=Strategy_RealLOS;
  end;}
end;

procedure TForm1.Botcalculate_PossibleLoc(Thisbot: Integer);
var
  x1, y1: Integer;
  expecteddistance: Byte;
begin
  expecteddistance := DistanceAccuracy * Bot[Thisbot].lastseen_tu div
    Bot[Thisbot].Speed;
  GenerateMovement_generic(Bot[Thisbot].LastSeenX, Bot[Thisbot].LastSeenY,
    1, 1, expecteddistance, true, false{,true});
  for x1 := 1 to MaxX do
    for y1 := 1 to MaxY do
      if (distance^[x1, y1] <= expecteddistance) and
        (botVis^[x1, y1] <> Strategy_Visible) and (botVis^[x1, y1] <
        Strategy_RealLOS) then
        botVis^[x1, y1] := Strategy_PossibleLoc;
end;

procedure TForm1.Botcalculate_PossibleLOS;
var
  i, x1, y1, x2, y2: Integer;
begin
  if DebugMode then
    Memo1.Lines.Add('[dbg] los recalculated');
  for x1 := 1 to MaxX do
    for y1 := 1 to MaxY do
      if botVis^[x1, y1] = Strategy_PossibleLOS then
        botVis^[x1, y1] := 0;
  for i := 1 to NBot do
    if Bot[i].Owner = Computer then
      Botlook_aRound(i);

  for x1 := 1 to MaxX do
    for y1 := 1 to MaxY do
      if botVis^[x1, y1] = Strategy_PossibleLoc then
      begin
        for x2 := 1 to MaxX do
          for y2 := 1 to MaxY do
            if (Map^[x2, y2] < MapWall) and (botVis^[x2, y2] <>
              Strategy_PossibleLoc) and (botVis^[x2, y2] <> Strategy_PossibleLOS) and
              (botVis^[x2, y2] < Strategy_RealLOS) then
              if checkLOS(x1, y1, x2, y2, true) > 0 then
                botVis^[x2, y2] := Strategy_PossibleLOS;
      end;
  recalculateLOS_Strategy := false;
end;

procedure TForm1.BotrecalcVisible;
var
  i: Integer;
begin
  if DebugMode then
    Memo1.Lines.Add('[dbg] Visible recalculated');
  recalculateLOS_Strategy := false;
  useLOS_Strategy := false;
  for i := 1 to NBot do
    if (Bot[i].Owner = Player) and (Bot[i].HP > 0) then
    begin
      if (cheater_type = Strategy_TypeDamnCheater) or
        (botVis^[Bot[i].X, Bot[i].Y] = Strategy_Visible) or
        (botVis^[Bot[i].X, Bot[i].Y] = Strategy_RealLOS) or
        (Bot[i].lastseen_tu < Bot[i].Speed) then
      begin
        Botlook_aRound(i);  // REAL LOS
      end
      else
      begin
        if Bot[i].lastseen_tu > Strategy_cheater then
        begin
          Bot[i].LastSeenX := Bot[i].X;
          Bot[i].LastSeenY := Bot[i].Y;
          Bot[i].lastseen_tu := Strategy_cheater;
        end;
        recalculateLOS_Strategy := true;
        useLOS_Strategy := true;
        Botcalculate_PossibleLoc(i);
      end;
      if DebugMode then
        Memo1.Lines.Add('[dbg/recalc] ' + Bot[i].Name + ' lastseen: ' +
          IntToStr(Bot[i].LastSeenX) + '.' + IntToStr(Bot[i].LastSeenY) +
          ' / ' + IntToStr(Bot[i].lastseen_tu) + ' tu');
    end;
  if not useLOS_Strategy then
    cheater_type := Strategy_TypeDamnCheater;
{  needLOS_Strategy:=false;
  for i:=1 to NBot do if (Bot[i].Owner=Player) and (Bot[i].Owner=attack) then needLOS_Strategy:=true;}
end;

{--------------------------------------------------------------------------------------}

const
  maxLOS_targets = MaxPlayers;

procedure TForm1.BotAction(Thisbot: Integer);
var
  LOS_targets: array[1..MaxLOS_targets] of Integer;
  j, k, l: Integer;
  StopActions, Flg, Weaponneeded: Boolean;
  Ammo_available, Weapon_available: Byte;
  Range, lastRange, aim, aim_at_weak: Integer;
  aim_at_weakValue: single;
  x1, y1, dx, dy: Integer;
  TimetoShot: Integer;
  cautionTime: Integer;
  passCount: Byte;
  I_amVisible: Boolean;
begin
  passCount := 0;
  //  if cheater_type=Strategy_TypeCheater then Botlook_aRound(Thisbot);
  repeat
    if cheater_type = Strategy_TypeCheater then
    begin
      if {(Bot[Thisbot].Action=ActionAttack) and} (recalculateLOS_Strategy) and
        (useLOS_Strategy) then
        Botcalculate_PossibleLOS;
    end;

    StopActions := false;
    Inc(passCount);
    if passCount > 250 then
      StopActions := true;

    //check available backup Weapon & Ammo
    Ammo_available := 0;
    Weapon_available := 0;
    for j := 2 to BackpackSize do
    begin
      if (Bot[Thisbot].Items[j].W_ID > 0) and (Bot[Thisbot].Items[j].N > 0) and
        (Bot[Thisbot].Items[j].State > 0) then
      begin
        if (Bot[Thisbot].Items[j].State > Bot[Thisbot].Items[j].MaxState div 3) and
          (Bot[Thisbot].Items[j].N > 3) then
          Weapon_available := j;
        if (Weapon_available = 0) and (Bot[Thisbot].Items[j].State > 0) and
          (Bot[Thisbot].Items[j].N > 0) then
          Weapon_available := j;
      end
      else
      if (Bot[Thisbot].Items[j].Ammo_ID > 0) and (Bot[Thisbot].Items[j].N > 0) and
        (Bot[Thisbot].Items[1].W_ID > 0) then
      begin
        Flg := false;
        for k := 1 to maxUsableAmmo do
          if WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].Ammo[k] =
            Bot[Thisbot].Items[j].Ammo_ID then
            Flg := true;
        if Flg then
        begin
          if (Bot[Thisbot].Items[j].N > 3) then
            Ammo_available := j;
          if (Ammo_available = 0) then
            Ammo_available := j;
        end;
      end;
    end;
    // Reload Weapon if needed
    Weaponneeded := false;
    if (Bot[Thisbot].Items[1].W_ID <= 0) or (Bot[Thisbot].Items[1].State = 0) or
      ((Bot[Thisbot].Items[1].State < Bot[Thisbot].Items[1].MaxState div 4) and
      (Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] <= OldVisible)) then
    begin
      if Weapon_available > 0 then
        LoadWeapon(Thisbot, Weapon_available)
      else
        Weaponneeded := true;
    end;
    // Reload Ammo if needed
    if (Bot[Thisbot].Items[1].N = 0) then
    begin
      if Ammo_available > 0 then
        LoadWeapon(Thisbot, Ammo_available)
      else
      if Weapon_available > 0 then
        LoadWeapon(Thisbot, Weapon_available)
      else
        Weaponneeded := true;
    end;
    // seek Weapon
    if (Weaponneeded) and (Itemsn > 0) then
    begin
      // go find Closest Weapon State>0
      lastRange := 256;
      aim := 0;
      for j := 1 to Itemsn do
        if (Item[j].Item.W_ID > 0) and (Item[j].Item.State > 0) and
          (Item[j].Item.N > 0) then
        begin
          GenerateMovement(Thisbot, Item[j].X, Item[j].Y);
          if (distance^[Item[j].X, Item[j].Y] < lastRange) and
            (Movement^[Item[j].X, Item[j].Y] < 10) then
          begin
            Flg := true;
            for k := 1 to NBot do
              if (Bot[k].HP > 0) and (k <> Thisbot) and (Bot[k].X = Item[j].X) and
                (Bot[k].Y = Item[j].Y) then
                Flg := false;
            if (aim > 0) and (Item[j].Item.State < Item[j].Item.MaxState div 3) and
              (Item[j].Item.N < 4) then
              Flg := false;
            if (Flg) then
            begin
              aim := j;
              lastRange := distance^[Item[j].X, Item[j].Y];
            end;
          end;
        end;
      if (aim > 0) and ((Bot[Thisbot].Items[BackpackSize].Ammo_ID > 0) or
        (Bot[Thisbot].Items[BackpackSize].W_ID > 0)) then
        for j := 2 to BackpackSize do
          Drop_Item(Thisbot, j);
      if (aim = 0) then
        StopActions := true {!}
      else
        Move_bot(Thisbot, Item[aim].X, Item[aim].Y, 100);
    end;
    //see Items on the gRound and take if vis<0 or needed
    find_OnFloor(Bot[Thisbot].X, Bot[Thisbot].Y);
    if (OnFloorn > 0) and ((Weaponneeded) or
      (Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] <= OldVisible)) then
    begin
      j := 1;
      repeat
        if Item[OnFloor[j]].Item.State > 0 then
        begin
          pick_up(Thisbot, j);
          j := OnFloorn;
        end;
        Inc(j);
      until j > OnFloorn;
    end;
    //get LOS tagets
    if (Bot[Thisbot].Action = ActionAttack) and (Bot[Bot[Thisbot].target].HP = 0) then
    begin
      for j := 1 to NBot do
        if (Bot[j].Owner = Player) and (Bot[j].HP > 0) then
          Bot[Thisbot].target := j;
      if Bot[Bot[Thisbot].target].HP = 0 then
        Bot[Thisbot].Action := ActionRandom;
    end;


    k := 0;
    for j := 1 to Playersn do
      if (checkLOS(Bot[Thisbot].X, Bot[Thisbot].Y, Bot[j].X, Bot[j].Y, true) > 0) and
        (Bot[j].HP > 0) then
      begin
        Inc(k);
        LOS_targets[k] := j;
      end;
    //if LOS not empty - attack&Move
    if k > 0 then
    begin
      lastRange := Sqr(MaxX) + Sqr(MaxY);
      aim := 0;
      aim_at_weak := 0;
      aim_at_weakValue := 0;
      for j := 1 to k do
      begin
        Range := Sqr(Bot[Thisbot].X - Bot[LOS_targets[j]].X) +
          Sqr(Bot[Thisbot].Y - Bot[LOS_targets[j]].Y);
        if Range < 1 then
          Range := 1;
        if Range < lastRange then
        begin
          aim := LOS_targets[j];
          lastRange := Range;
        end;
        if 1 / Sqrt(Sqrt(Range)) / Bot[LOS_targets[j]].HP > aim_at_weakValue then
        begin
          aim_at_weak := LOS_targets[j];
          aim_at_weakValue := 1 / Sqrt(Sqrt(Range)) / Bot[LOS_targets[j]].HP;
        end;
      end;
      Bot[Thisbot].Action := ActionAttack;
      Bot[Thisbot].target := aim;
      if Strategy_finishhim then
        Bot[Thisbot].target := aim_at_weak;
      alert_other_Bots(Bot[Thisbot].target, Thisbot);
      BotShots(Thisbot, Bot[Thisbot].target);
    end;

    //Escape Visible Range or at least minimize LOS
    TimetoShot := Bot[Thisbot].Items[1].RechargeState +
      WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].aim;
    cautionTime := TimetoShot + Bot[Thisbot].caution;
    if 255 - cautionTime < TimetoShot then
      cautionTime := TimetoShot;
    if Strategy_Hide then
      if (Bot[Thisbot].TU < cautionTime) and (Bot[Thisbot].TU >= Bot[Thisbot].Speed)
      {and (Vis^[Bot[Thisbot].X,Bot[Thisbot].Y]>OldVisible)} then
      begin
        if DebugMode then
          Memo1.Lines.Add('[dbg] ' + Bot[Thisbot].Name + ' caution Time used ' +
            IntToStr(Bot[Thisbot].TU) + ' of ' + IntToStr(cautionTime));
        x1 := Bot[Thisbot].X;
        y1 := Bot[Thisbot].Y;
        GenerateMovement(Thisbot, Bot[Bot[Thisbot].target].X,
          Bot[Bot[Thisbot].target].Y);
        //buggy
        for dx := Bot[Thisbot].X - Trunc(Bot[Thisbot].TU / Bot[Thisbot].Speed)
          to Bot[Thisbot].X + Trunc(Bot[Thisbot].TU / Bot[Thisbot].Speed) do
          if (dx > 1) and (dx < MaxX) then
            for dy := Bot[Thisbot].Y - Trunc(Bot[Thisbot].TU / Bot[Thisbot].Speed)
              to Bot[Thisbot].Y + Trunc(Bot[Thisbot].TU / Bot[Thisbot].Speed) do
              if (dy > 1) and (dy < MaxY) then
                if (Map^[dx, dy] < MapWall) and (distance^[dx, dy] > 0) and
                  (distance^[dx, dy] / DistanceAccuracy * Bot[Thisbot].Speed <=
                  Bot[Thisbot].TU) and ((LOS_base^[x1, y1] > LOS_base^[dx, dy]) or
                  ((Vis^[x1, y1] > OldVisible) and (Vis^[dx, dy] <= OldVisible))) then
                  if (Vis^[dx, dy] <= OldVisible) or (Vis^[x1, y1] > OldVisible) then
                  begin
                    Flg := true;
                    for j := 1 to NBot do
                      if (Bot[j].X = dx) and (Bot[j].Y = dy) then
                        Flg := false;
                    if Flg then
                    begin
                      x1 := dx;
                      y1 := dy;
                    end;
                  end;
        if (x1 <> Bot[Thisbot].X) or (y1 <> Bot[Thisbot].Y) then
        begin
          //        if DebugMode then Memo1.Lines.Add('[dbg] '+Bot[Thisbot].Name+' Hides from vis'+IntToStr(Vis^[Bot[Thisbot].X,Bot[Thisbot].Y])+'/los'+IntToStr(LOS_base^[Bot[Thisbot].X,Bot[Thisbot].Y])+'/tu'+IntToStr(Bot[Thisbot].TU));
          Move_bot(Thisbot, x1, y1, 100);
          //        if DebugMode then Memo1.Lines.Add('[dbg] '+Bot[Thisbot].Name+' Hides to vis'+IntToStr(Vis^[Bot[Thisbot].X,Bot[Thisbot].Y])+'/los'+IntToStr(LOS_base^[Bot[Thisbot].X,Bot[Thisbot].Y])+'/tu'+IntToStr(Bot[Thisbot].TU));

        end;
        StopActions := true;
      end
      else //buggy

      //Action/attack - go to the nearest LOS
      if (Bot[Thisbot].Action = ActionAttack) and (Bot[Bot[Thisbot].target].HP > 0) then
      begin
        if (k = 0) or (Bot[Thisbot].Items[1].RechargeState >= Bot[Thisbot].Speed) then
        begin
          if Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] <= OldVisible then
          begin
            lastRange := 256;
            I_amVisible := false;
          end
          else
          begin
            lastRange := Sqr(Bot[Thisbot].X - Bot[Bot[Thisbot].target].X) +
              Sqr(Bot[Thisbot].Y - Bot[Bot[Thisbot].target].Y);
            I_amVisible := true;
          end;
          j := 1;
          Flg := false;
          GenerateMovement(Thisbot, Bot[Bot[Thisbot].target].X,
            Bot[Bot[Thisbot].target].Y);
          //buggy
          repeat
            for dx := Bot[Thisbot].X - j to Bot[Thisbot].X + j do
              for dy := Bot[Thisbot].Y - j to Bot[Thisbot].Y + j do
                if (dx > 0) and (dy > 0) and (dx <= MaxX) and (dy <= MaxY) then
                  if (Map^[dx, dy] < MapWall) and
                    ((botVis^[dx, dy] = Strategy_PossibleLOS) or
                    (botVis^[dx, dy] = Strategy_RealLOS)) {(Vis^[dx,dy]>OldVisible)} and
                    (((lastRange > distance^[dx, dy]) and
                    (distance^[dx, dy] > 0) and (not I_amVisible)) or
                    ((lastRange > Sqr(dx - Bot[Bot[Thisbot].target].X) +
                    Sqr(dy - Bot[Bot[Thisbot].target].Y)) and
                    (distance^[dx, dy] / DistanceAccuracy *
                    Bot[Thisbot].Speed <= Bot[Thisbot].Items[1].RechargeState) and
                    (I_amVisible))) then
                  begin
                    aim := 1;
                    for l := 1 to NBot do
                      if (Bot[l].X = dx) and (Bot[l].Y = dy) and (Bot[l].HP > 0) then
                        aim := 0;
                    if (aim = 1) and (Movement^[dx, dy] < 10) then
                    begin
                      x1 := dx;
                      y1 := dy;
                      if I_amVisible then
                        lastRange :=
                          Sqr(x1 - Bot[Bot[Thisbot].target].X) + Sqr(y1 -
                          Bot[Bot[Thisbot].target].Y)
                      else
                        lastRange := distance^[dx, dy];
                      Flg := true;
                    end;
                  end;
            Inc(j);
          until (Flg) or (j > MaxX);
          if Flg then
          begin
            if ((k <> 0) and (j <= 1 + TimetoShot / (Bot[Thisbot].Speed * Sqrt(2))) and
              (Bot[Thisbot].Items[1].RechargeState >= Bot[Thisbot].Speed *
              Sqrt(Sqr(Bot[Thisbot].X - x1) + Sqr(Bot[Thisbot].Y - y1)))) or
              (k = 0) then
              Move_bot(Thisbot, x1, y1, 1);
          end
          else
          if (k = 0) then
            StopActions := true;
          //        BotrecalcVisible;
        end;
      end;
    //Action/Random - walk aRound
    if (Bot[Thisbot].Action = ActionRandom) or (passCount > 100) then
    begin
      x1 := Bot[Thisbot].X + Round(2 * (Random - 0.5));
      y1 := Bot[Thisbot].Y + Round(2 * (Random - 0.5));
      if (Map^[x1, y1] < MapWall) and ((x1 <> Bot[Thisbot].X) or
        (y1 <> Bot[Thisbot].Y)) and ((LOS_base^[x1, y1] <
        LOS_base^[Bot[Thisbot].X, Bot[Thisbot].Y]) or (Random < 0.05 +
        (Bot[Thisbot].TU - 50) / 100)) then
        Move_bot(Thisbot, x1, y1, 2);
      //may be blocked and inable to use all tus!!!
    end;
    //Action/guard - stand still

    if Bot[Thisbot].Action = ActionGuard then
      StopActions := true;

    if Bot[Thisbot].TU < Bot[Thisbot].Speed * Sqrt(2) * 1.01 then
      StopActions := true;
    if (Bot[Thisbot].TU > TimetoShot) and
      (Vis^[Bot[Thisbot].X, Bot[Thisbot].Y] > OldVisible) and
      (Bot[Thisbot].Items[1].State > 0) and (Bot[Thisbot].Items[1].N > 0) then
      StopActions := false;
    if Bot[Thisbot].HP <= 0 then
      StopActions := true;
  until (StopActions);
end;


procedure TForm1.end_turn;
var
  i: Integer;
begin
  This_turn := Computer;

  growSmoke;
  SelectedEnemy := -1;
  Memo1.Clear;
  ClearVisible;
  DrawMap;
  //LOS - is "current" MapVisible;

  if (reGenerateLOS){ and (This_turn<>Player)} then
    GenerateLOS_baseMap{(false,ax+dx,ay+dy)};

  for i := 1 to NBot do
    if Bot[i].Owner <> Player then
    begin
      SpendTU(i, Bot[i].TU);
      Bot[i].TU := 255;
    end;

  BotClearVisible;
  if DebugMode then
  begin
    if cheater_type = Strategy_TypeCheater then
      Memo1.Lines.Add('[dbg/Clear] weak cheater')
    else
      Memo1.Lines.Add('[dbg] damncheater');
  end;
  BotrecalcVisible;
  if DebugMode then
  begin
    if cheater_type = Strategy_TypeCheater then
      Memo1.Lines.Add('[dbg/recalc] weak cheater')
    else
      Memo1.Lines.Add('[dbg] damncheater');
  end;

  set_ProgressBar(true);
  ProgressBar1.Max := NBot;
  for i := 1 to NBot do
    if (Bot[i].Owner <> Player) and (Bot[i].HP > 0) then
    begin
      BotAction(i);
      ProgressBar1.Position := i;
      ProgressBar1.update;
    end;

  start_turn;
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.start_turn;
var
  i: Integer;
  ix, iy: Integer;
  n1, n2: Integer;
  saveInfo: Boolean;
begin
  This_turn := Player;
  Inc(current_turn);
  set_ProgressBar(false);
  Memo1.Lines.Add(Txt[37] + ': ' + IntToStr(current_turn));
  growSmoke;

  ClearVisible;
  BotClearVisible;

  n1 := 0;
  n2 := 0;
  for i := 1 to NBot do
    if Bot[i].HP > 0 then
    begin
      if (Bot[i].Owner = Player) then
      begin
        if (Bot[i].TU > 0) then
          SpendTU(i, Bot[i].TU);
        Bot[i].TU := 255;
        Bot[i].LastSeenX := Bot[i].X;
        Bot[i].LastSeenY := Bot[i].Y;
        Bot[i].lastseen_tu := Bot[i].TU;
        if DebugMode then
          Memo1.Lines.Add('[dbg/newturn] ' + Bot[i].Name + ' lastseen: ' +
            IntToStr(Bot[i].LastSeenX) + '.' + IntToStr(Bot[i].LastSeenY) +
            ' / ' + IntToStr(Bot[i].lastseen_tu) + ' tu');
        Inc(n2);
      end
      else
        Inc(n1);
    end;
  if (n1 < n2) or (n1 < 3) then
  begin
    for i := 1 to NBot do
      if (Bot[i].HP > 0) and (Bot[i].Owner = Computer) and
        (Bot[i].Action <> ActionAttack) then
      begin
        Bot[i].Action := ActionAttack;
        Bot[i].target := Round(Random * (Playersn - 1)) + 1;
      end;
  end;
  Memo1.Lines.Add(Txt[38] + ': ' + IntToStr(n1));
  saveInfo := false; {check to save battle result in case of Victory or Defeat}
  if n1 = 0 then
  begin
    if GameMode <> GameMode_Victory then
    begin
      GameMode := GameMode_Victory;
      saveInfo := true;
    end;
    Memo1.Lines.Add(Txt[39]);
  end;
  if n2 = 0 then
  begin
    if GameMode <> GameMode_Victory then
    begin
      GameMode := GameMode_Defeat;
      saveInfo := true;
    end;
    Memo1.Lines.Add(Txt[40]);
  end;
  if (n1 = 0) or (n2 = 0) then
  begin
    Memo1.Lines.Add(Txt[22] + ' = ' + IntToStr(n2));
    Memo1.Lines.Add(Txt[24] + ' = ' + IntToStr(n1));
    for n1 := 1 to MaxX do
      for n2 := 1 to MaxY do
      begin
        Vis^[n1, n2] := MaxVisible;
        MapChanged^[n1, n2] := 255;
      end;
    ix := 0;
    iy := 0;
    for i := 1 to NBot do
      if Bot[i].Owner = Player then
        Inc(ix, Bot[i].HP)
      else
        Inc(iy, Bot[i].HP);
    Memo1.Lines.Add(Txt[23] + ' = ' + IntToStr(ix));
    Memo1.Lines.Add(Txt[25] + ' = ' + IntToStr(iy));
    if saveInfo then
    begin
      Infohash := Infohash + ix + iy;
    end;
  end;
  n1 := 0;
  n2 := 0;
  for ix := 1 to MaxX do
    for iy := 1 to MaxY do
      if Map^[ix, iy] < MapWall then
      begin
        Inc(n1);
        if Vis^[ix, iy] > 0 then
          Inc(n2);
      end;
  Memo1.Lines.Add(Txt[45] + ': ' + IntToStr(Round(n2 * 100 / n1)) + '%');
  Memo1.Lines.Add('------------------------------');
  Generate_Enemy_List(false);

  DrawMap;
  if saveInfo then
    dosaveInfo;
end;

{================================================================================}
{================================================================================}
{================================================================================}


procedure TForm1.Button1Click(Sender: TObject);
var
  i, n1, n2: Integer;
begin
  n1 := 0;
  n2 := 0;
  for i := 1 to NBot do
    if (Bot[i].Owner = Player) and (Bot[i].HP > 0) then
    begin
      Inc(n1);
      if Bot[i].TU = 255 then
        Inc(n2);
    end;
  if (n1 = n2) and (n1 > 0) then
  begin
    if MessageDlg(Txt[46], mtCustom, [mbYes, mbCancel], 0) = mrYes then
      end_turn;
  end
  else
  begin
    if (n2 > 0) and (CheckBox2.Checked) then
    begin
      if MessageDlg(Txt[47] + ' ' + IntToStr(n2) + ' ' + Txt[48],
        mtCustom, [mbYes, mbCancel], 0) = mrYes then
        end_turn;
    end
    else
      end_turn;
  end;
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.Button2Click(Sender: TObject);
var
  Buttonpressed: Boolean;
begin
  if (MapGenerated) and ((GameMode = GameMode_Game) or
    (GameMode = GameMode_ItemInfo)) then
    Buttonpressed := (MessageDlg(Txt[49], mtCustom, [mbYes, mbCancel], 0) = mrYes)
  else
    Buttonpressed := true;
  if Buttonpressed then
  begin
    Memo1.Clear;
    GenerateMap;
    ToggleBox1.Checked := false;
    ToggleBox1.State := cbUnchecked;
    GameMode := GameMode_Game;
    ToggleBox1.Enabled := true;
    ToggleBox1.Visible := true;
  end;
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.Button3Click(Sender: TObject);
var
  ix, iy: Integer;
begin
  if MessageDlg(Txt[50], mtCustom, [mbYes, mbCancel], 0) = mrYes then
  begin
    Selected := -1;
    Selectedx := -1;
    Selectedy := -1;
    SelectedEnemy := -1;
    SelectedItem := -1;
    SelectedOnFloor := -1;
    for ix := 1 to Playersn do
      Bot[ix].HP := 0;
    for ix := 1 to MaxX do
      for iy := 1 to MaxY do
      begin
        Vis^[ix, iy] := MaxVisible;
        MapChanged^[ix, iy] := 255;
      end;
    Generate_Enemy_List(false);
    DrawMapall := true;
    DrawMap;
    ToggleBox1.Checked := false;
    ToggleBox1.State := cbUnchecked;
    GameMode := GameMode_Defeat;
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  ToggleBox1.Checked := false;
  ToggleBox1.State := cbUnchecked;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Form1.Close;
end;

procedure TForm1.CastleControl1Press(Sender: TObject; const Event: TInputPressRelease);
var
  Shift: TShiftState;
begin
  Shift := [];
  if mkShift in Event.ModifiersDown then
    Shift := Shift + [ssShift];
  if mkCtrl in Event.ModifiersDown then
    Shift := Shift + [ssCtrl];
  if mkAlt in Event.ModifiersDown then
    Shift := Shift + [ssAlt];

  CastleControl1MouseDown(Self, Event.MouseButton, Shift, Round(Event.Position[0]),
    Round(CastleControl1.Height - Event.Position[1]));
end;

procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  //WriteINIFile;
end;

procedure TForm1.CheckBox5Change(Sender: TObject);
begin
  Edit6.Enabled := CheckBox5.Checked;
  Edit1.Enabled := not CheckBox5.Checked;
  Edit4Change(nil);
end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin
  Create_language_interface;
  WriteINIFile;
end;

procedure TForm1.Edit4Change(Sender: TObject);
var
  BotsQuantity, hpQuantity, Playerhp, PlayerQuantity, i: Integer;
  Difficulty, mapSizex, mapSizey: Integer;
begin
  val(Edit5.Text, Playersn, i);
  if Playersn < 1 then
    Playersn := 1;
  PlayerQuantity := Playersn;
  val(Edit4.Text, Playerhp, i);
  val(Edit1.Text, BotsQuantity, i);
  val(Edit2.Text, mapSizex, i);
  val(Edit7.Text, mapSizey, i);
  val(Edit3.Text, hpQuantity, i);
  if hpQuantity <= 0 then
    hpQuantity := 1;

  val(Edit6.Text, Difficulty, i); //non-optimal bugfix
  if CheckBox5.Checked then
    BotsQuantity := (Difficulty div 25) * Playersn * Playerhp div hpQuantity;

  Label8.Visible := true;
  Label8.Caption := Txt[51];
  Label8.Color := $AAFFAA;
  if hpQuantity * BotsQuantity > PlayerQuantity * 20 * 10 + BotsQuantity * 10 * 10 then
  begin
    Label8.Caption := Txt[52];
    Label8.Color := $AAAAFF;
  end;
  if Playerhp * PlayerQuantity > BotsQuantity * 20 * 10 then
  begin
    Label8.Caption := Txt[53];
    Label8.Color := $0000FF;
  end;
  if BotsQuantity + PlayerQuantity > mapSizex * mapSizey * 0.2 then
  begin
    Label8.Caption := Txt[54];
    Label8.Color := $0000FF;
  end;

  if (mapSizex > 2) and (mapSizey > 2) and (Playerhp > 0) and
    (PlayerQuantity > 0) and (hpQuantity > 0) and (BotsQuantity > 0) then
  begin
    if CheckBox5.Checked then
      val(Edit6.Text, Difficulty, i)
    else
    begin
      if hpQuantity < 15 then
        hpQuantity := 15;
      Difficulty := calculate_Difficulty(PlayerQuantity, Playerhp *
        PlayerQuantity, BotsQuantity, hpQuantity * BotsQuantity,
        mapSizex, mapSizey, mapSizex * mapSizey div 2);

      if CheckBox1.Checked then
        Difficulty := Difficulty * DefenseDifficulty;
    end;
    Label10.Caption := Txt[26] + ': ' + sayDifficulty(Difficulty);
  end
  else
    Label10.Caption := Txt[56];
  Label10.Visible := true;
end;


{--------------------------------------------------------------------------------------}

procedure TForm1.FormDestroy(Sender: TObject);
var
  i, ix, iy: Integer;
begin
  Dispose(map);
  Dispose(MapStatus);
  Dispose(vis);
  Dispose(Movement);
  Dispose(distance);
  Dispose(MapChanged);
  Dispose(LOS_base);
  Dispose(botvis);
  Dispose(Explosion_area);

  FreeAndNil(txt_out);
  //todo: destroy Images!!!
  if SelectedImage <> nil then
    FreeAndNil(SelectedImage);
  if img_EnemySelected <> nil then
    FreeAndNil(img_EnemySelected);
  for i := 1 to 10 do
    if ExplosionImage[i] <> nil then
      FreeAndNil(ExplosionImage[i]);
  for i := 1 to MaxWall do
    if GLI1[i] <> nil then
      FreeAndNil(GLI1[i]);
  for i := 1 to MaxPass do
    if GLI2[i] <> nil then
      FreeAndNil(GLI2[i]);
  if ItemImage <> nil then
    FreeAndNil(ItemImage);
  for ix := 0 to BotTypes do
    for iy := 0 to MaxAngle do
      if BotImage[ix, iy] = nil then
        FreeAndNil(BotImage[ix, iy]);
  for ix := 1 to maxCracks do
    if CracksImage[ix] <> nil then
      FreeAndNil(CracksImage[ix]);
  if TextFrame <> nil then
    FreeAndNil(TextFrame);
  if MinimapRectangle = nil then
    FreeAndNil(MinimapRectangle);
  FreeAndNil(DamageFont);
  FreeAndNil(InfoFont);
  FreeAndNil(MoveFont);
end;

{================================================================================}
{================================================================================}
{================================================================================}


procedure TForm1.CastleControl1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MouseX, MouseY: Integer;
  i: Integer;
  found: Integer;
begin
  if GameMode > 200 then
  begin
    DoHighlight := false;
    MouseX := Round(x / (CastleControl1.Width / ViewSizeX) + 0.5) + ViewX;
    MouseY := Round(y / (CastleControl1.Height / ViewSizeY) + 0.5) + ViewY;
    if (Button = mbmiddle) then
    begin
      CenterMap(MouseX, MouseY);
    end
    else
    begin
      if (MouseX > 0) and (MouseY > 0) and (MouseX <= MaxX) and (MouseY <= MaxY) then
        if (Map^[MouseX, MouseY] < MapWall) and (Vis^[MouseX, MouseY] > 0) then
        begin
          MapChanged^[MouseX, MouseY] := 255;
          found := -1;
          for i := 1 to NBot do
            if (Bot[i].X = MouseX) and (Bot[i].Y = MouseY) and
              (Vis^[MouseX, MouseY] > OldVisible) and (Bot[i].HP > 0) then
              found := i;
          if found > 0 then
          begin
            {clicked a bot}
            if (Button = mbLeft) and (Bot[found].Owner = Player) then
            begin
              //if (Selectedx>0) then DrawMapall:=true; ///////// OPTIMIZE HERE
              if (Selected > 0) then
                MapChanged^[Bot[Selected].X, Bot[Selected].Y] := 255;
              if (SelectedEnemy > 0) then
                MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
              ScrollBar1.Position := 0;
              ScrollBar2.Position := 0;
              Selected := found;
              MapChanged^[Bot[Selected].X, Bot[Selected].Y] := 255;
              Selectedx := -1;
              Selectedy := -1;
              SelectedEnemy := -1;
              SelectedItem := -1;
              SelectedOnFloor := -1;
            end;
            if (Button = mbright) and (Bot[found].Owner = Computer) and
              (Selected > 0) then
            begin
              if (SelectedEnemy <> found) and (CheckBox2.Checked) then
              begin
                if Vis^[MouseX, MouseY] > OldVisible then
                begin
                  //if (Selectedx>0) then DrawMapall:=true;   ///////// OPTIMIZE HERE
                  if (SelectedEnemy > 0) then
                    MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
                  SelectedEnemy := found;
                  MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
                  DoHighlight := true;
                  HighlightX := Bot[SelectedEnemy].X;
                  HighlightY := Bot[SelectedEnemy].Y;
                  Selectedx := -1;
                  Selectedy := -1;
                end;
              end
              else
              begin
                {fire}
                if CheckBox2.Checked = false then
                begin
                  if SelectedEnemy > 0 then
                    MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
                  SelectedEnemy := found;
                end;
                Selectedx := -1;
                Selectedy := -1;
                if checkLOS(Bot[Selected].X, Bot[Selected].Y,
                  Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y, true) > 0 then
                  BotShots(Selected, SelectedEnemy)
                else if SelectedEnemy > 0 then
                begin
                  DoHighlight := true;
                  HighlightX := Bot[SelectedEnemy].X;
                  HighlightY := Bot[SelectedEnemy].Y;
                end;
              end;
            end;
          end
          else
          begin
            if (Button = mbleft) and (Selected > 0) then
            begin

              if ((Selectedx <> MouseX) or (Selectedy <> MouseY)) and
                (CheckBox2.Checked) then
              begin
                //if (Selectedx>0) then DrawMapall:=true;   ///////// OPTIMIZE HERE                                         Selectedx:=MouseX; Selectedy:=MouseY;
                if (SelectedEnemy > 0) then
                  MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
                SelectedEnemy := -1;
                Selectedx := MouseX;
                Selectedy := MouseY;
                GenerateMovement(Selected, Selectedx, Selectedy);
              end
              else
              begin
                {Move here}
                //if (Selectedx>0) then DrawMapall:=true;   ///////// OPTIMIZE HERE
                if (SelectedEnemy > 0) then
                  MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
                SelectedEnemy := -1;
                Selectedx := MouseX;
                Selectedy := MouseY;
                Move_bot(Selected, Selectedx, Selectedy, 100);
              end;
            end;
          end;
        end;
      DrawMap;
    end;
  end
  else if (GameMode = GameMode_ItemInfo) then
  begin
    GameMode := PreviousGameMode;
    DrawMapall := true;
    //if (PreviousGameMode>100) and (MapGenerated) then DrawMap;
  end;
end;

{================================================================================}
{================================================================================}
{================================================================================}

procedure TForm1.CastleControl1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  MouseX, MouseY: Integer;
  i: Integer;
  found: Boolean;
begin
  if GameMode > 200 then
  begin
    MouseX := Round(x / (CastleControl1.Width / ViewSizeX) + 0.5) + ViewX;
    MouseY := Round(y / (CastleControl1.Height / ViewSizeY) + 0.5) + ViewY;
    if (MouseX > 0) and (MouseY > 0) and (MouseX <= MaxX) and (MouseY <= MaxY) then
      if Vis^[MouseX, MouseY] = 0 then
        CastleControl1.Cursor := crHelp
      else
      begin
        if Map^[MouseX, MouseY] >= MapWall then
          CastleControl1.cursor := CrNo
        else
        begin
          if Map^[MouseX, MouseY] < MapWall then
          begin
            found := true;
            for i := 1 to NBot do
              if (Bot[i].X = MouseX) and (Bot[i].Y = MouseY) and (Bot[i].HP > 0) then
              begin
                found := false;
                if Bot[i].Owner = Player then
                  CastleControl1.cursor := CrHandPoint
                else
                begin
                  if Vis^[MouseX, MouseY] > OldVisible then
                    CastleControl1.cursor := crCross
                  else
                    found := true;
                end;
              end;
            if found then
              CastleControl1.cursor := crdefault;
          end;
        end;
      end;
  end
  else
    CastleControl1.cursor := crdefault;
end;



{================================================================================}
{================================================================================}
{================================================================================}

procedure TForm1.Image2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbleft then
  begin
    if (Selected > 0) and (SelectedItem > 1) then
      if (Bot[Selected].Items[SelectedItem].N > 0) and
        (Bot[Selected].Items[SelectedItem].Ammo_ID > 0) and
        (Bot[Selected].Items[SelectedItem].W_ID = 0) and
        (Bot[Selected].Items[1].W_ID > 0) and (Bot[Selected].Items[1].Ammo_ID = 0) then
        if LoadWeapon(Selected, SelectedItem) then
        begin
          SelectedOnFloor := -1;
          SelectedItem := -1;
          if GameMode = GameMode_ItemInfo then
          begin
            DrawMapall := true;
            GameMode := PreviousGameMode;
            DrawMap;
          end
          else
            Draw_Stats;
        end;
  end
  else {Button=mbright}
  begin
    if Selected > 0 then
      DisplayItemInfo(Bot[Selected].Items[1]);
  end;
end;


function TForm1.LoadWeapon(Thisbot, ThisItem: Integer): Boolean;
var
  tmpItem: ItemType;
  i: Integer;
  Flg, Loadw: Boolean;
begin
  Loadw := false;
  if Bot[Thisbot].Items[ThisItem].W_ID > 0 then
  begin
    if SpendTU(Thisbot, ChangeWeaponTime) then
    begin
      tmpItem := Bot[Thisbot].Items[1];
      Bot[Thisbot].Items[1] := Bot[Thisbot].Items[ThisItem];
      Bot[Thisbot].Items[ThisItem] := tmpItem;
      Bot[Thisbot].Items[1].RechargeState :=
        WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].Recharge;
      if Bot[Thisbot].Owner = Player then
        SoundEngine.PlaySound(Reload_sound, false, false, 0, 1, 0, 1, TVector3.Zero);
      Loadw := true;
    end;
  end
  else
  if (Bot[Thisbot].Items[1].Ammo_ID = 0) and
    (Bot[Thisbot].Items[ThisItem].Ammo_ID > 0) and
    (Bot[Thisbot].Items[ThisItem].W_ID = 0) and (Bot[Thisbot].Items[1].W_ID > 0) then
  begin
    Flg := false;
    for i := 1 to maxUsableAmmo do
      if WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].Ammo[i] =
        Bot[Thisbot].Items[ThisItem].Ammo_ID then
        Flg := true;
    if Flg then
    begin
      if SpendTU(Thisbot, WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].Reload) then
      begin
        Bot[Thisbot].Items[1].Ammo_ID := Bot[Thisbot].Items[ThisItem].Ammo_ID;
        Bot[Thisbot].Items[ThisItem].Ammo_ID := 0;
        Bot[Thisbot].Items[1].N := Bot[Thisbot].Items[ThisItem].N;
        Bot[Thisbot].Items[1].RechargeState :=
          WeaponSpecifications[Bot[Thisbot].Items[1].W_ID].Recharge;
        if Bot[Thisbot].Owner = Player then
          SoundEngine.PlaySound(clip_sound, false, false, 0, 1, 0, 1, TVector3.Zero);
        Loadw := true;
      end;
    end
    else
      ShowMessage(Txt[57]);
  end;
  LoadWeapon := Loadw;
end;

{--------------------------------------------------------------------------}

const
  FontSize = 13;
  Font7Size = 10;

procedure TForm1.Image3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  tmpItem: ItemType;
  SelectedNew, Freespace, i: Integer;
begin
  if (y >= 0) and (y <= (BackpackSize * 2 - 1) * FontSize) and (Selected > 0) then
  begin
    SelectedNew := Round((y - FontSize - 3 + ScrollBar1.Position) / (2 * FontSize)) + 2;
    if SelectedNew < 2 then
      SelectedNew := 2;
    if SelectedNew > BackpackSize then
      SelectedNew := BackpackSize;
    if Button = mbleft then
    begin
      if (ssShift in Shift) or ((ssCtrl in Shift) and
        (Bot[Selected].Items[SelectedNew].W_ID = 0) and
        (Bot[Selected].Items[SelectedNew].Ammo_ID = 0)) then
      begin
        if (Bot[Selected].Items[SelectedNew].Ammo_ID > 0) or
          (Bot[Selected].Items[SelectedNew].W_ID > 0) then
        begin
          Drop_Item(Selected, SelectedNew);
        end
        else
        if (Bot[Selected].Items[1].Ammo_ID > 0) and
          (Bot[Selected].Items[SelectedNew].W_ID = 0) and
          (Bot[Selected].Items[SelectedNew].Ammo_ID = 0) then
          if SpendTU(Selected, WeaponSpecifications[
            Bot[Selected].Items[1].W_ID].Reload) then
          begin
            Bot[Selected].Items[SelectedNew].Ammo_ID := Bot[Selected].Items[1].Ammo_ID;
            Bot[Selected].Items[SelectedNew].N := Bot[Selected].Items[1].N;
            Bot[Selected].Items[1].Ammo_ID := 0;
            SoundEngine.PlaySound(unclip_sound, false, false, 0, 1, 0,
              1, TVector3.Zero);
          end;
      end
      else if ssCtrl in Shift then
      begin
        if (Bot[Selected].Items[SelectedNew].Ammo_ID > 0) and
          (Bot[Selected].Items[SelectedNew].W_ID > 0) then
        begin
          //check place
          Freespace := 0;
          for i := BackpackSize downto 2 do
            if (Bot[Selected].Items[i].Ammo_ID = 0) and
              (Bot[Selected].Items[i].W_ID = 0) then
              Freespace := i;
          if Freespace >= 2 then
          begin
            if SpendTU(Selected, WeaponSpecifications[
              Bot[Selected].Items[SelectedNew].W_ID].Reload) then
            begin
              Bot[Selected].Items[Freespace].Ammo_ID :=
                Bot[Selected].Items[SelectedNew].Ammo_ID;
              Bot[Selected].Items[Freespace].N := Bot[Selected].Items[SelectedNew].N;
              Bot[Selected].Items[SelectedNew].Ammo_ID := 0;
              SoundEngine.PlaySound(unLoad_sound, false, false, 0, 1,
                0, 1, TVector3.Zero);
            end;
          end
          else
            ShowMessage(Txt[58]);

        end;
      end
      else if SelectedItem = SelectedNew then
      begin
        if LoadWeapon(Selected, SelectedItem) then
        begin
          SelectedOnFloor := -1;
          SelectedItem := -1;
        end;
      end
      else
      begin
        SelectedOnFloor := -1;
        if (Bot[Selected].Items[SelectedNew].Ammo_ID > 0) or
          (Bot[Selected].Items[SelectedNew].W_ID > 0) then
          SelectedItem := SelectedNew
        else if SelectedItem > 0 then
        begin
          tmpItem := Bot[Selected].Items[SelectedNew];
          Bot[Selected].Items[SelectedNew] := Bot[Selected].Items[SelectedItem];
          Bot[Selected].Items[SelectedItem] := tmpItem;
          SelectedItem := -1;
        end;
      end;
      if GameMode = GameMode_ItemInfo then
      begin
        DrawMapall := true;
        GameMode := PreviousGameMode;
        DrawMap;
      end
      else
        Draw_Stats;
      //DrawMap;
    end
    else {Button=mbright}
    begin
      DisplayItemInfo(Bot[Selected].Items[SelectedNew]);
    end;
  end;
end;

procedure TForm1.Image3MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  ChangeRate: Integer;
begin
  if ScrollBar1.Enabled then
  begin
    ChangeRate := -WheelDelta div 3;
    if ScrollBar1.Position + ChangeRate < 0 then
      ScrollBar1.Position := 0
    else
    if ScrollBar1.Position + ChangeRate > ScrollBar1.Max then
      ScrollBar1.Position := ScrollBar1.Max
    else
      ScrollBar1.Position := ScrollBar1.Position + ChangeRate;
  end;
end;

{------------------------------------------------------------------------------------}

procedure TForm1.Drop_Item(Thisbot, ThisItem: Integer);
begin
  if (Bot[Thisbot].Items[ThisItem].W_ID > 0) or
    (Bot[Thisbot].Items[ThisItem].Ammo_ID > 0) then
    if SpendTU(Thisbot, DropItemTime) then
    begin
      if Bot[Thisbot].Owner = Player then
        SoundEngine.PlaySound(Drop_sound, false, false, 0, 1, 0, 1, TVector3.Zero);

      if Itemsn = MaxItems then
        ShowMessage('TOO MANY ItemS!!!')
      else
        Inc(Itemsn);
      Item[Itemsn].Item := Bot[Thisbot].Items[ThisItem];
      Item[Itemsn].X := Bot[Thisbot].X;
      Item[Itemsn].Y := Bot[Thisbot].Y;
      Bot[Thisbot].Items[ThisItem].Ammo_ID := 0;
      Bot[Thisbot].Items[ThisItem].W_ID := 0;
      SelectedOnFloor := -1;
      SelectedItem := -1;
    end;

end;

{-------------------------------------------------------------------------------------}

procedure TForm1.pick_up(Thisbot, ThisItem: Integer);
var
  i: Integer;
  Flg: Boolean;
begin
  if OnFloorn > 0 then
  begin
    i := 1;
    Flg := false;
    repeat
      Inc(i);
      if (Bot[Thisbot].Items[i].W_ID = 0) and (Bot[Thisbot].Items[i].Ammo_ID = 0) then
        Flg := true;
    until (i >= BackpackSize) or (Flg);
    if Flg and SpendTU(Thisbot, PickUpItemTime) then
    begin
      if Bot[Thisbot].Owner = Player then
        SoundEngine.PlaySound(Item_sound, false, false, 0, 1, 0, 1, TVector3.Zero);
      Bot[Thisbot].Items[i] := Item[OnFloor[ThisItem]].Item;
      for i := OnFloor[ThisItem] to Itemsn - 1 do
        Item[i] := Item[i + 1];
      Dec(Itemsn);
    end;
    if (not Flg) and (Bot[Thisbot].Owner = Player) then
      ShowMessage(Txt[58]);
  end;

end;

procedure TForm1.Image4MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  SelectedNew: Integer;
begin
  if (OnFloorn > 0) and (Selected > 0) then
  begin
    SelectedNew := Round((y - Font7Size - 3 + ScrollBar2.Position) /
      (2 * Font7Size)) + 1;
    if SelectedNew < 1 then
      SelectedNew := 1;
    if SelectedNew > OnFloorn then
      SelectedNew := OnFloorn;
    if Button = mbleft then
    begin
      if (ssCtrl in Shift) and (Item[OnFloor[SelectedNew]].Item.W_ID > 0) and
        (Item[OnFloor[SelectedNew]].Item.Ammo_ID > 0) then
      begin
        if SpendTU(Selected, WeaponSpecifications[Item[OnFloor[SelectedNew]].Item.W_ID].Reload + (PickUpItemTime + DropItemTime) div 2) then
        begin
          Inc(Itemsn);
          Item[Itemsn] := Item[OnFloor[SelectedNew]];
          Item[OnFloor[SelectedNew]].Item.N := 0;
          Item[OnFloor[SelectedNew]].Item.Ammo_ID := 0;
          Item[Itemsn].Item.W_ID := 0;
          SelectedOnFloor := SelectedNew;
          SelectedItem := -1;
          SoundEngine.PlaySound(unLoad_sound, false, false, 0, 1, 0,
            1, TVector3.Zero);
        end;
      end
      else
      if SelectedNew = SelectedOnFloor then
      begin
        pick_up(Selected, SelectedOnFloor);
        SelectedOnFloor := -1;
      end
      else
      begin
        SelectedOnFloor := SelectedNew;
        SelectedItem := -1;
      end;
      if GameMode = GameMode_ItemInfo then
      begin
        DrawMapall := true;
        GameMode := PreviousGameMode;
        DrawMap;
      end
      else
        Draw_Stats;
      //DrawMap;
    end
    else {Button=mbright}
    begin
      DisplayItemInfo(Item[OnFloor[SelectedNew]].Item);
    end;
  end;
end;

procedure TForm1.Image5MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  SelectedNew: Integer;
begin
  if (PlayerUnitsN > 0) then
  begin
    if Selected > 0 then
      MapChanged^[Bot[Selected].X, Bot[Selected].Y] := 255;
    SelectedNew := Round((y - Font7Size - 3 + ScrollBar3.Position) /
      (Font7Size + 12)) + 1;
    if SelectedNew < 1 then
      SelectedNew := 1;
    if SelectedNew > PlayerUnitsN then
      SelectedNew := PlayerUnitsN;
    if PlayerUnits[SelectedNew] <> Selected then
    begin
      ScrollBar1.Position := 0;
      ScrollBar2.Position := 0;
      if (SelectedEnemy > 0) then
        MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
      Selectedx := -1;
      Selectedy := -1;
      SelectedEnemy := -1;
      SelectedItem := -1;
      SelectedOnFloor := -1;
    end;
    Selected := PlayerUnits[SelectedNew];
    if (Bot[Selected].X <= ViewX) or (Bot[Selected].Y <= ViewY) or
      (Bot[Selected].X > ViewX + ViewSizeY) or (Bot[Selected].Y > ViewY + ViewSizeY) then
      CenterMap(Bot[Selected].X, Bot[Selected].Y);
    MapChanged^[Bot[Selected].X, Bot[Selected].Y] := 255;
    if GameMode = GameMode_ItemInfo then
    begin
      DrawMapall := true;
      GameMode := PreviousGameMode;
    end;
    DrawMap;
  end;
end;


procedure TForm1.Image6MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  SelectedNew: Integer;
begin
  if (EnemyUnitsn > 0) then
  begin
    SelectedNew := Round((y - Font7Size - 3 + ScrollBar4.Position) /
      (Font7Size + 10)) + 1;
    if SelectedNew < 1 then
      SelectedNew := 1;
    if SelectedNew > EnemyUnitsn then
      SelectedNew := EnemyUnitsn;
    if SelectedEnemy <> EnemyUnits[SelectedNew] then
    begin
      if (SelectedEnemy > 0) then
        MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
    end;
    SelectedEnemy := EnemyUnits[SelectedNew];
    if (Bot[SelectedEnemy].X <= ViewX) or (Bot[SelectedEnemy].Y <= ViewY) or
      (Bot[SelectedEnemy].X > ViewX + ViewSizeY) or
      (Bot[SelectedEnemy].Y > ViewY + ViewSizeY) then
    begin
      if (Selected > 0) then
      begin
        if (Abs(Bot[Selected].X - Bot[SelectedEnemy].X) < ViewSizeX) and
          (Abs(Bot[Selected].Y - Bot[SelectedEnemy].Y) < ViewSizeY) then
          CenterMap((Bot[Selected].X + Bot[SelectedEnemy].X) div 2,
            (Bot[Selected].Y + Bot[SelectedEnemy].Y) div 2)
        else
          CenterMap(Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y);
      end
      else
        CenterMap(Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y);
    end;
    MapChanged^[Bot[SelectedEnemy].X, Bot[SelectedEnemy].Y] := 255;
    if GameMode = GameMode_ItemInfo then
    begin
      DrawMapall := true;
      GameMode := PreviousGameMode;
    end;
    DrawMap;
  end;
end;

procedure TForm1.Image4MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  ChangeRate: Integer;
begin
  if ScrollBar2.Enabled then
  begin
    ChangeRate := -WheelDelta div 3;
    if ScrollBar2.Position + ChangeRate < 0 then
      ScrollBar2.Position := 0
    else
    if ScrollBar2.Position + ChangeRate > ScrollBar2.Max then
      ScrollBar2.Position := ScrollBar2.Max
    else
      ScrollBar2.Position := ScrollBar2.Position + ChangeRate;
  end;
end;

procedure TForm1.Image5MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  ChangeRate: Integer;
begin
  if ScrollBar3.Enabled then
  begin
    ChangeRate := -WheelDelta div 3;
    if ScrollBar3.Position + ChangeRate < 0 then
      ScrollBar3.Position := 0
    else
    if ScrollBar3.Position + ChangeRate > ScrollBar3.Max then
      ScrollBar3.Position := ScrollBar3.Max
    else
      ScrollBar3.Position := ScrollBar3.Position + ChangeRate;
  end;
end;


procedure TForm1.Image6MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  ChangeRate: Integer;
begin
  if ScrollBar4.Enabled then
  begin
    ChangeRate := -WheelDelta div 3;
    if ScrollBar4.Position + ChangeRate < 0 then
      ScrollBar4.Position := 0
    else
    if ScrollBar4.Position + ChangeRate > ScrollBar4.Max then
      ScrollBar4.Position := ScrollBar4.Max
    else
      ScrollBar4.Position := ScrollBar4.Position + ChangeRate;
  end;
end;

procedure TForm1.CenterMap(ToX, ToY: Integer);
var
  mx, my: Integer;
  NewViewX, NewViewY: Integer;
begin

  NewViewX := ToX - ViewSizeX div 2;
  NewViewY := ToY - ViewSizeY div 2;
  if NewViewX < 0 then
    NewViewX := 0;
  if NewViewY < 0 then
    NewViewY := 0;
  if NewViewX > MaxX - ViewSizeX then
    NewViewX := MaxX - ViewSizeX;
  if NewViewY > MaxY - ViewSizeY then
    NewViewY := MaxY - ViewSizeY;

  if (NewViewX <> ViewX) or (NewViewY <> ViewY) then
  begin
    for mx := ViewX + 1 to ViewX + ViewSizeX do
    begin
      MapChanged^[mx, ViewY + 1] := 255;
      MapChanged^[mx, ViewY + ViewSizeY] := 255;
    end;
    for my := ViewY + 1 to ViewY + ViewSizeY do
    begin
      MapChanged^[ViewX + 1, my] := 255;
      MapChanged^[ViewX + ViewSizeX, my] := 255;
    end;
    ViewX := NewViewX;
    ViewY := NewViewY;
    DrawMapall := true;
    DrawMap;
  end;
end;

procedure TForm1.Image7MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
//var MouseX,MouseY:Integer;
begin
  CenterMap(Round(x / (CastleControl2.Width / MaxX) + 0.5),
    Round(y / (CastleControl2.Height / MaxY) + 0.5));
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  NextMusic: Integer;
begin
  repeat
    NextMusic := Trunc(Random * MaxMusic) + 1;
    if NextMusic = 0 then
      NextMusic := 1;
    if NextMusic > MaxMusic then
      NextMusic := MaxMusic;
  until NextMusic <> oldMusic;
  case NextMusic of
    1: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Socapex - Dark Ambiance - Mastered.ogg', MusicDuration);
    2: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'gloom.ogg', MusicDuration);
    3: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'dark_caves.ogg', MusicDuration);
    4: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Caves of sorrow.ogg', MusicDuration);
    5: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'ambientmain_0.ogg', MusicDuration);
    6: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Alert_0.ogg', MusicDuration);
    7: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Dirt_City_0.ogg', MusicDuration);
    8: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Forgoten_tombs_1.ogg', MusicDuration);
    9: Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Grey_land.ogg', MusicDuration);
    10: Music := soundengine.Loadbuffer(DataFolder + 'music' +
        PathDelim + 'Myst.ogg', MusicDuration);
    11: Music := soundengine.Loadbuffer(DataFolder + 'music' +
        PathDelim + 'Road.ogg', MusicDuration);
    12: Music := soundengine.Loadbuffer(DataFolder + 'music' +
        PathDelim + 'Trance_0.ogg', MusicDuration);
    13: Music := soundengine.Loadbuffer(DataFolder + 'music' +
        PathDelim + 'Vault_0.ogg', MusicDuration);
    else
      Music := soundengine.Loadbuffer(DataFolder + 'music' + PathDelim +
        'Wind_0.ogg', MusicDuration);
  end;

  SoundEngine.PlaySound(Music, false, false, 0, 1, 0, 1, TVector3.Zero);
  Timer1.Interval := Round(MusicDuration * 1000) + 200;
  oldMusic := NextMusic;
end;

procedure TForm1.set_ProgressBar(Flg: Boolean);
begin
  ProgressBar1.Visible := Flg;
  ProgressBar1.Position := 1;
  ProgressBar1.Min := 1;
  // if Button1.Visible then
  if Flg then
  begin
    Button1.Height := 60;
    Button1.Enabled := false;
  end
  else
  begin
    Button1.Enabled := true;
    Button1.Height := 72;
  end;
end;

procedure TForm1.SetControlsGame(Flg: Boolean);
begin
  CastleControl1.Visible := Flg;
  Image2.Visible := Flg;
  Image3.Visible := Flg;
  Image4.Visible := Flg;
  Image5.Visible := Flg;
  Image6.Visible := Flg;
  Memo1.Visible := Flg;
  Button1.Visible := Flg;
  ScrollBar1.Visible := Flg;
  ScrollBar2.Visible := Flg;
  ScrollBar3.Visible := Flg;
  ScrollBar4.Visible := Flg;
  if DebugMode then
    Label1.Visible := Flg
  else
    Label1.Visible := false;
  CheckBox2.Visible := Flg;
  CastleControl2.Visible := Flg;
  // if GameMode>200 then Button1.enabled:=true else Button1.enabled:=false;
end;

procedure TForm1.SetControlsMenu(Flg: Boolean);
begin
  RadioButton1.Visible := Flg;
  RadioButton2.Visible := Flg;
  RadioButton3.Visible := Flg;
  ComboBox1.Visible := Flg;
  Button2.Visible := Flg;
  Button3.Visible := Flg;
  Label2.Visible := Flg;
  Label3.Visible := Flg;
  Button4.Visible := Flg;
  Edit1.Visible := Flg;
  Label4.Visible := Flg;
  // Button5.Visible:=Flg;
  Button6.Visible := Flg;
  Label5.Visible := Flg;
  Edit2.Visible := Flg;
  Edit7.Visible := Flg;
  Label15.Visible := Flg;
  Label6.Visible := Flg;
  Edit3.Visible := Flg;
  Label7.Visible := Flg;
  Edit4.Visible := Flg;
  Label8.Visible := Flg;
  Label10.Visible := Flg;
  Edit5.Visible := Flg;
  Label11.Visible := Flg;
  Label12.Visible := Flg;
  CheckBox1.Visible := Flg;
  CheckBox3.Visible := Flg;
  // Button7.Visible:=Flg;
  Memo2.Visible := Flg;
  CheckBox4.Visible := Flg;
  Label14.Visible := Flg;
  Edit6.Visible := Flg;
  CheckBox5.Visible := Flg;
  CheckBox6.Visible := Flg;

  Label16.Visible := Flg;
  Label17.Visible := Flg;
  Label18.Visible := Flg;
  Label19.Visible := Flg;
  Label20.Visible := Flg;
  CheckBox7.Visible := Flg;
  CheckBox8.Visible := Flg;
  TrackBar2.Visible := Flg;
  TrackBar3.Visible := Flg;

  Label21.Visible := Flg;
  ComboBox2.Visible := Flg;

  if MapGenerated then
  begin
    if GameMode > 200 then
      Button4.Enabled := true
    else
    begin
      Button4.Enabled := false;
      Button4.Visible := false;
    end;
    if GameMode > 200 then
      Button3.Enabled := true
    else
    begin
      Button3.Enabled := false;
      Button3.Visible := false;
    end;
    ToggleBox1.Enabled := true;
    ToggleBox1.Visible := true;
  end
  else
  begin
    Button4.Enabled := false;
    Button4.Visible := false;
    Button3.Enabled := false;
    Button3.Visible := false;
    ToggleBox1.Enabled := false;
    ToggleBox1.Visible := false;
  end;

end;

procedure TForm1.ToggleBox1Change(Sender: TObject);
begin
  if GameMode = GameMode_ItemInfo then
  begin
    GameMode := PreviousGameMode;
    DrawMapall := true;
    if (PreviousGameMode > 100) and (MapGenerated) then
      DrawMap;
  end;
  if ToggleBox1.Checked then
  begin
    SetControlsMenu(true);
    SetControlsGame(false);
  end
  else
  begin
    SetControlsMenu(false);
    SetControlsGame(true);
  end;
end;

procedure TForm1.ScrollBar1Change(Sender: TObject);
begin
  Draw_Stats;
end;

procedure TForm1.ScrollBar2Change(Sender: TObject);
begin
  Draw_Stats;
end;

procedure TForm1.ScrollBar3Change(Sender: TObject);
begin
  DrawMap;
end;

procedure TForm1.ScrollBar4Change(Sender: TObject);
begin
  DrawMap;
end;

{--------------------------------------------------------------------------------------}
procedure TForm1.DisplayItemInfo(ThisItem: ItemType);
var {starty,startx:Integer;
    descr:string;
    Line_width:Integer;}
  tmp: Integer;
begin
  if (ThisItem.W_ID > 0) or (ThisItem.Ammo_ID > 0) then
  begin
    Info_Text := '';
    if GameMode <> GameMode_ItemInfo then
    begin
      DoAnimate_Info := true;
      PreviousGameMode := GameMode;
    end;
    GameMode := GameMode_ItemInfo;
    {Weapon Info}
    if ThisItem.W_ID > 0 then
    begin
      Info_Text += WeaponSpecifications[ThisItem.W_ID].Name + sLineBreak;
      Info_Text += Txt[59] + ': ' + IntToStr(ThisItem.State) + '/' +
        IntToStr(ThisItem.MaxState) + '(' +
        IntToStr(WeaponSpecifications[ThisItem.W_ID].MaxState) + ')      = ' +
        IntToStr(Round(100 * ThisItem.State / ThisItem.MaxState)) + '%' + sLineBreak;
      Info_Text += Txt[60] + ': +' + IntToStr(WeaponSpecifications[ThisItem.W_ID].Dam) +
        '         ' + Txt[63] + ': +' +
        IntToStr(WeaponSpecifications[ThisItem.W_ID].Acc) + sLineBreak;
      Info_Text += Txt[61] + ': ' + IntToStr(WeaponSpecifications[ThisItem.W_ID].aim) +
        sLineBreak;
      Info_Text += Txt[62] + ': ' +
        IntToStr(WeaponSpecifications[ThisItem.W_ID].Recharge) + sLineBreak;
      Info_Text += Txt[64] + ': ' +
        IntToStr(WeaponSpecifications[ThisItem.W_ID].Reload) + sLineBreak;
      Info_Text += WeaponSpecifications[ThisItem.W_ID].Description + sLineBreak;
      Info_Text += sLineBreak;
    end;
    if ThisItem.Ammo_ID > 0 then
    begin
      {Ammo Stats}
      Info_Text += AmmoSpecifications[ThisItem.Ammo_ID].Name + sLineBreak;
      Info_Text += Txt[65] + ': ' + IntToStr(ThisItem.N) + '/' +
        IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Quantity) + sLineBreak;
      Info_Text += Txt[60] + ': +' +
        IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Dam) + '         ' +
        Txt[63] + ': +' + IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Acc) + sLineBreak;
      Info_Text += Txt[66] + ': ' +
        IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Explosion) + ' / ' +
        Txt[67] + ': ' + IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Area) +
        ' / ' + Txt[68] + ': ' + IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Smoke) +
        sLineBreak;

      Info_Text += AmmoSpecifications[ThisItem.Ammo_ID].Description + sLineBreak;

      if ThisItem.W_ID > 0 then
      begin
        Info_Text += sLineBreak;
        {summary Stats}
        if ThisItem.State > ThisItem.MaxState div 3 then
          tmp := ThisItem.MaxState div 3
        else
          tmp := ThisItem.State;
        Info_Text += Txt[60] + ': ' +
          IntToStr(Round((AmmoSpecifications[ThisItem.Ammo_ID].Dam +
          WeaponSpecifications[ThisItem.W_ID].Dam) * tmp / (ThisItem.MaxState div 3))) +
          sLineBreak;
        Info_Text += Txt[63] + ': ' +
          IntToStr(AmmoSpecifications[ThisItem.Ammo_ID].Acc +
          WeaponSpecifications[ThisItem.W_ID].Acc) + sLineBreak;
        Info_Text += Txt[70] + ': ' +
          IntToStr(Trunc(255 / (WeaponSpecifications[ThisItem.W_ID].aim +
          WeaponSpecifications[ThisItem.W_ID].Recharge))) + sLineBreak;
        if ThisItem.State > ThisItem.MaxState div 3 then
          tmp := ThisItem.MaxState div 3
        else
          tmp := ThisItem.State;
        Info_Text += Txt[71] + ': ' +
          IntToStr(Round((AmmoSpecifications[ThisItem.Ammo_ID].Dam +
          WeaponSpecifications[ThisItem.W_ID].Dam) * tmp / (ThisItem.MaxState div 3)) *
          Trunc(255 / (WeaponSpecifications[ThisItem.W_ID].aim +
          WeaponSpecifications[ThisItem.W_ID].Recharge))) + sLineBreak;
        if AmmoSpecifications[ThisItem.Ammo_ID].Area > 0 then
        begin
          case AmmoSpecifications[ThisItem.Ammo_ID].Area of
            1 ..  9: tmp := 9;
            10 ..  25: tmp := 25;
            26 ..  49: tmp := 49;
            50 ..  81: tmp := 81;
            82 .. 121: tmp := 121;
            else
              tmp := AmmoSpecifications[ThisItem.Ammo_ID].Area;
          end;
          Info_Text += Txt[72] + ': ' + IntToStr(
            AmmoSpecifications[ThisItem.Ammo_ID].Explosion div tmp) + sLineBreak;
        end;
      end;

    end;

  end;
  if DoAnimate_Info then
  begin
    InfoAnimation := Now;
    repeat
      CastleControl1.Paint
    until (Now - InfoAnimation) * 20 * 60 * 60 * 1000 >= InfoDelay;
    DoAnimate_Info := false;
  end;
  CastleControl1.Paint;
end;

{-----------------------------------------------------------------------------------}

const
  ItemsY = 46;

procedure TForm1.Draw_Stats;
var
  i, n1, n2: Integer;
  yShift: Integer;
  s: string;
begin
  n1 := 0;
  n2 := 0;
  for i := 1 to NBot do
    if (Bot[i].Owner = Player) and (Bot[i].HP > 0) then
    begin
      if Bot[i].TU = 255 then
        Inc(n2);
      if Bot[i].TU > Bot[i].Speed * 1.5 then
        Inc(n1);
    end;
  s := Txt[73];
  {$IFDEF UNIX}
  if n2 > 0 then
    s := s + sLineBreak + IntToStr(n2) + ' ' + Txt[74];
  if n1 - n2 > 0 then
    s := s + sLineBreak + IntToStr(n1 - n2) + ' ' + Txt[75];
  {$ENDIF}
  Button1.Caption := s;

  with Image2.Canvas do
  begin
    Pen.Width := 1;
    Brush.Style := bsSolid;
    Brush.Color := 0;
    FillRect(0, 0, Image2.Width, Image2.Height);

    if Selected > 0 then
    begin
      Pen.Color := $777777;
      Brush.Color := $333344;
      Rectangle(3, ItemsY - 1, Image2.Width - 3, ItemsY + 4 * FontSize + 3);

      Brush.Style := bsClear;
      Font.Color := $FFFFFF;
      TextOut(5, 0, Bot[Selected].Name);

      Font.Size := 7;
      Pen.Color := $3333dd;
      Pen.Width := 10;
      MoveTo(5, 25);
      LineTo(Round((Image2.Width - 10) * Bot[Selected].HP /
        Bot[Selected].MaxHP) + 5, 25);
      TextOut(Image2.Width div 2 - 50, 20, IntToStr(Bot[Selected].HP) +
        '/' + IntToStr(Bot[Selected].MaxHP));

      Pen.Color := $33dd33;
      Pen.Width := 10;
      MoveTo(5, 37);
      //       if Bot[Selected].TU>MinimumTUUsable then
      LineTo(Round((Image2.Width - 10) * (Bot[Selected].TU{-MinimumTUUsable}) /
        (255{-MinimumTUUsable})) + 5, 37);
      TextOut(Image2.Width div 2 - 50, 32, IntToStr(Bot[Selected].TU));

      Font.Size := 10;
      if Bot[Selected].Items[1].W_ID > 0 then
      begin
        Font.Color := $FFFFFF;
        if Bot[Selected].Items[1].State < Bot[Selected].Items[1].MaxState div 2 then
          Font.Color := $99eeFF;
        if Bot[Selected].Items[1].State < Bot[Selected].Items[1].MaxState div 3 then
          Font.Color := $5555FF;
        if Bot[Selected].Items[1].State = 0 then
          Font.Color := $0000FF;
        TextOut(5, ItemsY, WeaponSpecifications[Bot[Selected].Items[1].W_ID].Name +
          ' ' + IntToStr(Bot[Selected].Items[1].State) + '/' + IntToStr(
          Bot[Selected].Items[1].MaxState) + '(' + IntToStr(
          Round(100 * Bot[Selected].Items[1].State /
          Bot[Selected].Items[1].MaxState)) + '%)');
      end;
      if (Bot[Selected].Items[1].Ammo_ID > 0) and (Bot[Selected].Items[1].N > 0) then
      begin
        if Bot[Selected].Items[1].State = 0 then
        begin
          Font.Color := $2222FF;
          TextOut(5, ItemsY + FontSize, Txt[76]);
        end
        else
        if Bot[Selected].Items[1].RechargeState > 0 then
        begin
          Font.Color := $2222FF;
          TextOut(5, ItemsY + FontSize, Txt[77] + ' ' + IntToStr(
            Round(100 * (1 - Bot[Selected].Items[1].RechargeState /
            WeaponSpecifications[Bot[Selected].Items[1].W_ID].Recharge))) + '%');
        end
        else
        begin
          Font.Color := $FFFFFF;
          TextOut(5, ItemsY + FontSize, Txt[60] + ': ' + IntToStr(
            WeaponSpecifications[Bot[Selected].Items[1].W_ID].Dam +
            AmmoSpecifications[Bot[Selected].Items[1].Ammo_ID].Dam));
          TextOut(70, ItemsY + FontSize, Txt[63] + ': ' + IntToStr(
            WeaponSpecifications[Bot[Selected].Items[1].W_ID].Acc +
            AmmoSpecifications[Bot[Selected].Items[1].Ammo_ID].Acc));
        end;
        Font.Color := $FFFFFF;
        TextOut(5, ItemsY + 3 * FontSize, Txt[78] + ': ' + IntToStr(
          WeaponSpecifications[Bot[Selected].Items[1].W_ID].AIM) +
          '+' + IntToStr(WeaponSpecifications[Bot[Selected].Items[1].W_ID].Recharge));
        TextOut(5, ItemsY + 2 * FontSize,
          AmmoSpecifications[Bot[Selected].Items[1].Ammo_ID].Name +
          ' ' + IntToStr(Bot[Selected].Items[1].N) + '/' + IntToStr(
          AmmoSpecifications[Bot[Selected].Items[1].Ammo_ID].Quantity));
      end
      else
      begin
        Font.Color := $0000FF;
        TextOut(5, ItemsY + FontSize, Txt[79]);
      end;
{        Explosion   :=  0;
         TRACESMOKE :=  0;  }
    end;
  end;
  {Draw Backpack Items}
  with Image3.Canvas do
  begin
    Font.Size := 9;
    if (BackpackSize * 2 - 2) * FontSize + 8 < Image3.Height then
    begin
      ScrollBar1.Enabled := false;
      ScrollBar1.Position := 0;
      //ScrollBar1.Visible:=false;
      //Image3.width:=194;
    end
    else
    begin
      ScrollBar1.Enabled := true;
      ScrollBar1.Min := 0;
      ScrollBar1.Max := (BackpackSize * 2 - 2) * FontSize - Image3.Height + 8;
      //ScrollBar1.Visible:=true;
      //set maxRanges;
      //Image3.width:=176
    end;
    Brush.Style := bsSolid;
    Brush.Color := 0;
    FillRect(0, 0, Image3.Width, Image3.Height);
    Pen.Width := 1;
    if Selected > 0 then
    begin
      yShift := ScrollBar1.Position;
      for i := 2 to BackpackSize do
      begin
        Brush.Style := bsSolid;
        if (i = SelectedItem)
        {and ((Bot[Selected].Items[i].W_ID>0) or (Bot[Selected].Items[i].Ammo_ID>0 )) } then
        begin
          Pen.Color := $888888;
          Brush.Color := $444444;
        end
        else
        begin
          Pen.Color := $333333;
          Brush.Color := $222222;
        end;
        Rectangle(3, (2 * i - 4) * FontSize + 3 - yShift, Image3.Width - 3,
          (2 * i - 2) * FontSize + 3 - yShift);
        Brush.Style := bsClear;
        if Bot[Selected].Items[i].W_ID > 0 then
        begin
          Font.Color := $FFFFFF;
          if Bot[Selected].Items[i].State < Bot[Selected].Items[i].MaxState div 2 then
            Font.Color := $99eeFF;
          if Bot[Selected].Items[i].State < Bot[Selected].Items[i].MaxState div 3 then
            Font.Color := $5555FF;
          if Bot[Selected].Items[i].State = 0 then
            Font.Color := $0000FF;
          TextOut(13, (2 * i - 4) * FontSize - yShift,
            WeaponSpecifications[Bot[Selected].Items[i].W_ID].Name +
            ' (' + IntToStr(Round(Bot[Selected].Items[i].State /
            Bot[Selected].Items[i].MaxState * 100)) + '%)');
        end;
        if Bot[Selected].Items[i].Ammo_ID > 0 then
        begin
          Font.Color := $FFFFFF;
          TextOut(13, (2 * i - 3) * FontSize - yShift,
            AmmoSpecifications[Bot[Selected].Items[i].Ammo_ID].Name +
            ' ' + IntToStr(Bot[Selected].Items[i].N));
        end;
      end;
    end;
  end;
  // Draw on-Floor Items
  with Image4.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := 0;
    FillRect(0, 0, Image4.Width, Image4.Height);
    Pen.Width := 1;
    Font.Size := 7;
    if Selected > 0 then
    begin
      find_OnFloor(Bot[Selected].X, Bot[Selected].Y);
      if OnFloorn > 0 then
      begin
        if (OnFloorn * 2) * Font7Size + 8 < Image4.Height then
        begin
          ScrollBar2.Enabled := false;
          ScrollBar2.Position := 0;
          //ScrollBar2.Visible:=false;
          //Image4.width:=194;
        end
        else
        begin
          ScrollBar2.Enabled := true;
          ScrollBar2.Min := 0;
          ScrollBar2.Max := (OnFloorn * 2) * Font7Size - Image4.Height + 8;
          //ScrollBar2.Visible:=true;
          //Image4.width:=176
        end;
        yShift := ScrollBar2.Position;
        for i := 1 to OnFloorn do
        begin
          Brush.Style := bsSolid;
          if (i = SelectedOnFloor)
          {and ((Bot[Selected].Items[i].W_ID>0) or (Bot[Selected].Items[i].Ammo_ID>0 )) } then
          begin
            Pen.Color := $666666;
            Brush.Color := $333333;
          end
          else
          begin
            Pen.Color := $222222;
            Brush.Color := $111111;
          end;
          Rectangle(3, (2 * i - 2) * Font7Size + 4 - yShift, Image4.Width - 3,
            (2 * i) * Font7Size + 3 - yShift);
          Brush.Style := bsClear;
          if Item[OnFloor[i]].Item.W_ID > 0 then
          begin
            Font.Color := $FFFFFF;
            if Item[OnFloor[i]].Item.State < Item[OnFloor[i]].Item.MaxState div 2 then
              Font.Color := $99eeFF;
            if Item[OnFloor[i]].Item.State < Item[OnFloor[i]].Item.MaxState div 3 then
              Font.Color := $5555FF;
            if Item[OnFloor[i]].Item.State = 0 then
              Font.Color := $0000FF;
            TextOut(13, (2 * i - 2) * Font7Size - yShift + 4,
              WeaponSpecifications[Item[OnFloor[i]].Item.W_ID].Name +
              ' (' + IntToStr(Round(Item[OnFloor[i]].Item.State /
              Item[OnFloor[i]].Item.MaxState * 100)) + '%)');
          end;
          if Item[OnFloor[i]].Item.Ammo_ID > 0 then
          begin
            Font.Color := $FFFFFF;
            TextOut(13, (2 * i - 1) * Font7Size - yShift + 3,
              AmmoSpecifications[Item[OnFloor[i]].Item.Ammo_ID].Name +
              ' ' + IntToStr(Item[OnFloor[i]].Item.N));
          end;
        end;
      end;
    end;
  end;

  {Draw Player Units List}
  with Image5.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := 0;
    FillRect(0, 0, Image5.Width, Image5.Height);
    PlayerUnitsN := 0;
    for i := 1 to NBot do
      if (Bot[i].Owner = Player) and (Bot[i].HP > 0) and
        (PlayerUnitsN < MaxUnitsList) then
      begin
        Font.Color := $FFFFFF;
        Font.Size := 7;
        Inc(PlayerUnitsN);
        PlayerUnits[PlayerUnitsN] := i;
      end;
    if (PlayerUnitsN - 1) * (Font7Size + 12) + 28 < Image5.Height then
    begin
      ScrollBar3.Enabled := false;
      ScrollBar3.Position := 0;
    end
    else
    begin
      ScrollBar3.Enabled := true;
      ScrollBar3.Min := 0;
      ScrollBar3.Max := (PlayerUnitsN - 1) * (Font7Size + 12) - Image5.Height + 28;
    end;
    yShift := ScrollBar3.Position;
    for i := 1 to PlayerUnitsN do
    begin
      if PlayerUnits[i] = Selected then
      begin
        Brush.Style := bsSolid;
        Brush.Color := $444444;
        Pen.Width := 1;
        Pen.Color := $666666;
        Rectangle(1, (i - 1) * (Font7Size + 12) - yShift, Image5.Width - 1,
          (i - 1) * (Font7Size + 12) + Font7Size + 12 - yShift);
      end;
      Brush.Style := bsClear;
      TextOut(3, (i - 1) * (Font7Size + 12) - yShift, Bot[PlayerUnits[i]].Name);
      Pen.Width := 3;
      Pen.Color := $3333dd;
      MoveTo(3, (i - 1) * (Font7Size + 12) + Font7Size + 3 - yShift);
      LineTo(3 + Round((Image5.Width - 7) * Bot[PlayerUnits[i]].HP /
        Bot[PlayerUnits[i]].MaxHP),
        (i - 1) * (Font7Size + 12) + Font7Size + 3 - yShift);
      Pen.Color := $33dd33;
      if Bot[PlayerUnits[i]].TU - MinimumTUUsable > (255 - MinimumTUUsable) div
        Image5.Width + 1 then
      begin
        MoveTo(3, (i - 1) * (Font7Size + 12) + Font7Size + 7 - yShift);
        LineTo(3 + Round((Image5.Width - 7) * (Bot[PlayerUnits[i]].TU -
          MinimumTUUsable) / (255 - MinimumTUUsable)), (i - 1) *
          (Font7Size + 12) + Font7Size + 7 - yShift);
      end;
    end;
  end;

  {Draw Enemy Units List}
  with Image6.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := 0;
    FillRect(0, 0, Image6.Width, Image6.Height);
    if (EnemyUnitsn - 1) * (Font7Size + 10) + 28 < Image6.Height then
    begin
      ScrollBar4.Enabled := false;
      ScrollBar4.Position := 0;
    end
    else
    begin
      ScrollBar4.Enabled := true;
      ScrollBar4.Min := 0;
      ScrollBar4.Max := (EnemyUnitsn - 1) * (Font7Size + 10) - Image6.Height + 28;
    end;
    yShift := ScrollBar4.Position;

    if EnemyUnitsn > 0 then
      for i := 1 to EnemyUnitsn do
      begin
        if EnemyUnits[i] = SelectedEnemy then
        begin
          Brush.Style := bsSolid;
          Brush.Color := $444444;
          Pen.Width := 1;
          Pen.Color := $666666;
          Rectangle(1, (i - 1) * (Font7Size + 10) - yShift, Image5.Width - 1,
            (i - 1) * (Font7Size + 10) + Font7Size + 9 - yShift);
        end;
        Font.Color := $FFFFFF;
        Font.Size := 7;
        Brush.Style := bsClear;
        TextOut(3, (i - 1) * (Font7Size + 10) - yShift, Bot[EnemyUnits[i]].Name);
        Pen.Width := 3;
        Pen.Color := $3333dd;
        MoveTo(3, (i - 1) * (Font7Size + 10) + Font7Size + 3 - yShift);
        LineTo(3 + Round((Image5.Width - 7) * Bot[EnemyUnits[i]].HP /
          Bot[EnemyUnits[i]].MaxHP),
          (i - 1) * (Font7Size + 10) + Font7Size + 3 - yShift);
      end;
  end;
end;

{--------------------------------------------------------------------------------------}

procedure TForm1.DrawMap;
{var mx,my,i:Integer;
    sx,sy:Integer;
    Scalex,Scaley,fx1,fy1,fx2,fy2:Single;
    Scaleminimapx,Scaleminimapy:Single;
    minimapx0,minimapy0:Integer;
    x1,y1,x2,y2:Integer;
    xx1,yy1:Integer;
    tmp_Color:Byte;
    btc1,btc2,btc3:Byte;}
//dx,dy,Range,maxRange:Integer;
//var ThisTime:TDateTime;
begin
  // ThisTime:=Now;
  CastleControl1.Paint;
  CastleControl2.Paint;

  Draw_Stats;

  Image2.Update;
  Image3.Update;
  Image4.Update;
  Image5.Update;
  Image6.Update;
  //if DebugMode then Label1.Caption:=IntToStr(Round(((now-ThisTime)*24*60*60*1000)))+'ms';

  ShowLOS := false;

end;


end.

