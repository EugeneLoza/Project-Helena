You can compile the source simply by opening projecthelena.lpr in Lazarus IDE and pushing F9 to complie&run.

To compile the game you have to install FPC + Lazarus (tested for FPC 3.3.1 and Lazarus 2.1, however, should work for earlier versions) and Castle Game Engine 6.5 (tested with 4.12.2018 GIT version, you can download latest GIT version here: https://github.com/castle-engine/castle-engine ).
Download and installation instructions may be found here  http://castle-engine.sourceforge.net/engine.php

Linux version requires (Debian/Ubuntu package reference):
32bit GTK+2 (Thanks Akien for the information)
libopenal1
libopenal-dev
libpng
libpng-dev
zlib1g
zlib1g-dev
libvorbis
libvorbis-dev
libfreetype6
libfreetype6-dev
You will also need dev version of OpenGL drivers for your videocard. In general case it is libgl1-mesa-dev.

Or Castle Game Engine DLLs (32 bit / 64 bit) in case of Windows. These may be downloaded here: http://castle-engine.sourceforge.net/engine.php
The DLLs must be placed in the exe folder (recommended) or in any folder referred to in $PATH.

If you prefer command-line compilation, you may try lazbuild. See instructions at http://wiki.lazarus.freepascal.org/lazbuild (Thanks to Akien for the information)